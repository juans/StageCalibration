#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  8 09:54:57 2018

@author: juans
"""

import numpy as np
import pandas as pd
import control as ctl
import scipy.signal as sg

import os

import matplotlib.pyplot as plt

def db(H):
    return ctl.mag2db(np.abs(H))

def deg(H):
    return np.rad2deg(np.angle(H))


nSpkrs = 8

spkrs = [] 

fax = np.logspace(np.log10(2e1), np.log10(2e4), 2**10)

b, a = sg.butter(4, 80 * 2 * np.pi, 'highpass', True)

dev1 = 5
dev2 = 0.1

for spkrId in range(nSpkrs):
    if spkrId == 4:
        b, a = sg.butter(4, 80 * 2 * np.pi, 'lowpass', True)
    _, H = sg.freqs(b, a, fax * 2 * np.pi)
    
    Hmag = db(H) + np.random.normal(scale=dev1, size=np.shape(H))
    Hdeg = deg(H) + np.random.normal(scale=dev1, size=np.shape(H))
    Hdeg = (Hdeg + fax * np.random.normal(scale=dev2) + 180) % 360 - 180
    
    spkr = pd.DataFrame({ 'Frequency' : fax, 'MagdB' : Hmag, 'Phase': Hdeg })
    
    plt.subplot(211)
    plt.semilogx(spkr.Frequency, spkr.MagdB)
    
    plt.subplot(212)
    plt.semilogx(spkr.Frequency, spkr.Phase)
    
    spkr.to_csv( str(spkrId + 1) + '.csv', index=False)
    
    
os.system('mv *.csv ../Data/TestData/')