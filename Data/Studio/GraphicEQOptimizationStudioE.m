clear all, close all, clc

addpath('../DSPFunc');
load('../GEq/GraphicEQ.mat');
HEq = HTs;
numSpkrs = 8;
N = 32768;  
[~, fs] = audioread('1.wav');

gEqFreqs = [31.5, 40, 50, 63, 80, 100, 125, 160, 200, 250, 315, 400, 500, 630, 800, 1000, 1250, 1600, 2000, 2500, 3150, 4000, 5000, 6300, 8000, 10000, 12500, 16000];


h = zeros(65536, numSpkrs);
for i = 1:numSpkrs
    h(:, i) = audioread([num2str(i), '.wav']);
    h(:, i) = circshift(h(:,i), -5512);
end
%%

H  = fft(h);
H  = H (1:end/2,:);
HPre = H;
fax = freqAxis(fs, N);
HEq = HEq(1:2:end,:);

[b, a] = butter(4, 40, 'high', 's');
w1 = abs(freqs(b, a, fax));
[b, a] = butter(4, 16000, 's');
w2 = abs(freqs(b, a, fax));

%%

close all;

H = HPre;
HSmoo = tfSmoother(fax, H, fs, 1/8, 'erb');

gains = zeros(8);
gEqGains = zeros(8, 28);

for i = 1:numSpkrs


    Hi = HSmoo(:, i);

    g = abs(Hi) \ ones(size(Hi));
    Hi = Hi * g;
    gains(i) = mag2db(g);
    H1mag = mag2db( abs(Hi ));
    HEqdB = mag2db( abs(HEq));

    x = lsqlin(HEqdB, -(H1mag.*w1.*w2), eye(28), 6 * ones(28, 1));

    HEqProc = 10.^(log10(HEq) * diag(x));

    H(:,i) = prod([H(:,i), HEqProc], 2);
    
    gEqGains(i,:) = x';
    
end

config = [gEqFreqs; gEqGains];

HSmoo = tfSmoother(fax, H, fs, 1/2, 'erb');

tfPlot(fax, HSmoo);

config1 = [gEqFreqs; round(4 * gEqGains(1:4,:)) / 4]';
config2 = [gEqFreqs; round(4 * gEqGains(5:8,:)) / 4]';








