//
//  Multichannel Level Tester.ck
//  Stage Callibration
//
//  Created by JuanS.
//

// ========================== Default Settings =========================== //

0.0                        => float inGainIndB;         // Gain in dB applied before analysis
0.0                        => float outGainIndB;        // Gain in dB applied to output of Speakers
2                          => int numChans;             // Number of Channels to analyze
3::second                  => dur duration;             // Window duration for analysis in seconds
2.0                        => float maxRmsRange;        // Maximum deviation of rms during measurement (error is +- maxRmsRange/2)
1                          => int printMeters;          // Enable printing of partial results
1                          => int printTotalResults;    // Enable printing of total results
0                          => float calibrationOffset;  // Difference between dbFs and dbSPL
1                          => int calibrationRequired;  // require dbSPL Calibration before starting
600::ms                    => dur rmsTime;              // Integration time for rms Follower
-1.0       =>    db2mag    => float clippingLimit;      // Limit to raise exception due to possible clipping
0                          => int outputChanOffset;     // Use if your first channel is not channel 0 in IO
0                          => int inputChannel;         // The Channel where the microphone is connected

// ================================ USAGE ================================ //

fun void printUsage() {

    chout <= IO.nl();
    chout <= "// ================== Usage: ================= //" <= IO.nl();
    chout <= "chuck MultichannelLevelTester.ck:default" <= IO.nl();
    chout <= "  Where the following default values will be used: " <= IO.nl();
    chout <= "      [numChans] = 2" <= IO.nl();
    chout <= "      [duration] = 3" <= IO.nl();
    chout <= "      [maxRmsRange] = 2" <= IO.nl();
    chout <= "      [inGain] = 0 dB" <= IO.nl();
    chout <= "      [outGain] = 0 dB" <= IO.nl();
    chout <= "  Or  " <= IO.nl() <= IO.nl();
    chout <= "chuck MultichannelLevelTester.ck:[numChans]:[duration]:[maxRmsRange]" <= IO.nl() <= IO.nl();
    chout <= "  Or  " <= IO.nl();
    chout <= "chuck MultichannelLevelTester.ck:[numChans]:[duration]:[maxRmsRange]:[requireCalibration(1/0)]" <= IO.nl() <= IO.nl();
    chout <= "  Or  " <= IO.nl();
    chout <= "chuck MultichannelLevelTester.ck:[numChans]:[duration]:[maxRmsRange]:[requireCalibration(1/0)]:[inGainIndB]:[outGainIndB]" <= IO.nl() <= IO.nl();
    chout <= "Where the following applies: " <= IO.nl();
    chout <= "      [numChans] is the number of different DAC channels to test" <= IO.nl();
    chout <= "      [duration] is the number of tests to run per channel" <= IO.nl();
    chout <= "      [maxRmsRange] is the duration of the anaysis window used (make sure this is longer than your estimated propagation time from speaker to microphone)" <= IO.nl();
    chout <= "      [requireCalibration(1/0)] is 1 where 1 indicates that the calibration will be required and 0 that it wont be executed" <= IO.nl();
    chout <= "      [inGain] is an additional Gain in dB applied to the input signal before the analysis" <= IO.nl();
    chout <= "      [outGain] is an additional Gain in dB applied to the output signal before going to the DAC" <= IO.nl() <= IO.nl();
    chout <= "// ================== Procedure: ================= //" <= IO.nl();
    chout <= "After running the script, the first speaker on the interface will be fed with pink noise" <= IO.nl();
    chout <= "Use the following keys to callibrate the input to measure dBSPL instead of dBFs: " <= IO.nl();
    chout <= "      m : minus 10dB " <= IO.nl();
    chout <= "      , : minus 1dB " <= IO.nl();
    chout <= "      . : plus  1dB " <= IO.nl();
    chout <= "      / : plus  10dB " <= IO.nl();
    chout <= "To finish the calibration hit the Space Bar " <= IO.nl();
    chout <= "After this, the script will reproduce pink noise through each speaker until the variation of the RMS"  <= IO.nl();
    chout <= "level is bellow the given [maxRmsRange] for the entire [duration] provided " <= IO.nl();
    chout <= "Notice a sad face [ :( ]  will appear next to the meters if the variation of the rms is currently greater than the limit" <= IO.nl();
    chout <= "This is important, as the script wont continue to the next speaker until the condition is satisfied "<= IO.nl();

    chout <= IO.nl();
    me.exit();
}

// ============================== ArgParser ============================== //

if ( me.args() == 0 ) {
    printUsage();
} else if (me.args() == 1 ) {
    if(me.arg(0) == "default") {
        chout <= "//================== Running with default Settings ==================== //" <= IO.nl();
    } else {
        printUsage();
    }
} else if (me.args() == 3) {
    me.arg(0) => Std.atoi => numChans;
    me.arg(1) => Std.atoi => float durInSeconds;
    durInSeconds::second => duration;
    me.arg(2) => Std.atof => maxRmsRange;
} else if (me.args() == 4) {
    me.arg(0) => Std.atoi => numChans;
    me.arg(1) => Std.atoi => float durInSeconds;
    durInSeconds::second => duration;
    me.arg(2) => Std.atof => maxRmsRange;
    me.arg(3) => Std.atoi => calibrationRequired;
} else if (me.args() == 6) {
        me.arg(0) => Std.atoi => numChans;
    me.arg(1) => Std.atoi => float durInSeconds;
    durInSeconds::second => duration;
    me.arg(2) => Std.atof => maxRmsRange;
    me.arg(3) => Std.atoi => calibrationRequired;
    me.arg(4) => Std.atof => inGainIndB;
    me.arg(5) => Std.atof => outGainIndB;
} else {
    printUsage();
}


// ============================= Load Required Classes ================================== //

// This class generates pink noise from a 6 biquad filter applied to white noise;
class PinkNoise extends Chubgraph {
    [
    [[1.0, 0.297084550557651, 0.0, -0.191295103633175, 0.0],
    [1.0, 0.262999405155621, -0.518299538540834, -0.164162476860957, -0.555216507865661],
    [1.0, 0.045729431542812, -0.916243581036970, -0.027981935558444, -0.924185458064213],
    [1.0, 0.006423434278709, -0.988235082039332, -0.003916555115119, -0.989388438498871],
    [1.0, 0.000873725554666, -0.998399597557809, -0.000532507148101, -0.998557268532788],
    [1.0, 0.000167572421047, -0.999832498574743, -0.000072099396910, -0.999804611201447]],
    [[1.0, 0.257984754805592, 0.0, -0.231762997295657, 0.0],
    [1.0, 0.225395170027167, -0.534087583424787, -0.200929080131387, -0.543236399535912],
    [1.0, 0.038857136564191, -0.919678747818714, -0.034469162438326, -0.921642699032913],
    [1.0, 0.005449699376258, -0.988734947659618, -0.004830190104279, -0.989020201135164],
    [1.0, 0.000741201513156, -0.998468032432799, -0.000655755791657, -0.998505890723132],
    [1.0, 0.000153884775473, -0.999846021578575, -0.000089846009156, -0.999798703915507]]
    ] @=> float coefs [][][];

    Noise nz;
    BiQuad biquads[6];
    nz.gain(0.01);
    1::second / 1::samp => float sampleRate;
    0 => int fltrSet;
    if (sampleRate == 48000)
        1 => fltrSet;
    nz => biquads[0];
    for (int i; i < biquads.size(); i++) {
        coefs[fltrSet][i][0] => biquads[i].b0;
        coefs[fltrSet][i][1] => biquads[i].b1;
        coefs[fltrSet][i][2] => biquads[i].b2;
        1.0 => biquads[i].a0;
        coefs[fltrSet][i][3] => biquads[i].a1;
        coefs[fltrSet][i][4] => biquads[i].a2;
        if (i + 1 == biquads.size())
            continue;
        biquads[i] => biquads[i+1];
    }
    biquads[5] => outlet;
}


// ============================= START ================================== //

// Getting input from channel 1
adc.chan(inputChannel) => Gain inGain;
PinkNoise pinkNoise => Gain outGain;

inGainIndB => db2mag => inGain.gain;
outGainIndB => db2mag => outGain.gain;

// setup rms follower
inGain => OnePole rmsFilter => blackhole;
inGain => rmsFilter;
3 => rmsFilter.op;

-Math.exp(-1 / (rmsTime / 1::samp)) => rmsFilter.a1;
1 + rmsFilter.a1() => rmsFilter.b0;


// some required global variables
int calibrationDone;
float rms;
float maxRms;
float minRms;
float rmsRange;
float levels[numChans];
int invalidFlag;
time later;

// spork clip checker
spork ~clippingChecker();

if (printMeters) spork ~printer();


// ========================== MISC FUNCS =========================== //

fun void drawMeter(float val, float min, float max) {
    30 => float totalNumTicks;
    (val - min) / (max - min) => float normVal;
    (normVal * totalNumTicks) $ int => int numTicks;
    for (int i; i < totalNumTicks; i ++) {
        if (i < numTicks) chout <= "=";
        else if(i == numTicks) chout <= "|";
        else chout <= " ";
    }
}

// round to two decimal positions
fun float round(float val) {
    return (((val * 100 + 0.5) $ int) $ float) / 100.0;
}

// chuck has a weird db converter :/
fun float mag2db(float mag) {
    return Std.rmstodb(mag) - 100;
}

// chuck has a weird db converter :/
fun float db2mag(float db) {
    return Std.dbtorms(db + 100);
}

// Use to go to dB full scale to dB Spl
fun float dbFs2dbSpl(float dbFs) {
    return dbFs + calibrationOffset;
}

// Use to go to dB full scale to dB Spl
fun float dbSpl2dbFs(float dbSpl) {
    return dbSpl - calibrationOffset;
}

// Keyboard listener for calibration!
fun void keyboardListener() {
    Hid hid;
    HidMsg hidMsg;

    hid.openKeyboard(0);
    <<< hid.name() >>>;

    while(true) {
        hid => now;
        while(hid.recv(hidMsg)) {
            if (hidMsg.isButtonDown()) {
                if (hidMsg.ascii == 46) 1+=> calibrationOffset;          // press .
                else if (hidMsg.ascii == 44) 1-=> calibrationOffset;     // press ,
                else if (hidMsg.ascii == 47) 10 +=> calibrationOffset;   // press /
                else if (hidMsg.ascii == 77) 10 -=> calibrationOffset;   // press m
                else if (hidMsg.ascii == 32) 1 => calibrationDone;       // press SPC
            }
        }
    }
}

// ============================== Calibration procedure ========================= //

if (calibrationRequired) {
    spork~ keyboardListener() @=> Shred kbShred;
    outGain => dac.chan(0 + outputChanOffset);
    while(calibrationDone == 0) {
        1::samp => now;
        rmsFilter.last() => Math.sqrt => mag2db => dbFs2dbSpl => rms;
    }
    kbShred.exit();
    outGain =< dac.chan(0 + outputChanOffset);
    1::second => now;
}


// Drawing meters
fun void printer() {
    60 => float meterRange;
    while(true) {
        1::second / 24.0 => now;
        chout <= "////////////////////////////////////////////////" <= IO.nl();
        drawMeter(rms, -meterRange + calibrationOffset, calibrationOffset);
        chout <= "RMS: \t" <= round(rms) <= IO.nl();
        drawMeter(rmsRange, 0, 6);
        chout <= "Dif: \t" <= rmsRange;
        if (rmsRange > maxRmsRange) chout <= "\t :( \t";
        chout <= IO.nl();
    }
}

// Check if anything is above max allowed limit, if it is, extend anaysis window for duration (make measurement invalid)
fun void clippingChecker() {
    while(true) {
        1::samp => now;
        pinkNoise.last() => float sampleOut;
        inGain.last() => float sampleIn;
        if (sampleOut >= clippingLimit || sampleIn >= clippingLimit) {
            <<< "Clipped" >>>;
            now + duration => later;
            100::ms => now;
        }
    }
}


for (int chan; chan < numChans; chan++) {
    1::second => now;
    // chuck to a specific speaker
    outGain => dac.chan(chan + outputChanOffset);
    // reset values
    -100 => maxRms;
    0 => minRms;

    // set analysis window
    now + duration => later;
    while(now < later) {
        1::samp => now;
        // compute RMS
        rmsFilter.last() => Math.sqrt => mag2db => dbFs2dbSpl => rms;

        // compute Bounds if outside of range
        if (rms > maxRms) rms => maxRms;
        if (rms < minRms) rms => minRms;

        //compute range
        maxRms - minRms => float newRmsRange;

        //Smooth range Value
        rmsFilter.b0() * (newRmsRange - rmsRange) +=> rmsRange;

        // if outside range, reset analysis window (make measurement invalid)
        if (rmsRange > maxRmsRange) now + duration => later;

        // slowly decrease range
        rmsFilter.b0() * (rms - maxRms) +=> maxRms;
        rmsFilter.b0() * (rms - minRms) +=> minRms;
    }
    // unchuck channel
    outGain =< dac.chan(chan + outputChanOffset);
    // save rms level to analysis vector
    rms => levels[chan];
}

chout <= "The calibration level used was: " <= calibrationOffset <= IO.nl();

if (printTotalResults) {
    chout <= "////////////////////////////////////////////////" <= IO.nl();
    chout <= "////////////////////////////////////////////////" <= IO.nl();

    for (int chan; chan < numChans; chan++) {
        chout <= "Chan " <= chan <= ": " <= levels[chan] <= IO.nl();
    }

    chout <= "////////////////////////////////////////////////" <= IO.nl();
    chout <= "////////////////////////////////////////////////" <= IO.nl();
}

FileIO levelFile;

levelFile.open("levels.csv", FileIO.WRITE );
levelFile <= "N, Level" <= IO.nl();

for (int chan; chan < numChans; chan++) {
    chan + 1 => int chanId;

    levelFile <= chanId <= ", " <= levels[chan] <= IO.nl();
}

levelFile.close();
