adc => Gain inGain => blackhole;
Impulse imp => Gain outGain => dac;

inGain.gain(1.0);
outGain.gain(1.0);

512 => int bufSize;

float buffer[bufSize];
int i;

spork~ audioListener();
spork~ pulseGenerator();

fun void pulseGenerator(){
    Hid hid;
    HidMsg hidMsg;
    
    hid.openKeyboard(0);
    <<< hid.name() >>>;
    
    while (true) {
        hid => now;
        while (hid.recv(hidMsg)) {
            if (hidMsg.isButtonDown() && hidMsg.ascii == 68) {
                <<< "Dirac!" >>>;
                imp.next(1.0);
            }
        }
    }
}

fun void analyze() {
    float ref;
    for (int i; i < buffer.size(); i++) {
        if (Math.fabs(buffer[i]) > Math.fabs(ref)) {
            buffer[i] => ref;
        }
    }
    
    if (Math.fabs(ref) < 0.0625) {
        return;
    }
    
    if (ref > 0) <<< "Positive" >>>;
    else <<< "Negative" >>>;
    
    
}

fun void audioListener() {
    while (true) {
        1::samp => now;
        inGain.last() => buffer[i];
        (i + 1) % bufSize => i;
        if (i == 0) {
            spork~ analyze();
        }
    }
}


while(true) {
    1::ms => now;
}