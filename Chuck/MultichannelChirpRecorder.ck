//
//  Multichannel Level Tester.ck
//  Stage Callibration
//
//  Created by JuanS.
//

// ========================== Default Settings =========================== //

2   => int numChans;            // Number of Speakers
0   => int micInput;            // Input used for the mic
1   => int refInput;            // Input used for the Reference
1   => int refOutput;           // Output used to feed signal back to ref Input
17  => int log2FileLength;      // Log2 of FileLength in samples
0   => float outGainIndB;       // OutputGain indB
0   => float inGainIndB;        // InputGain  indB
1   => int calibrationRequired; // Disable this to bypass the calibration

// ================================ USAGE ================================ //

fun void printUsage() {

     chout <= IO.nl();
     chout <= "// ================== Usage: ================= //" <= IO.nl();
     chout <= "chuck MultichannelNoiseRecorder.ck:default" <= IO.nl();
     chout <= "  Where the following default values will be used: " <= IO.nl();
     chout <= "     [numChans] = 1" <= IO.nl();
     chout <= "     [micInput] = 0" <= IO.nl();
     chout <= "     [refInput] = 1" <= IO.nl();
     chout <= "     [refOutput]= 1" <= IO.nl();
     chout <= "     [log2FileLength]= 17" <= IO.nl();
     chout <= " Or  " <= IO.nl() <= IO.nl();
     chout <= "chuck MultichannelNoiseRecorder.ck:[numChans]:[micInput]:[refInput]:[refOutput]:[log2FileLength]" <= IO.nl() <= IO.nl();
     chout <= " Or  " <= IO.nl() <= IO.nl();
     chout <= "chuck MultichannelNoiseRecorder.ck:[numChans]:[micInput]:[refInput]:[refOutput]:[log2FileLength]:[outGainIndB]:[inGainIndB]" <= IO.nl() <= IO.nl();
     chout <= "where the following applies: " <= IO.nl();
     chout <= "     [numChans] is the number of speakers to test" <= IO.nl();
     chout <= "     [micInput] is the ADC input where the measurement Mic is connected" <= IO.nl();
     chout <= "     [refInput] is the ADC input where the loopback signal is connected" <= IO.nl();
     chout <= "     [refOutput] is the DAC output used for the loopback signal, DON'T let this signal go to a speaker" <= IO.nl();
     chout <= "     [log2FileLength] is the log2 of the length of the file. Because it needs to be a power of 2, it is expressed as such power" <= IO.nl();
     chout <= "     [outGainIndB] is the output Gain applied before the signal goes to the ADCs" <= IO.nl();
     chout <= "     [InGainIndB] is the input Gain applied before the signal gets stored in the File" <= IO.nl();
     chout <= IO.nl(); IO.nl();
     chout <= "To setup the system, make sure each DAC Output is going to a different speaker and the last DAC creates a loopback" <= IO.nl();
     chout <= "that is physically fed to usually the second ADC of the device. Also connect the measurement mic into the ADC 1." <= IO.nl();
     chout <= "This will record the response on the left channel and the reference on the right channel." <= IO.nl();
     chout <= IO.nl();

     me.exit();
}

// ============================== ArgParser ============================== //

if ( me.args() == 0 ) {
    printUsage();
} else if (me.args() == 1 ) {
    if(me.arg(0) == "default") {
        chout <= "//================== Running with default Settings ==================== //" <= IO.nl();
    } else {
        printUsage();
    }
} else if (me.args() == 5) {
    me.arg(0) => Std.atoi => numChans;
    me.arg(1) => Std.atoi => micInput;
    me.arg(2) => Std.atoi => refInput;
    me.arg(3) => Std.atoi => refOutput;
    me.arg(4) => Std.atoi => log2FileLength;
} else if (me.args() == 7 ) {
    me.arg(0) => Std.atoi => numChans;
    me.arg(1) => Std.atoi => micInput;
    me.arg(2) => Std.atoi => refInput;
    me.arg(3) => Std.atoi => refOutput;
    me.arg(4) => Std.atoi => log2FileLength;
    me.arg(5) => Std.atof => outGainIndB;
    me.arg(6) => Std.atof => inGainIndB;
} else {
    printUsage();
}

// ============================= Load Required Classes ================================== //

// This class generates pink noise from a 6 biquad filter applied to white noise;
class ChirpGen {
    SinOsc osc => Envelope env => Gain out;

    20 => float f0;
    20000 => float f1;
    "log" => string type;
    10::second => dur duration;
    1::ms => dur startTime;
    1::ms => dur endTime;

    0 => int offFlag;
    0 => int isRunning;
    now => time later;
    float curFreq;
    float ratio;
    float diff;

    fun void setBandwidth(float fStart, float fEnd) {
        fStart => f0;
        fEnd => f1;
    }

    fun void setType(string newType) {
        if ((newType != "log") && (newType != "lin")) {
            cherr <= "Unrecognized Chirp Type" <= IO.nl();
            me.exit();
        }
        newType => type;
    }

    fun void setDuration(dur newDuration) {
        newDuration => duration;
    }

    fun void setRampTimes(dur newStartTime, dur newEndTime) {
        newStartTime => startTime;
        newEndTime => endTime;
    }

    fun void prepare() {
        Math.pow(f1/f0, 1 / (duration / 1::samp)) => ratio;
        (f1 - f0) / (duration / 1::samp) => diff;
        f0 => curFreq;
        0 => env.value;
        0 => isRunning;
    }

    fun void tick() {
        if (isRunning == 0) {
            now + duration => later;
            1 => env.keyOn;
            1 => env.target;
            startTime => env.duration;
            1 => isRunning;
        } else if (isRunning == 1) {
            if(now < later) {
                curFreq => osc.freq;
                if (type == "log"){
                    ratio *=> curFreq;
                } else if (type == "lin") {
                    diff +=> curFreq;
                }
                if (now >= later - endTime && offFlag == 0) {
                    1 => offFlag;
                    endTime => env.duration;
                    0 => env.target;
                    0 =>env.keyOff;
                }
            } else {
                -1 => isRunning;
            }
        }
    }
}

// ============================= START ================================== //


me.dir() => string path;
ChirpGen chirp;
chirp.out => Gain outGain;
WvOut2 waveOut => blackhole;
Gain inGainMic => waveOut.chan(0);
Gain inGainRef => waveOut.chan(1);

adc.chan(micInput) => inGainMic;
adc.chan(refInput) => inGainRef;

outGain => dac.chan(refOutput);

1<<log2FileLength => int fileLength;
inGainIndB => db2mag => inGainMic.gain;
inGainIndB => db2mag => inGainRef.gain;

chirp.setBandwidth(20, 20000);
chirp.setDuration(1::second);
chirp.setType("log");
chirp.setRampTimes(1::ms, 1::ms);
chirp.prepare();

// Some Global Variables

500::ms => dur rmsTime;
1.0 - Math.exp(-1 / (rmsTime / 1::samp)) => float bRms;
int calibrationDone;
float micRms;
float refRms;
float calibrationOffset;
float micState;
float refState;


spork ~printer() @=> Shred pShred;

// ========================== MISC FUNCS =========================== //
fun void drawMeter(float val, float min, float max) {
    30 => float totalNumTicks;
    (val - min) / (max - min) => float normVal;
    (normVal * totalNumTicks) $ int => int numTicks;
    for (int i; i < totalNumTicks; i ++) {
        if (i < numTicks) chout <= "=";
        else if(i == numTicks) chout <= "|";
        else chout <= " ";
    }
}

// round to two decimal positions
fun float round(float val) {
    return (((val * 100 + 0.5) $ int) $ float) / 100.0;
}

// chuck has a weird db converter :/
fun float mag2db(float mag) {
    return Std.rmstodb(mag) - 100;
}

// chuck has a weird db converter :/
fun float db2mag(float db) {
    return Std.dbtorms(db + 100);
}


fun void printer() {
    60 => float meterRange;
    while(true) {
        1::second / 24.0 => now;
        chout <= "////////////////////////////////////////////////" <= IO.nl();
        drawMeter(micRms, -meterRange + calibrationOffset, calibrationOffset);
        chout <= "mic RMS: \t" <= round(micRms) <= IO.nl();
        drawMeter(refRms, -meterRange + calibrationOffset, calibrationOffset);
        chout <= "ref RMS: \t" <= round(refRms) <= IO.nl();
        chout <= IO.nl();
    }
}

fun void keyboardListener() {
    Hid hid;
    HidMsg hidMsg;

    hid.openKeyboard(0);
    <<< hid.name() >>>;

    while(true) {
        hid => now;
        while(hid.recv(hidMsg)) {
            if (hidMsg.isButtonDown()) {
                if (hidMsg.ascii == 32) 1 => calibrationDone;       // press SPC
            }
        }
    }
}

fun void computeRMS() {
    while(true) {
        inGainMic.last() => float micVal;
        inGainRef.last() => float refVal;

        bRms * (micVal * micVal - micState) +=> micState;
        bRms * (refVal * refVal - refState) +=> refState;

        micState => Math.sqrt => mag2db => micRms;
        refState => Math.sqrt => mag2db => refRms;
        1::samp => now;
    }
}

// ============================== Calibration procedure ========================= //
spork~ computeRMS() @=> Shred mShred;

for (int i; i < numChans; i++) {
    path + "SpeakerChirp_" + ( i+1 ) + ".wav" => string name;
    chout <= name <= IO.nl();
    waveOut.wavFilename(name);
    outGain => dac.chan(i);
    outGainIndB => db2mag => outGain.gain;
    1::second => now;
    chirp.prepare();
    while(chirp.isRunning != -1) {
        1::samp => now;
        chirp.tick();
    }
    0.0 => outGain.gain;
    outGain =< dac.chan(i);
    fileLength::samp => now;
    waveOut.closeFile();
}
pShred.exit();
mShred.exit();
