class ChirpGen {
    SinOsc osc => Envelope env => Gain out;

    20 => float f0;
    20000 => float f1;
    "log" => string type;
    10::second => dur duration;
    1::ms => dur startTime;
    1::ms => dur endTime;

    0 => int offFlag;
    0 => int isRunning;
    now => time later;
    float curFreq;
    float ratio;
    float diff;

    fun void setBandwidth(float fStart, float fEnd) {
        fStart => f0;
        fEnd => f1;
    }

    fun void setType(string newType) {
        if ((newType != "log") && (newType != "lin")) {
            cherr <= "Unrecognized Chirp Type" <= IO.nl();
            me.exit();
        }
        newType => type;
    }

    fun void setDuration(dur newDuration) {
        newDuration => duration;
    }

    fun void setRampTimes(dur newStartTime, dur newEndTime) {
        newStartTime => startTime;
        newEndTime => endTime;
    }

    fun void prepare() {
        Math.pow(f1/f0, 1 / (duration / 1::samp)) => ratio;
        (f1 - f0) / (duration / 1::samp) => diff;
        f0 => curFreq;
        0 => env.value;
        0 => isRunning;
    }

    fun void tick() {
        if (isRunning == 0) {
            now + duration => later;
            1 => env.keyOn;
            1 => env.target;
            startTime => env.duration;
            1 => isRunning;
        } else if (isRunning == 1) {
            if(now < later) {
                curFreq => osc.freq;
                if (type == "log"){
                    ratio *=> curFreq;
                } else if (type == "lin") {
                    diff +=> curFreq;
                }
                if (now >= later - endTime && offFlag == 0) {
                    1 => offFlag;
                    endTime => env.duration;
                    0 => env.target;
                    0 =>env.keyOff;
                }
            } else {
                -1 => isRunning;
                me.exit();
            }
        }
    }
}

ChirpGen chirp;

chirp.out => dac;
chirp.setBandwidth(20, 20000);
chirp.setDuration(10::second);
chirp.setType("log");
chirp.setRampTimes(1::ms, 1::ms);
chirp.prepare();

while(true) {
    1::samp => now;
    chirp.tick();
}
