//
//  Multichannel Level Tester.ck
//  Stage Callibration
//
//  Created by JuanS.
//

// ========================== Default Settings =========================== //

2   => int numChans;            // Number of Speakers
0   => int micInput;            // Input used for the mic
1   => int refInput;            // Input used for the Reference
1   => int refOutput;           // Output used to feed signal back to ref Input
17  => int log2FileLength;      // Log2 of FileLength in samples
0   => float outGainIndB;       // OutputGain indB
0   => float inGainIndB;        // InputGain  indB
1   => int calibrationRequired; // Disable this to bypass the calibration

// ================================ USAGE ================================ //

fun void printUsage() {

     chout <= IO.nl();
     chout <= "// ================== Usage: ================= //" <= IO.nl();
     chout <= "chuck MultichannelNoiseRecorder.ck:default" <= IO.nl();
     chout <= "  Where the following default values will be used: " <= IO.nl();
     chout <= "     [numChans] = 1" <= IO.nl();
     chout <= "     [micInput] = 0" <= IO.nl();
     chout <= "     [refInput] = 1" <= IO.nl();
     chout <= "     [refOutput]= 1" <= IO.nl();
     chout <= "     [log2FileLength]= 17" <= IO.nl();
     chout <= " Or  " <= IO.nl() <= IO.nl();
     chout <= "chuck MultichannelNoiseRecorder.ck:[numChans]:[micInput]:[refInput]:[refOutput]:[log2FileLength]" <= IO.nl() <= IO.nl();
     chout <= " Or  " <= IO.nl() <= IO.nl();
     chout <= "chuck MultichannelNoiseRecorder.ck:[numChans]:[micInput]:[refInput]:[refOutput]:[log2FileLength]:[outGainIndB]:[inGainIndB]" <= IO.nl() <= IO.nl();
     chout <= "where the following applies: " <= IO.nl();
     chout <= "     [numChans] is the number of speakers to test" <= IO.nl();
     chout <= "     [micInput] is the ADC input where the measurement Mic is connected" <= IO.nl();
     chout <= "     [refInput] is the ADC input where the loopback signal is connected" <= IO.nl();
     chout <= "     [refOutput] is the DAC output used for the loopback signal, DON'T let this signal go to a speaker" <= IO.nl();
     chout <= "     [log2FileLength] is the log2 of the length of the file. Because it needs to be a power of 2, it is expressed as such power" <= IO.nl();
     chout <= "     [outGainIndB] is the output Gain applied before the signal goes to the ADCs" <= IO.nl();
     chout <= "     [InGainIndB] is the input Gain applied before the signal gets stored in the File" <= IO.nl();
     chout <= IO.nl(); IO.nl();
     chout <= "To setup the system, make sure each DAC Output is going to a different speaker and the last DAC creates a loopback" <= IO.nl();
     chout <= "that is physically fed to usually the second ADC of the device. Also connect the measurement mic into the ADC 1." <= IO.nl();
     chout <= "This will record the response on the left channel and the reference on the right channel." <= IO.nl();
     chout <= IO.nl();

     me.exit();
}

// ============================== ArgParser ============================== //

if ( me.args() == 0 ) {
    printUsage();
} else if (me.args() == 1 ) {
    if(me.arg(0) == "default") {
        chout <= "//================== Running with default Settings ==================== //" <= IO.nl();
    } else {
        printUsage();
    }
} else if (me.args() == 5) {
    me.arg(0) => Std.atoi => numChans;
    me.arg(1) => Std.atoi => micInput;
    me.arg(2) => Std.atoi => refInput;
    me.arg(3) => Std.atoi => refOutput;
    me.arg(4) => Std.atoi => log2FileLength;
} else if (me.args() == 7 ) {
    me.arg(0) => Std.atoi => numChans;
    me.arg(1) => Std.atoi => micInput;
    me.arg(2) => Std.atoi => refInput;
    me.arg(3) => Std.atoi => refOutput;
    me.arg(4) => Std.atoi => log2FileLength;
    me.arg(5) => Std.atof => outGainIndB;
    me.arg(6) => Std.atof => inGainIndB;
} else {
    printUsage();
}

// ============================= Load Required Classes ================================== //

// This class generates pink noise from a 6 biquad filter applied to white noise;
class PinkNoise extends Chubgraph {
    [
    [[1.0, 0.297084550557651, 0.0, -0.191295103633175, 0.0],
    [1.0, 0.262999405155621, -0.518299538540834, -0.164162476860957, -0.555216507865661],
    [1.0, 0.045729431542812, -0.916243581036970, -0.027981935558444, -0.924185458064213],
    [1.0, 0.006423434278709, -0.988235082039332, -0.003916555115119, -0.989388438498871],
    [1.0, 0.000873725554666, -0.998399597557809, -0.000532507148101, -0.998557268532788],
    [1.0, 0.000167572421047, -0.999832498574743, -0.000072099396910, -0.999804611201447]],
    [[1.0, 0.257984754805592, 0.0, -0.231762997295657, 0.0],
    [1.0, 0.225395170027167, -0.534087583424787, -0.200929080131387, -0.543236399535912],
    [1.0, 0.038857136564191, -0.919678747818714, -0.034469162438326, -0.921642699032913],
    [1.0, 0.005449699376258, -0.988734947659618, -0.004830190104279, -0.989020201135164],
    [1.0, 0.000741201513156, -0.998468032432799, -0.000655755791657, -0.998505890723132],
    [1.0, 0.000153884775473, -0.999846021578575, -0.000089846009156, -0.999798703915507]]
    ] @=> float coefs [][][];

    Noise nz;
    BiQuad biquads[6];
    nz.gain(0.01);
    1::second / 1::samp => float sampleRate;
    0 => int fltrSet;
    if (sampleRate == 48000)
        1 => fltrSet;
    nz => biquads[0];
    for (int i; i < biquads.size(); i++) {
        coefs[fltrSet][i][0] => biquads[i].b0;
        coefs[fltrSet][i][1] => biquads[i].b1;
        coefs[fltrSet][i][2] => biquads[i].b2;
        1.0 => biquads[i].a0;
        coefs[fltrSet][i][3] => biquads[i].a1;
        coefs[fltrSet][i][4] => biquads[i].a2;
        if (i + 1 == biquads.size())
            continue;
        biquads[i] => biquads[i+1];
    }
    biquads[5] => outlet;
}

// ============================= START ================================== //


me.dir() => string path;
PinkNoise pinkNoise => Gain outGain;
WvOut2 waveOut => blackhole;
Gain inGainMic => waveOut.chan(0);
Gain inGainRef => waveOut.chan(1);

adc.chan(micInput) => inGainMic;
adc.chan(refInput) => inGainRef;

outGain => dac.chan(refOutput);

1<<log2FileLength => int fileLength;
inGainIndB => db2mag => inGainMic.gain;
inGainIndB => db2mag => inGainRef.gain;

// Some Global Variables

500::ms => dur rmsTime;
1.0 - Math.exp(-1 / (rmsTime / 1::samp)) => float bRms;
int calibrationDone;
float micRms;
float refRms;
float calibrationOffset;
float micState;
float refState;


spork ~printer() @=> Shred pShred;

// ========================== MISC FUNCS =========================== //
fun void drawMeter(float val, float min, float max) {
    30 => float totalNumTicks;
    (val - min) / (max - min) => float normVal;
    (normVal * totalNumTicks) $ int => int numTicks;
    for (int i; i < totalNumTicks; i ++) {
        if (i < numTicks) chout <= "=";
        else if(i == numTicks) chout <= "|";
        else chout <= " ";
    }
}

// round to two decimal positions
fun float round(float val) {
    return (((val * 100 + 0.5) $ int) $ float) / 100.0;
}

// chuck has a weird db converter :/
fun float mag2db(float mag) {
    return Std.rmstodb(mag) - 100;
}

// chuck has a weird db converter :/
fun float db2mag(float db) {
    return Std.dbtorms(db + 100);
}


fun void printer() {
    60 => float meterRange;
    while(true) {
        1::second / 24.0 => now;
        chout <= "////////////////////////////////////////////////" <= IO.nl();
        drawMeter(micRms, -meterRange + calibrationOffset, calibrationOffset);
        chout <= "mic RMS: \t" <= round(micRms) <= IO.nl();
        drawMeter(refRms, -meterRange + calibrationOffset, calibrationOffset);
        chout <= "ref RMS: \t" <= round(refRms) <= IO.nl();
        chout <= IO.nl();
    }
}

fun void keyboardListener() {
    Hid hid;
    HidMsg hidMsg;

    hid.openKeyboard(0);
    <<< hid.name() >>>;

    while(true) {
        hid => now;
        while(hid.recv(hidMsg)) {
            if (hidMsg.isButtonDown()) {
                if (hidMsg.ascii == 32) 1 => calibrationDone;       // press SPC
            }
        }
    }
}

fun void computeRMS() {
    while(true) {
        inGainMic.last() => float micVal;
        inGainRef.last() => float refVal;

        bRms * (micVal * micVal - micState) +=> micState;
        bRms * (refVal * refVal - refState) +=> refState;

        micState => Math.sqrt => mag2db => micRms;
        refState => Math.sqrt => mag2db => refRms;
        1::samp => now;
    }
}

// ============================== Calibration procedure ========================= //
spork~ computeRMS() @=> Shred mShred;
if (calibrationRequired) {
    outGain => dac.chan(0);
    outGainIndB => db2mag => outGain.gain;
    spork~ keyboardListener() @=> Shred kbShred;

    while(calibrationDone == 0) {
        1::samp => now;
    }
    kbShred.exit();
    0.0 => outGain.gain;
    outGain =< dac.chan(0);
    1::second => now;
}


for (int i; i < numChans; i++) {
    path + "SpeakerNz_" + ( i+1 ) + ".wav" => string name;
    chout <= name <= IO.nl();
    waveOut.wavFilename(name);
    outGain => dac.chan(i);
    outGainIndB => db2mag => outGain.gain;
    fileLength::samp => now;
    0.0 => outGain.gain;
    waveOut.closeFile();
    outGain =< dac.chan(i);
    fileLength::samp => now;
}
pShred.exit();
mShred.exit();
