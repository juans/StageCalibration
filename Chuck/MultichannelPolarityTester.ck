//
//  Multichannel Polarity Tester.ck
//  Stage Callibration
//
//  Created by JuanS.
//

// ========================== Default Settings =========================== //

0.0                        => float inGainIndB;         // Gain in dB applied before analysis
-10.0                      => float outGainIndB;        // Gain in dB applied to output of Speakers
2                          => int numChans;             // Number of Channels to analyze
10                         => int numTests;             // Number of Tests per channel
100::ms                    => dur duration;             // Window duration for analysis in ms
(duration/1::samp) $ int   => int bufSize;              // Window duration for analysis in samples
1                          => int printPartialResults;  // Enable printing of partial results
1                          => int printTotalResults;    // Enable printing of total results

// ================================ USAGE ================================ //

fun void printUsage() {
    chout <= IO.nl();
    chout <= "// ================== Usage: ================= //" <= IO.nl();
    chout <= "chuck MultichannelPolarityTester.ck:default" <= IO.nl();
    chout <= "  Where the following default values will be used: " <= IO.nl();
    chout <= "      [numChans] = 2" <= IO.nl();
    chout <= "      [numTests] = 10" <= IO.nl();
    chout <= "      [duration] = 100" <= IO.nl();
    chout <= "      [inGain] = 0 dB" <= IO.nl();
    chout <= "      [outGain] = 0 dB" <= IO.nl();
    chout <= "  Or  " <= IO.nl() <= IO.nl();
    chout <= "chuck MultichannelPolarityTester.ck:[numChans]:[numTests]:[duration]" <= IO.nl() <= IO.nl();
    chout <= "  Or  " <= IO.nl();
    chout <= "chuck MultichannelPolarityTester.ck:[numChans]:[numTests]:[duration]:[inGainIndB]:[outGainIndB]" <= IO.nl() <= IO.nl();
    chout <= "Where the following applies: " <= IO.nl();
    chout <= "      [numChans] is the number of different DAC channels to test" <= IO.nl();
    chout <= "      [numTests] is the number of tests to run per channel" <= IO.nl();
    chout <= "      [duration] is the duration of the anaysis window used (make sure this is longer than your estimated propagation time from speaker to microphone)" <= IO.nl();
    chout <= "      [inGain] is an additional Gain in dB applied to the input signal before the analysis" <= IO.nl();
    chout <= "      [outGain] is an additional Gain in dB applied to the output signal before going to the DAC" <= IO.nl();

    me.exit();
}


// ============================== ArgParser ============================== //

if ( me.args() == 0 ) {
    printUsage();
} else if (me.args() == 1) {
    if (me.arg(0) == "default") {
        chout <= "//================== Running with default Settings ==================== //" <= IO.nl();
    } else {
        printUsage();
    }
} else if (me.args() == 3) {
    me.arg(0) => Std.atoi => numChans;
    me.arg(1) => Std.atoi => numTests;
    me.arg(2) => Std.atoi => int durationInMs;
    durationInMs::ms => duration;
} else if (me.args() == 5) {
    me.arg(0) => Std.atoi => numChans;
    me.arg(1) => Std.atoi => numTests;
    me.arg(2) => Std.atoi => int durationInMs;
    durationInMs::ms => duration;
    me.arg(3) => Std.atof => inGainIndB;
    me.arg(4) => Std.atof => outGainIndB;
} else {
    printUsage();
}

// ============================= START ================================== //

// Getting input from channel 1
adc.chan(0) => Gain inGain => blackhole;
Impulse imp => Gain outGain;

// Setting Gains for input and output
inGainIndB + 100 => Std.dbtorms => inGain.gain;
outGainIndB + 100 => Std.dbtorms => outGain.gain;


// CSV Written
FileIO delayFile;
FileIO polarityFile;

delayFile.open("delays.csv", FileIO.WRITE );
polarityFile.open("polarity.csv", FileIO.WRITE );

delayFile <= "N, Delay" <= IO.nl();
polarityFile <= "N, Polarity" <= IO.nl();

// Analisis Buffer;
float buffer[bufSize];
int polarities[numTests];
float delaysInMs[numTests];

chout <= " Make silence please: " <= IO.nl();
1::second => now;

chout <= " Starting Analysis: " <= IO.nl();
for (int chan; chan < numChans; chan++) {
    outGain => dac.chan(chan);
    // Reset global averages

    for (int test; test < numTests; test++) {
        // wait duration to start new impulse
        0 => polarities[test];
        0 => delaysInMs[test];
        duration => now;
        imp.next(1.0);
        for (int n; n < bufSize; n++) {
            // Store each sample after impulse in the analysis buffer
            inGain.last() => buffer[n];
            1::samp => now;
        }

        // anaylze blocking, only valid test if successful
        // TODO: (This might cause the program to get stuck!)
        analyze(test) => int successful;
        if (successful == 0)
            1 -=> test;
    }

    outGain =< dac.chan(chan);

    analyzeResults(chan) => int successful;
    if (successful == 0)
        1 -=> chan;

    2 * duration => now;
}

delayFile.close();
polarityFile.close();

fun int analyze(int testNumber) {
    int polarity;
    float ref;
    int sample;
    for (int n; n < bufSize; n++) {
        if (Std.fabs(ref) < Std.fabs(buffer[n])) {
            buffer[n] => ref;
            n => sample;
        }
    }

    sample::samp => dur delay;
    delay / 1::ms => float delayInMs;

    if ( Std.fabs(ref) < 0.0625) {
        chout <= "Level too low!!!" <= IO.nl();
        return 0;
    }

    if ( Std.fabs(ref) / inGain.gain() >= 1.0 ) {
        chout <= "level too High!!!" <= IO.nl();
        return 0;
    }

    if (ref > 0)
        1 => polarity;
    else
        -1 => polarity;

    if (printPartialResults) {
        if (polarity > 0)
            chout <=  "Polarity: positive" <= ", Delay: " <= delayInMs <= IO.nl();
        else
            chout <=  "Polarity: negative" <= ", Delay: " <= delayInMs <= IO.nl();
    }


    polarity => polarities[testNumber];
    delayInMs => delaysInMs[testNumber];

    return 1;
}

fun int analyzeResults(int chan) {

    getMean(polarities) => float meanPolarity;
    getMean(delaysInMs) => float meanDelay;
    getStd(polarities, meanPolarity) => float stdPolarity;
    getStd(delaysInMs, meanDelay) => float stdDelay;

    if (stdPolarity / Math.fabs(meanPolarity) > 0.1 || stdDelay / meanDelay > 0.1) {
        chout <= "// =========  Analysis Failed, Trying again ... ========= //" <= IO.nl();
        return 0;
    }

    chan + 1 => int chanId;

    chout <=  "// ===================================//" <= IO.nl();
    chout <=  "Average Results for chan:    " <= chanId <= IO.nl();
    chout <=  "Average Polarity:            " <= meanPolarity <= IO.nl();
    chout <=  "Std Polarity:                " <= stdPolarity <= IO.nl();
    chout <=  "Average Delay in ms:         " <= meanDelay <= IO.nl();
    chout <=  "Std Delay in ms:             " <= stdDelay <= IO.nl();
    chout <=  "// ===================================//" <= IO.nl();

    polarityFile <= chanId <= ", " <= meanPolarity <= IO.nl();
    delayFile <= chanId <= ", " <= meanDelay <= IO.nl();
    return 1;
}

fun float getMean(int vec[]) {
    float accum;
    for (int i; i < vec.size(); i++) {
        vec[i] +=> accum;
    }
    return accum / (vec.size() $ float);
}

fun float getMean(float vec[]) {
    float accum;
    for (int i; i < vec.size(); i++) {
        vec[i] +=> accum;
    }
    return accum / (vec.size() $ float);
}

fun float getStd(int vec[], float mean) {
    float accum;
    for (int i; i < vec.size(); i++) {
        (mean - vec[i]) * (mean - vec[i]) +=> accum;
    }
    return Math.sqrt(accum / (vec.size() $ float));
}

fun float getStd(float vec[], float mean) {
    float accum;
    for (int i; i < vec.size(); i++) {
        (mean - vec[i]) * (mean - vec[i]) +=> accum;
    }
    return Math.sqrt(accum / (vec.size() $ float));
}
