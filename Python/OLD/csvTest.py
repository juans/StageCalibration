#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 14 20:59:01 2018

@author: juans
"""


import numpy as np
import scipy as sp
import control as ctl

import matplotlib.pyplot as plt

fs = 48000;
path = "SmaartData/"
speakers = []
processors = []

class Speaker:
    def __init__(self, csvFile, identifier):
        data = np.loadtxt(open(csvFile, "rb"), delimiter=",", skiprows=1)
        self.fax = data[:, 0]
        self.magdB = data[:, 1]
        self.phase = data[:, 2]
        self.coherence = data[:, 3]
        self.id = identifier
    
    def getTF(self):
        return ctl.db2mag(self.magdB) * np.exp(1j * np.deg2rad(self.phase))
        
class Processor:
    def __init__(self, fs):
        self.fs = fs;
        self.gain = 1.0
        self.polarity = 1
        self.delay = 0
        self.iirFilters = []
        self.firFilters = []
        
    def setDelay(self, delayInMs):
        self.delay = delayInMs/1000.0
    
    def setGain(self, gainIndB):
        self.gain = ctl.db2mag(gainIndB)
        
    def setPolarity(self, polarity):
        self.polarity = polarity
        
    def addIIRFilter(self, iirFilter):
        self.iirFilters.apend(iirFilter)
        
    def addFIRFilter(self, firFilter):
        self.firFilter.append(firFilter)
        
    def process(self, fax, H):
        Y = H * self.gain * self.polarity * np.exp(1j * 2 * np.pi * fax * self.delay)
        for iir in self.iirFilters:
            Hf = sp.signal.freqs(iir.b, iir.a, fax * 2 * np.pi, analog=True)
            Y *= Hf
            
        for fir in self.firFilters:
            h = fir.getKernel()
            Hf = sp.fft(h, len(fax))
            Y *= Hf
            
        return Y
        

for i in range(56):
    fileName = path + str(i + 1) + ".csv"
    spkr = Speaker(fileName, i + 1)
    speakers.append(spkr)
    
for i in range(8):
    fileName = path + "S" + str(i + 1) + ".csv"
    sub = Speaker(fileName, i + 1)
    speakers.append(spkr)
    
delayFile = path + "delays.csv"
delayData = np.loadtxt(open(delayFile, "rb"), delimiter=",", skiprows=1, usecols=(1))

assert(len(delayData) == len(speakers))

for i in range(len(speakers)):
    processors.append(Processor(fs))
    processors[i].setDelay(-delayData[i])
    
    
    
    
 ## ======================================================================== ## 

for i in range(len(speakers)):
    speaker = speakers[i]
    plt.subplot(211)
    plt.semilogx(speaker.fax, speaker.magdB, linewidth = 0.1)
    plt.grid(True)
    plt.xlim(2e1, 2e4)
    plt.ylim(-30, 20)
    
    plt.subplot(212)
    plt.semilogx(speaker.fax, speaker.phase, linewidth = 0.1)
    plt.grid(True)
    plt.xlim(2e1, 2e4)
    plt.ylim(-180, 180)

