#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 22 21:49:22 2018

@author: juans
"""

import numpy as np
import matplotlib.pyplot as plt

fs = 48000
h = np.zeros(128);

h[8] = 1;


H = np.fft.fft(h);

H = H[0:int(len(H)/2)]


fax = np.linspace(0, fs/2, len(H), endpoint = False)


plt.plot(fax, np.unwrap(np.angle(H)))

A = np.reshape(2 * np.pi * fax, (-1, 1))
b = np.unwrap(np.angle(H))

t = np.linalg.lstsq(A, b)

to = t[0][0] * fs

print(to)