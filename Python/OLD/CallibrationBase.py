#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 29 10:04:56 2018

@author: juans
"""


import matplotlib.pyplot as plt
import numpy as np
import scipy as sp
import sounddevice as sd
from scipy.io import wavfile
from Utilities import *
from Graphics import *
from DSP import *



def getIRFromChirp(ref, res, win, irLen):
    x0 = ref
    x1 = res
    window = win
    
    N = np.size(x0)
    Nlog = int(np.ceil(np.log2(N)))
    
    xi = np.concatenate((np.zeros(int((2**Nlog - N)/2)), x0[::-1], np.zeros(int((2**Nlog - N)/2)), np.zeros(2**Nlog)))
    x0 = np.concatenate((np.zeros(int((2**Nlog - N)/2)), x0, np.zeros(int((2**Nlog - N)/2)), np.zeros(2**Nlog)))
    x1 = np.concatenate((np.zeros(int((2**Nlog - N)/2)), x1, np.zeros(int((2**Nlog - N)/2)), np.zeros(2**Nlog)))
    
    N = np.size(x0)
    
    Xi = sp.fft(xi)
    X0 = sp.fft(x0)
    X1 = sp.fft(x1)
    
    fi = np.linspace(-0.5, 0.5, N, endpoint = False)
    fi = np.fft.fftshift(fi)
    
    H0 = Xi * X0 * np.abs(fi) / np.sqrt(32 * N) * db2mag(-0.529)
    H1 = Xi * X1 * np.abs(fi) / np.sqrt(32 * N) * db2mag(-0.529)
    
    h0 = np.real( np.fft.ifft(H0) )
    h1 = np.real( np.fft.ifft(H1) )
    
    winLen = np.size(window)
    
    win = np.concatenate((window[:int(np.size(window)/2)], np.ones(irLen - winLen), window[int(np.size(window)/2):]))
    
    delay = findDelay(h1, h0)
    
    h1 = np.roll(h1, delay)
    
    h = h1[ int((np.size(h1) - winLen) / 2) : int((np.size(h1) - winLen) / 2) + irLen ] * win
    
    return h

plt.close('all')

fs, x0 = wavfile.read('../AudioFiles/RoomSineSweep/32Bit/IR_REF.wav')
fs, x1 = wavfile.read('../AudioFiles/RoomSineSweep/32Bit/IR_1.wav')
x0 = x0 / (2**31)
x1 = x1 / (2**31)

irLen = 2**10
win = np.hamming(2**4)
zpFact = 8

h1 = getIRFromChirp(x0, x1, win, irLen)
h0 = getIRFromChirp(x0, x0, win, irLen)

H0 = sp.fft(h0, zpFact * np.size(h0))    
H1 = sp.fft(h1, zpFact * np.size(h1))

H0 = firstHalf(H0)
H1 = firstHalf(H1)

H0mag = db( H0 )
H0ang = phase( H0 ) 

H1mag = db( H1 )
H1ang = phase( H1 ) 

fax = np.linspace(0, fs/2, np.size(H1mag), endpoint = False)

tfPlot( fax, H1 )
tfPlot( fax, H0 )

