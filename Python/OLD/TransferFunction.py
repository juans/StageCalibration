#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 23 23:28:40 2018

@author: juans
"""

import numpy as np
import control as ctl
from enum import Enum

class TransferFunction:
    class FreqType(Enum):
        lin = 0
        log = 1
        
    def __init__(self, fax, H):
        self.fax = fax
        self.H = H
        self.coherence = None
        self.bandwidth = None
        
    def setCoherence(self, coherence):
        assert(len(coherence) == len(self.H))
        self.coherence = coherence
       
    def getFreqs(self):
        return self.fax;
        
    def get(self):
        return self.H
    
    def getMag(self):
        return np.abs(self.H)
    
    def getMagdB(self):
        return ctl.mag2db(np.abs(self.H))
    
    def getPhase(self):
        return np.rad2deg(np.angle(self.H))
    
    def getBandwidth(self):
        magDif = np.convolve(np.diff(np.abs(self.H)), np.ones(2))
        minIdx = np.argmax(magDif)
        maxIdx = np.argmin(magDif)
        
        return np.array([minIdx, maxIdx])
        
        
    def getDelay(self):
        HAng = np.unwrap(np.angle(self.H))
        
        minIdx = np.argmax(HAng)
        maxIdx = np.argmin(HAng)
        
        p = np.polyfit(self.fax[minIdx:maxIdx], np.unwrap(np.angle(self.H[minIdx:maxIdx]))/(2 * np.pi), 1)
        delay = -p[0] - 13
        return delay
        
    def getGain(self):
        p = np.polyfit(self.fax, np.abs(self.H), 0)
        gain = p[0]
        return gain