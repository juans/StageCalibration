#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 23 17:27:21 2018

@author: juans
"""


import numpy as np
import matplotlib.pyplot as plt
import control as ctl

N = 2**10
K = 2**12
n = np.arange(N)
kLog = np.logspace(np.log10(1/K), np.log10(K), K, endpoint = False)
kLin = np.linspace(0, K, K, endpoint = False)

fs = 48000

f = np.array([1000, 500, 7000, 10000])
a = np.array([1, 0.5, 0.8, 0.4])
x = np.dot(a, np.cos(2 * np.pi * np.outer(f, n) / fs))

win = np.blackman(len(x))
win = win * 2

fftLogMatrix = np.exp(-1j * np.pi * np.outer(kLog, n) / K );
fftLinMatrix = np.exp(-1j * np.pi * np.outer(kLin, n) / K );
HLog = np.matmul(fftLogMatrix, x) / (N/2)
Win = np.matmul(fftLogMatrix, win) / (N/2)
HLog = np.convolve(HLog, Win, mode='same')/N
HLin = np.matmul(fftLinMatrix, win * x) / (N/2)
Hfft = np.fft.fft(win * x) / (N/2)
Hfft = Hfft[:int(len(Hfft)/2)]

faxLog = kLog * fs/2 / K
faxLin = kLin * fs/2 / K
faxFFT = np.linspace(0, fs/2, len(Hfft), endpoint = False)

s = 10

plt.subplot(3,1,1)
plt.plot()
plt.semilogx(faxLog, np.convolve(ctl.mag2db(np.abs(HLog)), np.ones(s)/s, mode = 'same'))
plt.xlim(2e1, 2e4)
plt.ylim(-100, 10)
plt.grid(True)
plt.subplot(3,1,2)
plt.semilogx(faxLin, ctl.mag2db(np.abs(HLin)))
plt.xlim(2e1, 2e4)
plt.ylim(-100, 10)
plt.grid(True)
plt.subplot(3,1,3)
plt.semilogx(faxFFT, ctl.mag2db(np.abs(Hfft)))
plt.xlim(2e1, 2e4)
plt.ylim(-100, 10)
plt.grid(True)
plt.show()
