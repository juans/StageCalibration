#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 15 01:29:58 2018

@author: juans
"""

import numpy as np
import control as ctl
import scipy as sp
import pandas as pd
import copy

from IIRFilter import IIRFilter

import matplotlib.pyplot as plt

pathToLoad = "SmaartData/"


class TransferFunction:
    def __init__(self, fax, H):
        self.fax = fax
        self.H = H
        self.id = -1
        
    def setCoherence(self, coherence):
        self.coherence = coherence
        
    def setId(self, identifier):
        self.id = identifier
        
    def setType(self, typeId, typeDescription):
        self.typeId = typeId
        self.typeDescription = typeDescription
        
    def setUse(self, useId, useDescription):
        self.useId = useId
        self.useDescription = useDescription
                
    def getMagdB(self):
        return ctl.mag2db(np.abs(self.H))
    
    def getPhase(self):
        return np.rad2deg(np.angle(self.H)) 
    
    def getMag(self):
        return np.abs(self.H)
    
class Processor:
    def __init__(self, fs):
        self.fs = fs
        self.gain = 1
        self.delay = 0
        self.polarity = 1
        self.iir = []
        self.fir = []
    
    def setGain(self, gainIndB):
        self.gain = ctl.db2mag(gainIndB)
        
    def setDelay(self, delayInMs):
        self.delay = delayInMs/1000.0
        
    def setPolarity(self, polarity):
        assert(np.abs(polarity) == 1)
        self.polarity = polarity;
        
    def addIIRFilter(self, iirFilter):
        self.iir.append(iirFilter)

    def addFIRFilter(self, firFilter):
        self.fir.append(firFilter)
        
    def process(self, fax, X):
        Y = X * self.gain * self.polarity * np.exp(-1j * 2 * np.pi * fax * self.delay)
        for iir in self.iir:
            _, H = sp.signal.freqs(iir.b, iir.a, fax * 2 * np.pi)
            Y *= H
            
        
        
        for fir in self.fir:
            _, H = sp.signal.freqz(fir, 1, 2 * np.pi * fax / fs)
            Y *= H
            
        return Y
    
    def getTF(self, fax, identifier = -1):
        H = self.process(fax, np.ones_like(fax))
        
        tf = TransferFunction(fax, H)
        tf.setCoherence(np.ones_like(fax))
        tf.setId(identifier)
        
        return tf
        
#def plotTransferFunction(tfArray, typeId=None, useId=None, sel=None):
#    plt.figure()
#    for tf in tfArray:
#        
#        if (tf.typeId != typeId and typeId != None): continue
#        if (tf.useId != useId and useId != None): continue
#        if (np.all(sel != tf.id) and np.all(sel != None)): continue
#        
#        plt.subplot(211)
#        plt.semilogx(tf.fax, tf.getMagdB(), linewidth = 0.5)
#        plt.xlim(2e1, 2e4)
#        plt.ylim(-40, 30)
#        plt.grid(True)
#        
#        plt.subplot(212)
#        plt.semilogx(tf.fax, tf.getPhase(), linewidth = 0.5)
#        plt.xlim(2e1, 2e4)
#        plt.ylim(-180, 180)
#        plt.grid(True)
        
class TFPlotter:
    def __init__(self):
        plt.ioff()
        self.tfRoom = []
        self.tfProcessor = []
        self.tfResult = []
        self.figs = []
        
        self.xLim = [2e1, 2e4]
        self.yLimMag = [-20, 40]
        self.yLimAng = [-180, 180]
        
        
        self.figs.append(plt.figure('Room'))
        self.figs.append(plt.figure('Processor'))
        self.figs.append(plt.figure('Result'))
        
        self.configurePlots()
        
    def configurePlots(self):
        
        for fig in self.figs:
            plt.figure(fig.number)
            
            plt.subplot(211)
            plt.xscale('log')
            plt.xlim(*self.xLim)
            plt.ylim(*self.yLimMag)
            plt.grid(True)
            
            plt.subplot(212)
            plt.xscale('log')
            plt.xlim(*self.xLim)
            plt.ylim(*self.yLimAng)
            plt.grid(True)
            
    def addTF(self, tf, tfType):
        if (tfType == 'Room'):
            self.tfRoom.append(tf)
        elif (tfType == 'Processor'):
            self.tfProcessor.append(tf)
        elif (tfType == 'Result'):
            self.tfResult.append(tf)
        else:
            raise Exception('TfType didnt match room, processor or result')
            
    def addSystem(self, system):
        for spkr in system.room:        
            idx = spkr.id
            tfPlotter.addTF(sys.room[idx], 'Room')
            fax = sys.room[idx].fax
            tfPlotter.addTF(sys.processor[idx].getTF(fax), 'Processor')
            tfPlotter.addTF(sys.result[idx], 'Result')
            
    def plot(self, typeId=[], useId=[], sel=[]):
        
        plt.figure('Room')
        self.plotTfArray(self.tfRoom)
        plt.show()
        plt.figure('Processor')
        self.plotTfArray(self.tfProcessor)
        plt.show()
        plt.figure('Result')
        self.plotTfArray(self.tfResult)
        plt.show()
        
    def plotTfArray(self, tfArray):
        lw = 0.3
        for tf in tfArray:
            plt.subplot(211)
            plt.semilogx(tf.fax, tf.getMagdB(), linewidth=lw)
            plt.subplot(212)
            plt.semilogx(tf.fax, tf.getPhase(), linewidth=lw)
            
class CalibrationSystem:
    def __init__(self, pathToLoad, numSpeakers, fs):
        self.pathToLoad = pathToLoad
        self.numSpeakers = 64
        self.fs = fs
        self.room = []
        self.processor = []
        self.result = []
        
        self.loadDataFiles()
        self.loadMeasurements()
        
    def loadDataFiles(self):
        self.sysData = pd.read_csv(self.pathToLoad + "SystemDescription.csv", sep=',')
        self.delayData = np.loadtxt(open(self.pathToLoad + "delays.csv", "rb"), delimiter=",", skiprows=1, usecols=(1))
        
    def loadMeasurements(self):
        for i in range(self.numSpeakers):
            fileName = str(i + 1) + ".csv"
            data = np.loadtxt(open(pathToLoad + fileName, "rb"), delimiter=",", skiprows=1)
            data = data.T
            fax = data[0]
            A = ctl.db2mag(data[1])
            theta = np.deg2rad(data[2])
            H = A * np.exp(1j * theta)
            c = data[3]
            
            measurement = TransferFunction(fax, H)
            measurement.setCoherence(c)
            measurement.setId(i)
            measurement.setType(self.sysData['TypeId'][i], self.sysData['Type'][i])
            measurement.setUse(self.sysData['UseId'][i], self.sysData['Use'][i])
            
            process = Processor(self.fs)
            
            res = copy.deepcopy(measurement)
            res.H = process.process(measurement.fax, measurement.H)
            
            self.room.append(measurement)
            self.processor.append(process)
            self.result.append(res)
        
        
pathToLoad = "SmaartData/"
numSpeakers = 64
fs = 48000
sys = CalibrationSystem(pathToLoad, numSpeakers, fs)
    
for spkr in sys.room:
    idx = spkr.id
    if spkr.useId == 0:
        lpf = IIRFilter(*IIRFilter.getButterworth(2, 91, 'low'))
        sys.processor[idx].addIIRFilter(lpf)
        sys.processor[idx].setGain(-12)
        
    if spkr.useId == 1:
        hpf = IIRFilter(*IIRFilter.getButterworth(2, 91, 'hi'))        
        sys.processor[idx].addIIRFilter(hpf)
    

for spkr in sys.room:
    idx = spkr.id
    sys.result[idx].H = sys.processor[idx].process(spkr.fax, spkr.H)



tfPlotter = TFPlotter()
tfPlotter.addSystem(sys)    
tfPlotter.plot()



    
    
        