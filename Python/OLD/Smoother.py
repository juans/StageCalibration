#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 30 19:57:57 2018

@author: juans
"""

import numpy as np
from Utilities import *

class Smoother:
    def __init__(self, fs, frac, smoothingType):
        self.fs = fs
        self.frac = frac
        self.smoothingType = smoothingType

    def smoothMagnitude(self, Hmag):
        if Hmag.ndim == 1:
            return np.squeeze( self.smoothMagnitude( Hmag[:, np.newaxis ] ) )
        Hsmooth = np.zeros_like(Hmag)
        N = np.shape(Hsmooth)[0]
        for i in np.arange(N):
            minLim = int( max( 0, i * 2**(-self.frac / 2) ) )
            maxLim = int( min( N-2, i * 2**( self.frac / 2) ) ) + 1
            if (minLim == maxLim): continue 
            Hsmooth[i,:] = np.nanmean( Hmag[minLim:maxLim,:] , axis = 0)
        return Hsmooth

    def smoothPhase(self, Hang):
        if Hang.ndim == 1:
            return np.squeeze( self.smoothPhase( Hang[:, np.newaxis ] ) )
        Hang = np.unwrap( Hang, axis = 0 )
        Hsmooth = np.zeros_like(Hang)
        N = np.shape(Hsmooth)[0]
        for i in np.arange(N):
            minLim = int( max( 0, i * 2**(-self.frac / 2 ) ) )
            maxLim = int( min( N-1, i * 2**( self.frac / 2 ) ) ) + 1
            Hsmooth[i,:] = np.nanmean(Hang[minLim:maxLim, :], axis = 0)
        return ((Hsmooth + np.pi) % (2 * np.pi)) - np.pi

    def smoothComplex(self, H):
        if H.ndim == 1:
            return np.squeeze( self.smoothComplex( H[:, np.newaxis ] ) )
        Hmag = db( H )
        Hang = phase( H )
        HmagSmooth = self.smoothMagnitude(Hmag)
        HangSmooth = self.smoothPhase(Hang)

        Hsmooth = db2mag( HmagSmooth ) * np.exp( 1j * HangSmooth )
        return Hsmooth



