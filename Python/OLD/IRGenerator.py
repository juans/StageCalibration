#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 22 01:03:47 2018

@author: juans
"""
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from scipy.io import wavfile
import control as ctl
from TransferFunction import TransferFunction


class IRGenerator:
    def __init__(self, tfSize, hopSize):
        self.tfSize = tfSize
        self.irSize = 2 * tfSize
        self.hopSize = hopSize
        
    def computeLogValues(self):
        n = np.arange(self.irSize)
        kLog = np.logspace(np.log10(1/self.tfSize), np.log10(self.tfSize), self.tfSize, endpoint = False)
        self.LDFT = np.exp(-1j * np.pi * np.outer(kLog, n) / self.tfSize );        
        
    def linearAnalysis(self, ref, res):
        assert(len(ref) == len(res))
        N = len(ref)

        ASpecX = np.zeros(self.irSize)
        ASpecY = np.zeros(self.irSize)
        XSpec  = np.zeros(self.irSize)

        win = np.blackman(self.irSize)

        pos = 0
        numHops = 0
        while(pos < N):
            if (pos + self.irSize > N):
                x = ref[pos:]
                y = res[pos:]
                x = np.concatenate((x, np.zeros(self.irSize - len(x))))
                y = np.concatenate((y, np.zeros(self.irSize - len(y))))
            else:
                x = ref[pos:pos + self.irSize]
                y = res[pos:pos + self.irSize]

            x = x * win
            y = y * win

            X = sp.fft(x)
            Y = sp.fft(y)

            ASpecX = ASpecX + np.conj(X) * X
            ASpecY = ASpecY + np.conj(Y) * Y
            XSpec  = XSpec  + np.conj(X) * Y

            pos = pos + self.hopSize
            numHops += 1

        H = XSpec / ASpecX
        c = XSpec * np.conj(XSpec) / (ASpecX * ASpecY)
        return H, c
    
    def logarithmicAnalysis(self, ref, res):
        assert(len(ref) == len(res))
        N = len(ref)
        
        ASpecX = np.zeros(self.tfSize)
        ASpecY = np.zeros(self.tfSize)
        XSpec  = np.zeros(self.tfSize)
        
        win = np.blackman(self.irSize)
        
        pos = 0
        numHops = 0
        while(pos < N):
            if (pos + self.irSize > N):
                x = ref[pos:]
                y = res[pos:]
                x = np.concatenate((x, np.zeros(self.irSize - len(x))))
                y = np.concatenate((y, np.zeros(self.irSize - len(y))))
            else:
                x = ref[pos:pos + self.irSize]
                y = res[pos:pos + self.irSize]
                
            x = x * win
            y = y * win
            
            X = np.matmul(self.LDFT, x)
            Y = np.matmul(self.LDFT, y)
            
            ASpecX = ASpecX + np.conj(X) * X
            ASpecY = ASpecY + np.conj(Y) * Y
            XSpec  = XSpec  + np.conj(X) * Y
            
            pos = pos + self.hopSize
            numHops += 1
        
        H = XSpec / ASpecX
        c = XSpec * np.conj(XSpec) / (ASpecX * ASpecY)
        return H, c
        
        
        
    def getIR(self, ref, res):
        H, _ = self.linearAnalysis(ref, res)
        return sp.ifft(H);
    
    def getFastLogTF(self, ref, res):
        H, c = self.linearAnalysis(ref, res)
        h = sp.ifft(H)
        cT= sp.ifft(c)
        Hlog = np.matmul(self.LDFT, h)
        cLog = np.matmul(self.LDFt, cT)
        return 
    
    
        
    def getTF(self, ref, res):
        H, c = self.analyzeSystem(ref, res)
        H = H[:int(len(H)/2)]
        c = c[:int(len(c)/2)]
        
        tf = TransferFunction.TransferFunction(H)
        tf.setCoherence(c)
        return tf
    