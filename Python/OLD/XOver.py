#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 28 17:15:54 2018

@author: juans
"""

import sys
import scipy as sp
import numpy as np
from FileManager import FileManager
from TFGenerator import TFGenerator
from SpectralProcessor import SpectralProcessor
from PlotView import PlotView
from IIRFilter import IIRFilter
from scipy.io import wavfile
import matplotlib.pyplot as plt


fs, _ = wavfile.read('AudioFiles/HF/Ref/1.wav');

N = 2**10
tfGen = TFGenerator(N, 2 * N, 'DFFT', 'log')
fax = tfGen.getFreqs()

fs, hfX = wavfile.read('AudioFiles/HF/Ref/1.wav');
fs, hfY = wavfile.read('AudioFiles/HF/Res/1.wav');
fs, lfX = wavfile.read('AudioFiles/LF/Ref/1.wav');
fs, lfY = wavfile.read('AudioFiles/LF/Res/1.wav');

## Starting objects

view = PlotView(fax, fs)

tfHi = tfGen.getTF(hfX, hfY)
tfLo = tfGen.getTF(lfX, lfY)
pHi = SpectralProcessor(fax, fs)
pLo = SpectralProcessor(fax, fs)
pTo = SpectralProcessor(fax, fs)


## Analysis

dHi = tfHi.getDelay()
dLo = tfLo.getDelay()
gHi = tfHi.getGain()
gLo = tfLo.getGain()

bwHi = tfHi.getBandwidth()
bwLo = tfLo.getBandwidth()

HHi = tfHi.get()

magDifHi = np.convolve(np.diff(np.abs(HHi)), np.ones(2))
magDifHi = np.convolve(magDifHi, np.ones(2**7), 'same')
minIdx = np.argmax(magDifHi)
maxIdx = np.argmin(magDifHi)
bwHi = np.array([minIdx, maxIdx])

print('Bandwidth of Hi: ', str(fax[bwHi] * fs) )

plt.semilogx(fax * fs, magDifHi)
plt.xlim([2e1, 2e4])

HLo = tfLo.get()

magDifLo = np.convolve(np.diff(np.abs(HLo)), np.ones(2))
magDifLo = np.convolve(magDifLo, np.ones(2**7), 'same')
minIdx = np.argmax(magDifLo)
maxIdx = np.argmin(magDifLo)
bwLo = np.array([minIdx, maxIdx])

plt.semilogx(fax * fs, magDifLo)

print('Bandwidth of Hi: ', str(fax[bwLo] * fs) )









######
### Processing Configuration
#
#pHi.setDelay(dHi)
#pLo.setDelay(dLo)
#pHi.setGain(1/gHi)
#pLo.setGain(1/gLo)
#
#pLo.addDelay(180)
#pHi.addGain(10)
#
#pHi.addGainIndB(-27)
#pLo.addGainIndB(-10)
#pLo.setPolarity(-1)
#
#
#b, a = sp.signal.butter(2, 91 * 2 * np.pi, btype='high', analog=True)
#pHi.addIIRFilter(IIRFilter(b, a))
#pHi.addIIRFilter(IIRFilter(b, a))
#
#b, a = sp.signal.butter(2, 91 * 2 * np.pi, btype='low', analog=True)
#pLo.addIIRFilter(IIRFilter(b, a))
#pLo.addIIRFilter(IIRFilter(b, a))
#
### Processing
#
#HHi = pHi.process(tfHi)
#HLo = pLo.process(tfLo)
#
### Summing
#
#HTo = pTo.processSum([HHi, HLo])
##pTo.addIIRFilter()
#
### Plotting
#
#view.addTF(HHi)
#view.addTF(HLo)
#view.addTF(HTo)
#view.setYlim(-30, 6)
#view.show()
