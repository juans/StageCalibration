#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 29 21:41:15 2018

@author: juans
"""


import numpy as np
import scipy as sp

def findDelay(a, b):
    A = sp.fft(a)
    B = sp.fft(b)
    xCorr = np.fft.ifft( A * np.conj(B) )
    return -np.argmax(np.abs(xCorr))


def chirpToIR(ref, res):
    x0 = ref
    x1 = res
    
    N = np.size(ref)
    Nlog = int(np.ceil(np.log2(N)))
    xi = np.concatenate((np.zeros(int((2**Nlog - N)/2)), x0[::-1], np.zeros(int((2**Nlog - N)/2)), np.zeros(2**Nlog)))
    x1 = np.concatenate((np.zeros(int((2**Nlog - N)/2)), x1, np.zeros(int((2**Nlog - N)/2)), np.zeros(2**Nlog)))
    
    N = np.size(xi)

    Xi = sp.fft(xi)
    X1 = sp.fft(x1)

    wi = np.linspace(-0.5, 0.5, N, endpoint = False)
    wi = np.fft.fftshift(wi)
    
    H = Xi * X1 * wi / (32 * np.sqrt(N))
    
    h = np.real( sp.ifft( H ) )
    
    return h


def getIRFromChirp(ref, res, win, irLen):
    x0 = ref
    x1 = res
    window = win
    
    N = np.size(x0)
    Nlog = int(np.ceil(np.log2(N)))
    
    xi = np.concatenate((np.zeros(int((2**Nlog - N)/2)), x0[::-1], np.zeros(int((2**Nlog - N)/2)), np.zeros(2**Nlog)))
    x0 = np.concatenate((np.zeros(int((2**Nlog - N)/2)), x0, np.zeros(int((2**Nlog - N)/2)), np.zeros(2**Nlog)))
    x1 = np.concatenate((np.zeros(int((2**Nlog - N)/2)), x1, np.zeros(int((2**Nlog - N)/2)), np.zeros(2**Nlog)))
    
    N = np.size(x0)
    
    Xi = sp.fft(xi)
    X0 = sp.fft(x0)
    X1 = sp.fft(x1)
    
    fi = np.linspace(-0.5, 0.5, N, endpoint = False)
    fi = np.fft.fftshift(fi)
    
    H0 = Xi * X0 * np.abs(fi) / np.sqrt(32 * N) * db2mag(-0.529)
    H1 = Xi * X1 * np.abs(fi) / np.sqrt(32 * N) * db2mag(-0.529)
    
    h0 = np.real( np.fft.ifft(H0) )
    h1 = np.real( np.fft.ifft(H1) )
    
    winLen = np.size(window)
    
    win = np.concatenate((window[:int(np.size(window)/2)], np.ones(irLen - winLen), window[int(np.size(window)/2):]))
    
    delay = findDelay(h1, h0)
    
    h1 = np.roll(h1, delay)
    
    h = h1[ int((np.size(h1) - winLen) / 2) : int((np.size(h1) - winLen) / 2) + irLen ] * win
    
    return h