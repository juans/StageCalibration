#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 30 13:45:25 2018

@author: juans
"""

import numpy as np
from Utilities import *

class IROptimizer:
    
    def __init__(self, irLen, irPad, preRoll, postRoll):
        assert(isPow2(irLen))
        assert(isPow2(irPad))
        assert(isPow2(preRoll))
        assert(isPow2(postRoll))
        assert(irPad > irLen)
        
        self.irLen = irLen
        self.irPad = irPad
        self.preRoll = preRoll
        self.postRoll = postRoll
        
        winStart = np.hanning(preRoll * 2)
        winStart = firstHalf(winStart)
        winEnd = np.hanning(postRoll * 2)
        winEnd = lastHalf(winEnd)
        
        win = np.concatenate( (winStart, np.ones(irLen - preRoll - postRoll), winEnd) )
        self.win = win[:, np.newaxis]
        
    def getOptimalIR(self, h):
        if h.ndim == 1:
            return np.squeeze( self.getOptimalIR( h[:, np.newaxis ] ) )
        nIr = np.shape(h)[1]
        hOpt = np.zeros(( self.irPad, nIr ))
        startIdx = int(np.shape(h)[0] / 2) - self.preRoll
        
        hOpt[:self.irLen,:] = h[ startIdx : startIdx + self.irLen ,:] * self.win
        hOpt = np.roll(hOpt, -self.preRoll + 1, axis = 0)
        return hOpt