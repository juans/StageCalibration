#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  8 00:01:33 2018

@author: juans
"""

import control as ctl
import numpy as np
import scipy as sp

import matplotlib.pyplot as plt
import matplotlib.ticker as plticker

class IIRDesigner:
    def getParametricEQ(freq, gainIndB, Q):
        g = ctl.db2mag(gainIndB)
        wo = freq * 2 * np.pi
        
        if g > 1:
            	b2 = 1/(wo * wo);
            	b1 = g/(Q * wo);
            	b0 = 1;
            	a2 = 1/(wo * wo);
            	a1 = 1/(wo * Q);
            	a0 = 1;
        elif g < 1:
            	b2 = 1/(wo * wo);
            	b1 = 1/(Q * wo);
            	b0 = 1;
            	a2 = 1/(wo * wo);
            	a1 = 1/(g * wo * Q);
            	a0 = 1;
        else:
            	b2 = 0;
            	b1 = 0;
            	b0 = 1;
            	a2 = 0;
            	a1 = 0;
            	a0 = 1;
                
        b = np.array([b2, b1, b0])
        a = np.array([a2, a1, a0])
        
        return b, a
    
    def getShelfEQ(freq, gainIndB, Q, Type):
        g = ctl.db2mag(gainIndB);
        wo = 2 * np.pi * freq;

        if g == 1:
            return 1, 1
        
        b2 = 1/(wo * wo)
        b1 = 1/(Q * wo)
        b0 = 1
        a2 = 1/(wo * wo)
        a1 = 1/(Q * wo)
        a0 = 1
            
        if Type == 'low':
            if g > 1:
                b0 *= g
            else:
                a0 /= g
        elif Type == 'hi':
            if g > 1:
                b2 *= g
            else:
                a2 /= g
        else:
            raise NameError('Type not recognized')
        	
        b = np.array([b2, b1, b0])
        a = np.array([a2, a1, a0])
        
        return b, a
    
    def getBiquadAllPass(freq, Q):
        wo = 2 * np.pi * freq
        
        b2 = 1/(wo * wo)
        b1 = -1/(wo * Q)
        b0 = 1
        a2 = 1/(wo * wo)
        a1 = 1/(wo * Q)
        a0 = 1
        
        b = np.array([b2, b1, b0])
        a = np.array([a2, a1, a0])
        
        return b, a
    
    def getAllPass(freq):
        wo = 2 * np.pi * freq
        
        b1 = -1 / wo
        b0 = 1
        a1 = 1 / wo
        a0 = 1
        
        b = np.array([b1, b0])
        a = np.array([a1, a0])
        
        return b, a 
    
    def getButterworth(order, freq, Type):
        wo = 2 * np.pi * freq
        if Type == 'low':
            Type = 'lowpass'
        elif Type == 'hi':
            Type = 'highpass'
        return sp.signal.butter(order, wo, Type, analog=True)
    
    def getLinkwitzRiley(order, freq, Type):
        if order % 2 != 0:
            raise NameError('Order Must be even for Linkwitz Riley Filter')
        b, a = IIRDesigner.getButterworth(order / 2, freq, Type)
        b = np.convolve(b, b)
        a = np.convolve(a, a)
        return b, a
        
    def getResonantFilter(freq, Q, Type):
        wo = 2 * np.pi * freq;
        
        if Type == 'low':
            b2 = 0
            b1 = 1/(Q * wo)
            b0 = 1
            a2 = 1/(wo * wo)
            a1 = 1/(Q * wo)
            a0 = 1
        elif Type == 'hi':
            b2 = 1/(wo * wo)
            b1 = 1/(Q * wo)
            b0 = 0
            a2 = 1/(wo * wo)
            a1 = 1/(Q * wo)
            a0 = 1
        else:
            raise NameError('Type not recognized')
        	
        b = np.array([b2, b1, b0])
        a = np.array([a2, a1, a0])
        
        return b, a
    
    def getResonantFilter2(freq, Q, Type):
        wo = 2 * np.pi * freq;
        
        if Type == 'low':
            b2 = 0
            b1 = 0
            b0 = 1
            a2 = 1/(wo * wo)
            a1 = 1/(Q * wo)
            a0 = 1
        elif Type == 'hi':
            b2 = 1/(wo * wo)
            b1 = 0
            b0 = 0
            a2 = 1/(wo * wo)
            a1 = 1/(Q * wo)
            a0 = 1
        else:
            raise NameError('Type not recognized')
        	
        b = np.array([b2, b1, b0])
        a = np.array([a2, a1, a0])
        
        return b, a
        
    
fax = np.linspace(2e1, 2e4, 2**10, endpoint = False)    
#b, a = IIRDesigner.getParametricEQ(1000, 10, 1)
#b, a = IIRDesigner.getShelfEQ(1000, 10, 1, 'low')
#b, a = IIRDesigner.getAllPass(1000, 1)
#b, a = IIRDesigner.getAllPass(10000)
#b, a = IIRDesigner.getButterworth(4, 1000, 'hi');
#b, a = IIRDesigner.getLinkwitzRiley(8, 1000, 'hi')
#_, H1 = sp.signal.freqs(b, a, fax * 2 * np.pi)
#
#b, a = IIRDesigner.getLinkwitzRiley(2, 1000, 'low')
#_, H2 = sp.signal.freqs(b, a, fax * 2 * np.pi)
#H = H2 - H1
#b, a = IIRDesigner.getResonantFilter2(1000, 10, 'hi')
#_, H1 = sp.signal.freqs(b, a, fax * 2 * np.pi)
#b, a = IIRDesigner.getResonantFilter2(1000, 10, 'low')
#_, H2 = sp.signal.freqs(b, a, fax * 2 * np.pi)
#b, a = IIRDesigner.getParametricEQ(1000, -10,  2)
#_, H3 = sp.signal.freqs(b, a, fax * 2 * np.pi)
#H = (H1 + H2) + H3

fc = 1000

b, a = IIRDesigner.getBiquadAllPass(fc, 1)

_, H = sp.signal.freqs(b, a, fax * 2 * np.pi)

z, p, k = sp.signal.tf2zpk(b, a)
z /= 2 * np.pi
p /= 2 * np.pi

plt.subplot(311)
plt.semilogx(fax, ctl.mag2db(np.abs(H)))
plt.grid(True, which='both')
plt.xlim(2e1, 2e4)
plt.ylim(-24, 24)
plt.gca().yaxis.set_major_locator(plticker.MultipleLocator(base = 6))



plt.subplot(312)
plt.semilogx(fax, np.rad2deg(np.angle(H)))
plt.grid(True, which='both')
plt.xlim(2e1, 2e4)
plt.ylim(-180, 180)

plt.subplot(313)
plt.plot(np.real(z), np.imag(z), 'o')
plt.plot(np.real(p), np.imag(p), 'X')
plt.axis('square')
plt.grid(True, which='both')
plt.xlim(-fc, fc)
plt.ylim(-fc, fc)
plt.gca().xaxis.set_major_locator(plticker.MultipleLocator(base = 500))
plt.gca().yaxis.set_major_locator(plticker.MultipleLocator(base = 500))



plt.show()