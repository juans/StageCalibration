#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 24 00:38:42 2018

@author: juans
"""

import numpy as np
import matplotlib.pyplot as plt
from TransferFunction import TransferFunction

class PlotView:
    
    idCounter = 0
    
    def __init__(self, fax, fs):
        self.fs = fs
        self.fax = fax * fs
        PlotView.idCounter += 1
        self.figureID = PlotView.idCounter
        plt.figure(self.figureID)
        self.xlim = [2e1, 2e4]
        self.ylim = [-80, 12]
        
    def addTF(self, tf):
        plt.figure(self.figureID)
        
        plt.subplot(2, 1, 1)
        plt.semilogx(self.fax, tf.getMagdB())
        
        plt.subplot(2, 1, 2)
        plt.semilogx(self.fax, tf.getPhase())
        
    def setXlim(self, low, high):
        self.xlim = [low, high]
        
    def setYlim(self, low, high):
        self.ylim = [low, high]
        
    def show(self):
        plt.figure(self.figureID)        
        plt.subplot(2, 1, 1)
        plt.grid(True)
        plt.xlim(self.xlim[0], self.xlim[1])
        plt.ylim(self.ylim[0], self.ylim[1])
        
        plt.subplot(2, 1, 2)
        plt.grid(True)
        plt.xlim(self.xlim[0], self.xlim[1])
        plt.ylim(-180, 180)
        
        plt.show()
        