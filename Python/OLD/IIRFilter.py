#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 24 03:39:49 2018

@author: juans
"""

import numpy as np
import control as ctl
import scipy as sp

class IIRFilter:
    def __init__(self, b, a):
        self.b = b
        self.a = a
        
    def getParametricEQ(freq, gainIndB, Q):
        g = ctl.db2mag(gainIndB)
        wo = freq * 2 * np.pi
        
        if g > 1:
            	b2 = 1/(wo * wo);
            	b1 = g/(Q * wo);
            	b0 = 1;
            	a2 = 1/(wo * wo);
            	a1 = 1/(wo * Q);
            	a0 = 1;
        elif g < 1:
            	b2 = 1/(wo * wo);
            	b1 = 1/(Q * wo);
            	b0 = 1;
            	a2 = 1/(wo * wo);
            	a1 = 1/(g * wo * Q);
            	a0 = 1;
        else:
            	b2 = 0;
            	b1 = 0;
            	b0 = 1;
            	a2 = 0;
            	a1 = 0;
            	a0 = 1;
                
        b = np.array([b2, b1, b0])
        a = np.array([a2, a1, a0])
        
        return b, a
    
    def getShelfEQ(freq, gainIndB, Q, Type):
        g = ctl.db2mag(gainIndB);
        wo = 2 * np.pi * freq;

        if g == 1:
            return 1, 1
        
        b2 = 1/(wo * wo)
        b1 = 1/(Q * wo)
        b0 = 1
        a2 = 1/(wo * wo)
        a1 = 1/(Q * wo)
        a0 = 1
            
        if Type == 'low':
            if g > 1:
                b0 *= g
            else:
                a0 /= g
        elif Type == 'hi':
            if g > 1:
                b2 *= g
            else:
                a2 /= g
        else:
            raise NameError('Type not recognized')
        	
        b = np.array([b2, b1, b0])
        a = np.array([a2, a1, a0])
        
        return b, a
    
    def getBiquadAllPass(freq, Q):
        wo = 2 * np.pi * freq
        
        b2 = 1/(wo * wo)
        b1 = -1/(wo * Q)
        b0 = 1
        a2 = 1/(wo * wo)
        a1 = 1/(wo * Q)
        a0 = 1
        
        b = np.array([b2, b1, b0])
        a = np.array([a2, a1, a0])
        
        return b, a
    
    def getAllPass(freq):
        wo = 2 * np.pi * freq
        
        b1 = -1 / wo
        b0 = 1
        a1 = 1 / wo
        a0 = 1
        
        b = np.array([b1, b0])
        a = np.array([a1, a0])
        
        return b, a 
    
    def getButterworth(order, freq, Type):
        wo = 2 * np.pi * freq
        if Type == 'low':
            Type = 'lowpass'
        elif Type == 'hi':
            Type = 'highpass'
        return sp.signal.butter(order, wo, Type, analog=True)
    
    def getLinkwitzRiley(order, freq, Type):
        if order % 2 != 0:
            raise NameError('Order Must be even for Linkwitz Riley Filter')
        b, a = IIRFilter.getButterworth(order / 2, freq, Type)
        b = np.convolve(b, b)
        a = np.convolve(a, a)
        
        return b, a
        
    def getResonantFilter(freq, Q, Type):
        wo = 2 * np.pi * freq;
        
        if Type == 'low':
            b2 = 0
            b1 = 1/(Q * wo)
            b0 = 1
            a2 = 1/(wo * wo)
            a1 = 1/(Q * wo)
            a0 = 1
        elif Type == 'hi':
            b2 = 1/(wo * wo)
            b1 = 1/(Q * wo)
            b0 = 0
            a2 = 1/(wo * wo)
            a1 = 1/(Q * wo)
            a0 = 1
        else:
            raise NameError('Type not recognized')
        	
        b = np.array([b2, b1, b0])
        a = np.array([a2, a1, a0])
        
        return b, a
    
    def getResonantFilter2(freq, Q, Type):
        wo = 2 * np.pi * freq;
        
        if Type == 'low':
            b2 = 0
            b1 = 0
            b0 = 1
            a2 = 1/(wo * wo)
            a1 = 1/(Q * wo)
            a0 = 1
        elif Type == 'hi':
            b2 = 1/(wo * wo)
            b1 = 0
            b0 = 0
            a2 = 1/(wo * wo)
            a1 = 1/(Q * wo)
            a0 = 1
        else:
            raise NameError('Type not recognized')
        	
        b = np.array([b2, b1, b0])
        a = np.array([a2, a1, a0])
        
        return b, a
        
            
                