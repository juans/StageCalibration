#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 30 13:38:23 2018

@author: juans
"""

import matplotlib.pyplot as plt
import numpy as np
import scipy as sp
import sounddevice as sd
from scipy.io import wavfile
from IRGenerator import IRGenerator
from IROptimizer import IROptimizer
from Utilities import *
from Graphics import *
from DSP import *

from Smoother import Smoother

fs, x0 = wavfile.read('../AudioFiles/RoomSineSweep/32Bit/IR_REF.wav')
_, x  = wavfile.read('../AudioFiles/RoomSineSweep/32Bit/IR_1.wav')
x0 = x0 / ( 2**31 )
x  = x  / ( 2**31 )

irLen = 2**7
irPad = 2**12
preRoll = 2**3
postRoll = 2**5
numSpkrs = 16

irGen = IRGenerator(x0)
h = irGen.getIR( x )

irOpt = IROptimizer(irLen, irPad, preRoll, postRoll)
hOpt = irOpt.getOptimalIR( h )

hOpt = np.roll(hOpt, 20)

h0 = np.zeros(2**10)
h0[20] = 1

H = sp.fft( h0 )
H = firstHalf( H )

Hmag = db( H )
Hang = phase( H )


smoother = Smoother(fs, 1/6.0, 1)

HSmoothMag = smoother.smoothMagnitude( Hmag )
HSmoothAng = smoother.smoothPhase( Hang )

fax = np.linspace(0, fs/2, np.size(Hmag), endpoint = False)

tfPlot(fax, Hmag, Hang)
tfPlot(fax, HSmoothMag, HSmoothAng)





