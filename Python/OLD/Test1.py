#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 21 21:07:37 2018

@author: juans
"""


import sys
from scipy.io import wavfile





class FileManager:
    def __init__(self, basePath):
        self.basePath = basePath
        self.HFRefPath = basePath + '/HF/Ref/'
        self.LFRefPath = basePath + '/LF/Ref/'
        self.HFResPath = basePath + '/HF/Res/'
        self.LFResPath = basePath + '/LF/Res/'
        self.fs = 0
        self.HFRef = []
        self.HFRes = []
        self.LFRef = []
        self.LFRes = []
        
    def loadFiles(self, nHF, nLF):
        for i in range(nHF):
            fs, data = wavfile.read(self.HFRefPath + str(i+1) + '.wav')
            if (self.fs == 0):
                self.fs = fs
            else:
                assert(fs == self.fs)
            self.HFRef.append(data)
            
        for i in range(nHF):
            fs, data = wavfile.read(self.HFResPath + str(i+1) + '.wav')
            if (self.fs == 0):
                self.fs = fs
            else:
                assert(fs == self.fs)
            self.HFRes.append(data)
        
        for i in range(nLF):
            fs, data = wavfile.read(self.LFRefPath + str(i+1) + '.wav')
            if (self.fs == 0):
                self.fs = fs
            else:
                assert(fs == self.fs)
            self.LFRef.append(data)
        
        for i in range(nLF):
            fs, data = wavfile.read(self.LFResPath + str(i+1) + '.wav')
            if (self.fs == 0):
                self.fs = fs
            else:
                assert(fs == self.fs)
            self.LFRes.append(data)





