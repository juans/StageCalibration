#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  1 00:57:11 2018

@author: juans
"""
import control as ctl
import numpy as np
import scipy as sp
from enum import Enum
from TransferFunction import TransferFunction

class TFGenerator:
    
    class Method(Enum):
        dfft = 0
        chirp = 1
    
    
    def __init__(self, tfSize, irSize, method, freqType):
        assert( method == 'DFFT' or method == 'chirp' )
        assert( freqType == 'lin' or freqType == 'log' or freqType == 'fullLog' )
        
        
        ## Setting Internal Variables
        
        self.tfSize = tfSize
        self.irSize = irSize
        self.hopSize = 64       # Fix this
        self.fax = None
        self.LDFT = None
        
        if method == 'DFFT':
            self.method = TFGenerator.Method.dfft
        elif method == 'chirp':
            self.method = TFGenerator.Method.chirp
        
        if freqType == 'lin':
            self.freqType = TransferFunction.FreqType.lin
        elif freqType == 'log':
            self.freqType = TransferFunction.FreqType.log
            
        self.win = np.blackman(self.irSize)
            
        if self.freqType == TransferFunction.FreqType.lin:
            self.fax = np.linspace(0, 0.5, self.tfSize, endpoint = False)
        elif self.freqType == TransferFunction.FreqType.log:
            self.fax = np.logspace(np.log10(1/self.tfSize), np.log10(self.tfSize), self.tfSize, endpoint = False) / (2 * self.tfSize)
            self.computeLDFT()
            
    def computeLDFT(self):
        n = np.arange(self.irSize)
        kLog = np.logspace(np.log10(1/self.tfSize), np.log10(self.tfSize), self.tfSize, endpoint = False)
        self.LDFT = np.exp(-1j * np.pi * np.outer(kLog, n) / self.tfSize );
            
    def getTF(self, ref, res):
        if self.method == TFGenerator.Method.dfft:
            H, C = self.dfft(ref, res)
        elif self.method == TFGenerator.Method.chirp:
            H, C = self.chirpToTF(ref, res)
            
        if self.freqType == TransferFunction.FreqType.log:
            h = sp.ifft(np.concatenate((H, np.zeros(1), H[-1:0:-1])))
            H = np.matmul(self.LDFT, h)
            
            c = sp.ifft(np.concatenate((C, np.zeros(1), C[-1:0:-1])))
            C = np.matmul(self.LDFT, c)
            
 
        tf = TransferFunction(self.fax, H)
        tf.setCoherence(C)
        
        return tf
    
    def getFreqs(self):
        return self.fax
    
    def dfft(self, ref, res):
        assert(len(ref) == len(res))
        N = len(ref)

        ASpecX = np.zeros(self.tfSize)
        ASpecY = np.zeros(self.tfSize)
        XSpec  = np.zeros(self.tfSize)

        pos = 0
        numHops = 0
        while(pos < N):
            if (pos + self.irSize > N):
                x = ref[pos:]
                y = res[pos:]
                x = np.concatenate((x, np.zeros(self.irSize - len(x))))
                y = np.concatenate((y, np.zeros(self.irSize - len(y))))
            else:
                x = ref[pos:pos + self.irSize]
                y = res[pos:pos + self.irSize]

            x = x * self.win
            y = y * self.win
            
#            if self.freqType == TransferFunction.FreqType.fullLog:
#                X = np.matmul(self.LDFT, x)
#                Y = np.matmul(self.LDFT, y)
#            else:
            X = sp.fft(x, 2 * self.tfSize)
            Y = sp.fft(y, 2 * self.tfSize)
            X = X[:self.tfSize]
            Y = Y[:self.tfSize]

            ASpecX = ASpecX + np.conj(X) * X
            ASpecY = ASpecY + np.conj(Y) * Y
            XSpec  = XSpec  + np.conj(X) * Y

            pos = pos + self.hopSize
            numHops += 1

        H = XSpec / ASpecX
        c = XSpec * np.conj(XSpec) / (ASpecX * ASpecY)
        return H, c
    
    def deconvolve(self, ref, res):
        assert(len(ref) == len(res))
        
        N = len(ref)
        Nlog = np.ceil(np.log2(N))
        
        ## set the proper length 
        zpVal = int((2**Nlog - N)/2)
        x = np.concatenate((np.zeros(zpVal), ref[::-1], np.zeros(zpVal + 2**Nlog)))
        y = np.concatenate((np.zeros(zpVal),    res   , np.zeros(zpVal + 2**Nlog)))
        
        X = sp.fft(x)
        Y = sp.fft(y)
        
        f = np.linspace(-0.5, 0.5, len(X), endpoint = False)
        f = np.fft.fftshift(np.abs(f))
        
        H = X * Y * f
        
        h = np.real(sp.ifft(H))
        
        h = h[int(len(h)/2) - self.prePad : int(len(h)/2) - self.prePad + self.irLen]
        h *= self.win
        
        if self.freqType == TFGenerator.FreqType.log:
            H = np.matmul(self.LDFT, h)
        elif self.freqType == TFGenerator.FreqType.lin:
            H = sp.fft(h, 2 * self.tfSize)
            H = H[:self.tfSize]
            
        c = np.ones_like(H)
            
        return H, c
            
        
        
        
        
        
        
            
            
        
    
    