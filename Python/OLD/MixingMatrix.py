#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 15 21:52:49 2018

@author: juans
"""

import numpy as np
import scipy as sp


class MixingMatrix:
    def __init__(self, numInputs, numOutputs):
        self.numInputs = numInputs
        self.numOutputs = numOutputs
        self.gainMatrix = np.zeros((numOutputs, numInputs))
        
        for i in range(min(*np.shape(self.gainMatrix))):
            self.gainMatrix[i][i] = 1.0
            
        
        

mixingMatrix = MixingMatrix(56, 64)

print(mixingMatrix.gainMatrix)
        