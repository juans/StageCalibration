#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 29 20:36:38 2018

@author: juans
"""

import matplotlib.pyplot as plt
import numpy as np
import scipy as sp
from Utilities import *


def tfPlot(fax, H):
    if H.ndim == 1:
        lw = 1
    else:
        lw = 1.0 / np.shape(H)[1]
    
    Hmag = db( H )
    Hang = phase( H )
    
    plt.figure()
    tfPlot(fax, Hmag, lw)
    tfPlot(fax, Hang, lw)
    plt.show()
    
def tfPlot(fax, Hmag, Hang):
    assert(np.shape(Hmag) == np.shape(Hang))
    if Hmag.ndim == 1:
        lw = 1
    else:
        lw = 1.0 / np.shape(Hmag)[1]
        
    plt.figure()
    tfPlotMag(fax, Hmag, lw)
    tfPlotAng(fax, Hang, lw)
    plt.show()
    
    
def irPlot(ir, fs):
    H = sp.fft(ir)
    H = firstHalf(H)
    fax = np.linspace(0, fs/2, np.size(H), endpoint = False)
    tfPlot(fax, H)
    
def tfPlotMag(fax, Hmag, lw):
    plt.subplot(2, 1, 1)
    plt.semilogx(fax, Hmag, linewidth = lw)
    plt.grid(True)
    plt.title('Magnitude')
    plt.ylabel('Amp [dB]')
    plt.xlim(2e1, 2e4)
    
def tfPlotAng(fax, Hang, lw):
    plt.subplot(2, 1, 2)
    plt.semilogx(fax, np.rad2deg(Hang), linewidth = lw)
    plt.title('Phase')
    plt.grid(True)
    plt.xlabel('Freq [Hz]')
    plt.ylabel('Phase [o]')
    plt.xlim(2e1, 2e4)
    plt.ylim(-180, 180)
    
    
    

    
    