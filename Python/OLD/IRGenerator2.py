#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 30 22:39:43 2018

@author: juans
"""

import numpy as np
import scipy as sp
import matplotlib as plt
import control as ctl
from TransferFunction import TransferFunction

class IRGenerator:
    def __init__(self, irSize, tfSize, hopSize):
        self.tfSize = tfSize
        self.irSize = irSize
        self.hopSize = hopSize
        self.tf = None
        self.c = None
        self.ir = None
        
    def initialize(self):
        n = np.arange(self.irSize)
        kLog = np.logspace(np.log10(1/self.tfSize), np.log10(self.tfSize), self.tfSize, endpoint = False)
        self.LDFT = np.exp(-1j * np.pi * np.outer(kLog, n) / self.tfSize)
        self.win = np.blackman(self.irSize)
        
    def analyze(self, ref, res, mode):
        assert(mode == 'lin' or mode == 'log')
        assert(len(ref) == len(res))
        N = len(ref)

        ASpecX = np.zeros(self.irSize)
        ASpecY = np.zeros(self.irSize)
        XSpec  = np.zeros(self.irSize)

        pos = 0
        numHops = 0
        while(pos < N):
            if (pos + self.irSize > N):
                x = ref[pos:]
                y = res[pos:]
                x = np.concatenate((x, np.zeros(self.irSize - len(x))))
                y = np.concatenate((y, np.zeros(self.irSize - len(y))))
            else:
                x = ref[pos:pos + self.irSize]
                y = res[pos:pos + self.irSize]

            x = x * self.win
            y = y * self.win
            
            if mode == 'lin':
                X = sp.fft(x, n=2 * self.tfSize)
                Y = sp.fft(y, n=2 * self.tfSize)                
            elif mode == 'log':
                X = np.matmul(self.LDFT, x)
                y = np.matmul(self.LDFT, y)

            ASpecX = ASpecX + np.conj(X) * X
            ASpecY = ASpecY + np.conj(Y) * Y
            XSpec  = XSpec  + np.conj(X) * Y

            pos = pos + self.hopSize
            numHops += 1

        H = XSpec / ASpecX
        c = XSpec * np.conj(XSpec) / (ASpecX * ASpecY)
        return H, c
    
    def getIR(self, ref, res):
        H, _ = self.analyze(ref, res, 'lin')
        h = sp.ifft(H)
        return np.real(h)
    
    def getTF(self, ref, res):
        H, c = self.analyze(ref, res, 'lin')
        H = H[:int(len(H)/2)]
        c = c[:int(len(c)/2)]    
        return H, c
    
    def getFastLogTF(self, ref, res):
        H, C = self.analyze(ref, res, 'lin')
        h = sp.ifft(H)
        c = sp.ifft(C)
        CLog = np.matmul(self.LDFT, c)
        HLog = np.matmul(self.LDFT, h)
        return HLog, CLog
        
    def getLogTF(self, ref, res):
        HLog, cLog = self.analyze(ref, res, 'log')
        
        return HLog, cLog
        
        
        
        
    
        
    
        
        