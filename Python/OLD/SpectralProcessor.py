#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 23 23:29:57 2018

@author: juans
"""

import numpy as np
import control as ctl
import scipy.signal as sp
from TransferFunction import TransferFunction




class SpectralProcessor:
    def __init__(self, fax, fs):
        self.fax = fax
        self.fs = fs
        self.polarity = 1
        self.gain = 1
        self.delay = 0
        self.iirFilters = []
        self.firFilters = None
        
    def setDelay(self, delay):
        self.delay = delay
        
    def addDelay(self, delay):
        self.delay += delay
        
    def setGain(self, gain):
        self.gain = gain
        
    def addGain(self, gain):
        self.gain *= gain
        
    def setGainIndB(self, gainIndB):
        self.gain = ctl.db2mag(gainIndB)
        
    def addGainIndB(self, gainIndB):
        self.addGain(ctl.db2mag(gainIndB))
        
    def setPolarity(self, polarity):
        assert(polarity == 1 or polarity == -1)
        self.polarity = polarity
        
    def getTF(self):
        H = np.ones_like(self.fax)
        return H * self.gain * np.exp(-1j * 2 * np.pi * self.fax * self.delay)
    
    def addIIRFilter(self, iirFilter):
        self.iirFilters.append(iirFilter)
        
    def processSum(self, tfArray):
        HT = np.zeros_like(tfArray[0].get())
        for H in tfArray:
            HT += H.get()
        
        HT = HT * self.polarity * self.gain * np.exp(1j * 2 * np.pi * self.fax * self.delay)
        for iirFilter in self.iirFilters:
            if (len(iirFilter.a) == 1 and len(iirFilter.b) == 1): continue
            _, HFilt = sp.freqs(iirFilter.b, iirFilter.a, self.fax * self.fs * 2 * np.pi)
            HT *= HFilt
        postTF = TransferFunction(self.fax, HT)
        return postTF
#        if (tfArray.any() == None):
#        cT = np.ones_like(tfArray[0].get())
#        for c in range(tfArray):
#            cT *= c
#        
#        postTF.setCoherence(cT)
#        return postTF
        
    def process(self, tf):
        H = tf.get() * self.polarity * self.gain * np.exp(1j * 2 * np.pi * self.fax * self.delay)
        for iirFilter in self.iirFilters:
            if (len(iirFilter.a) == 1 and len(iirFilter.b) == 1): continue
            _, HFilt = sp.freqs(iirFilter.b, iirFilter.a, self.fax * self.fs * 2 * np.pi)
            H *= HFilt
            
        postTF = TransferFunction(self.fax, H)
        if (tf.coherence is not None):
            postTF.setCoherence(tf.coherence)
            
        return postTF
    