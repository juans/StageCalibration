#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 15 03:41:24 2018

@author: juans
"""

import numpy as np
import scipy as sp
import control as ctl

import matplotlib.pyplot as plt


def dB(H):
    return ctl.mag2db(np.abs(H))

def deg(H):
    return np.rad2deg(np.angle(H))


data = np.loadtxt(open("SmaartData/1.csv", "rb"), delimiter=",", skiprows=1)
data = data.T

fax = data[0]
magdB = data[1]
phase = data[2]
fs = 48000

X = ctl.db2mag(magdB) * np.exp(1j * np.deg2rad(phase))

fir = np.ones(4)


_, H = sp.signal.freqz(fir, 1, 2 * np.pi * fax / fs)

Y = H * X;


plt.subplot(211)
#plt.semilogx(fax, dB(X))
#plt.semilogx(fax, dB(Y))
plt.semilogx(fax, dB(H))
plt.xlim(2e1, 2e4)
plt.ylim(-30, 20)
plt.grid(True)

plt.subplot(212)
#plt.semilogx(fax, deg(X))
#plt.semilogx(fax, deg(Y))
plt.semilogx(fax, deg(H))
plt.xlim(2e1, 2e4)
plt.ylim(-180, 180)
plt.grid(True)





