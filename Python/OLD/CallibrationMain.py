#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 29 20:33:53 2018

@author: juans
"""

import matplotlib.pyplot as plt
import numpy as np
import scipy as sp
import sounddevice as sd
from scipy.io import wavfile
from IRGenerator import IRGenerator
from Utilities import *
from Graphics import *
from DSP import *
from IROptimizer import IROptimizer


## ============== Loading AudioFiles Section ================== ##


baseFolder = '../AudioFiles/RoomSineSweep/32Bit/IR_'
extension = '.wav'
irLen = 2**7
irPad = 2**12
preRoll = 2**3
postRoll = 2**5
numSpkrs = 16


fs, x0 = wavfile.read(baseFolder + 'REF' + extension)
x0 = x0 / (2**31)
N = np.size(x0)

x = np.zeros((N, 16))

for i in range(0,7):
    fs, x[:, i] = wavfile.read(baseFolder + str(i+1) + extension)
    fs, x[:, i+8] = wavfile.read(baseFolder + str(i+1) + 's' + extension)
    
x = x / (2**31)
    

## ========== Computing Full Impulse Responses =============== ##

irGen = IRGenerator(x0)
h0 = irGen.getIR(x0)
h = irGen.getIR(x)
d = irGen.getDelay(h)

h = np.roll(h, d, axis = 0)

## ============== Trimm Impulse responses ==================== ##

irOpt = IROptimizer(irLen, irPad, preRoll, postRoll)

h0Opt = irOpt.getOptimalIR( h0 )
hOpt = irOpt.getOptimalIR( h )


## ============== Optimal Trimmed FFTs ============== ##

H0 = sp.fft(h0Opt)
H = sp.fft(hOpt, axis = 0)

H0 = firstHalf(H0)
H = firstHalf(H)

## ================= Data is ready start callibration ========== ##

fax = np.linspace(0, fs/2, np.size(H0), endpoint = False)




tfPlot(fax, H)
tfPlot(fax, H0)



#H = sp.fft(h, axis = 0)
#H = firstHalf(H)
#N = np.shape(H)[0]
#fax = np.linspace(0, fs / 2, N, endpoint = False)
#tfPlot(fax, H)