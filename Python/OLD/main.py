#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 23 20:02:08 2018

@author: juans
"""

import sys
import scipy as sp
import numpy as np
from FileManager import FileManager
from IRGenerator import IRGenerator
from SpectralProcessor import SpectralProcessor
from PlotView import PlotView
from IIRFilter import IIRFilter
#import numpy as np
#import scipy as sp
#import control as ctl
#import matplotlib.pyplot as plt


if (len(sys.argv) == 1):
    idx = sys.argv[0].find("main.py")
    basePath = sys.argv[0][:idx] + 'AudioFiles'
else:
    basePath = sys.argv[1]

#HFNumTypes = input("Set Number of HF Speaker Types: ")
#LFNumTypes = input("Set Number of LF Speaker Types: ")
nHFTypes = 1
nLFTypes = 1
nHFSpkrs = 1
nLFSpkrs = 1
files = FileManager(basePath)
files.loadFiles(nHFSpkrs, nLFSpkrs)


### get Tranfer functions
N = 2**10
irGen = IRGenerator(N, 2**6)

view = PlotView(N, files.fs)
view.magdBIR = True

tfRaw = []
tfProcessed = []
processorArray = []

pT = SpectralProcessor(N, files.fs)

for i in range(len(files.HFRef)):
    tf = irGen.getIR(files.HFRef[0], files.HFRes[0])

    processor = SpectralProcessor(N, files.fs)
    processor.setDelay( tf.getDelay() )
    processor.addDelay( 0 )
    processor.setGain( 1/tf.getGain() )    
#    processor.addIIRFilter(IIRFilter.parametricFilter(1000, -10, 5))
#    processor.addIIRFilter(IIRFilter.parametricFilter(5400, 0, 2))
    b, a = sp.signal.butter(4, 100, btype='highpass', analog=True)
    processor.addIIRFilter(IIRFilter(b, a))
    tfPost = processor.process(tf)
    view.addTF(tfPost)
    
    tfRaw.append(tf)
    processorArray.append(processor)
    tfProcessed.append(tfPost)

for i in range(len(files.LFRef)):
    tf = irGen.getIR(files.LFRef[0], files.LFRes[0])

    processor = SpectralProcessor(N, files.fs)
    delay = tf.getDelay()
    b, a = sp.signal.butter(2, 100 * 2 * np.pi, btype='lowpass', analog=True)
    processor.addIIRFilter(IIRFilter(b, a))
    processor.setDelay( delay )
    processor.addDelay( -10 )
    processor.setGainIndB( 6 )
    processor.setPolarity( 1 )
    tfPost = processor.process(tf)
    view.addTF(tfPost)
    
    tfRaw.append(tf)
    processorArray.append(processor)
    tfProcessed.append(tfPost)


view.setYlim(-20, 6)
view.show()




