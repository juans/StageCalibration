#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 29 20:35:17 2018

@author: juans
"""

import numpy as np


def mag2db(x):
    return 20 * np.log10(x)

def db2mag(x):
    return 10**(x / 20.0)

def db(X):
    return 20 * np.log10( np.abs( X ) )

def phase(X):
    return np.angle( X )

def firstHalf( x ):
    if x.ndim == 1:
        return np.squeeze( firstHalf( x[:, np.newaxis ] ) )
    N = np.shape(x)[0]
    N2 = int( N/2 ) 
    
    return x[ :N2,: ]

def lastHalf( x ):
    if x.ndim == 1:
        return np.squeeze( lastHalf( x[:, np.newaxis ] ) )
    N = np.shape(x)[0]
    N2 = int( N/2 )
    
    return x[ N2:,: ]

def isPow2(x):
    return bool( x and not ( x & ( x-1 ) ) )