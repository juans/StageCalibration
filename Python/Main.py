#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  8 09:59:29 2018

@author: juans
"""

from Settings import fs

import pandas as pd
import numpy as np
import control as ctl
from Speaker import Speaker
from Plotter import TFPlotter
from IIR import IIR





def createSystem(superPath):
    system = pd.read_csv(superPath + 'SystemDescription.csv')
    spkrs = []

    for spkr in system.values[:,0]:
        nId = spkr - 1
        path = superPath + str(spkr) + '.csv'
        data = np.genfromtxt(path, delimiter=',', skip_header=True)

        fax = data[:,0]
        mag = data[:,1]
        deg = data[:,2]

        if len(data[1,:]) == 4:
            coh = data[:,3]
        else:
            coh = np.ones_like(fax)

        H = ctl.db2mag(mag) * np.exp(1j * np.deg2rad(deg))

        metaData = system.values[nId]

        dsc = Speaker.Description(metaData[0], metaData[1], metaData[2], metaData[3], metaData[4])
        spkr = Speaker(fax, H, coh, dsc)

        spkrs.append(spkr)

    return spkrs



#path = '../Data/SmaartData/'
path = '../Data/SimpleData/'
#path = '../Data/TestData/'
sysDsc = pd.read_csv(path + 'SystemDescription.csv')
spkrs = createSystem(path)

tfPlotter = TFPlotter('erb', 1/2)
nTaps = 511
xOverFreq = 90
lowSlope = 12
hiSlope = 12
lowType = 'Butterworth'
hiType = 'Butterworth'


if lowSlope % 6 != 0:
    raise NameError('Low Slope needs to be a multiple of 6')
if hiSlope % 6 != 0:
    raise NameError('Hi Slope needs to be a multiple of 6')

lowOrder = int(lowSlope / 6)
hiOrder = int(hiSlope / 6)

lastTypeId = -1
for spkr in spkrs:

    useId = spkr.description.UseId
    name = spkr.description.Use
    fax = spkr.fax

    if useId == 0:
        b, a = IIR.getFilter(lowOrder, xOverFreq, lowType, 'low')
        lp = IIR(b, a, analog = True)
        spkr.processor.iir.append(lp)
    else:
        b, a = IIR.getFilter(hiOrder, xOverFreq, hiType, 'hi')
        hp = IIR(b, a, analog=True)
        spkr.processor.iir.append(hp)

    spkr.processor.gain = np.random.normal(1, 1) # Offset to force optimization
    spkr.optimizeGain()
    spkr.optimizeDelay()
    spkr.optimizeFIR(nTaps)

    H = spkr.getProcessed()
#    H = spkr.getRaw()
#    H = spkr.processor.fir.getTF(fax, fs)
    if (useId != lastTypeId):
        tfPlotter.addFig(useId, name)
    tfPlotter.addTF(useId, fax, H)

tfPlotter.show();
