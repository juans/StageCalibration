#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  8 22:06:18 2018

@author: juans
"""


import numpy as np
import scipy.signal as sg
import sys
sys.path.append("../.")
from ComplexFunc import db, deg, tfPlot

import matplotlib.pyplot as plt



N = 2**10
fax = np.logspace(np.log10(2e1), np.log10(2e4), N)

b, a = sg.butter(8, 100 * 2 * np.pi, btype = 'highpass', analog = True)

_, H1 = sg.freqs(b, a, fax * 2 * np.pi)

#delay = np.random.normal(0.01, 0.01)
delay = 0.0001

H2 = H1 * np.exp(1j * 2 * np.pi * fax * delay)

Hang1 = np.unwrap(np.angle(H1))
Hang2 = np.unwrap(np.angle(H2))

p = np.polyfit(fax, Hang2, 1)

#H3 = H2 * np.exp(-1j * 2 * np.pi * fax * p[0]/2)
H3 = H2 * np.exp(-1j * 2 * np.pi * fax * delay)

lin = np.polyval(p, fax)

plt.plot(fax, Hang1)
plt.plot(fax, Hang2)
plt.plot(fax, lin)








