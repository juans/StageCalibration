#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  8 09:59:29 2018

@author: juans
"""

import numpy as np
import control as ctl
import scipy.signal as sg


class FIR:
    def __init__(self, b):
        self.b = b

    def getTF(self, fax, fs):
        _, H = sg.freqz(self.b, 1, fax * 2 * np.pi / fs)
        return H
        
