#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  8 09:59:29 2018

@author: juans
"""

import matplotlib.pyplot as plt


from ComplexFunc import db, deg
from Smoothers import CBWSmoother, ERBSmoother

class TFPlotter:
    def __init__(self, smoothType, smoothMag):
        if smoothType == 'cbw':
            self.smoother = CBWSmoother(smoothMag)
        elif smoothType == 'erb':
            self.smoother = ERBSmoother(smoothMag)
        else:
            raise TypeError('Smoothing Type not recognized')

        self.xLim = [2e1, 2e4]
        self.yLimMag = [-20, 20]
        self.yLimAng = [-180, 180]
        self.figs = []

    def addFig(self, figId, name = None):
        self.figs.append(figId)
        plt.figure(figId)
        plt.subplot(211)
        if name != None: plt.title(name)
        plt.xlim(self.xLim)
        plt.ylim(self.yLimMag)
        plt.grid(True)
        plt.ylabel('Mag dB')

        plt.subplot(212)
        plt.xlabel('Freq')
        plt.ylabel('Phase')
        plt.xlim(self.xLim)
        plt.ylim(self.yLimAng)
        plt.grid(True)


    def addTF(self, figId, fax, H):

        HSmooth = self.smoother.smooth(fax, H)

        plt.figure(figId)
        plt.subplot(211)
        plt.semilogx(fax, db(HSmooth), linewidth=0.5)
        plt.subplot(212)
        plt.semilogx(fax, deg(HSmooth), linewidth=0.5)

    def show(self):
        for figId in self.figs:
            plt.figure(figId)
            plt.show()
