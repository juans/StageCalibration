#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  8 09:59:29 2018

@author: juans
"""

import numpy as np
import control as ctl
from dataclasses import dataclass

from ComplexFunc import db, deg
from Smoothers import CBWSmoother, ERBSmoother
from Processor import Processor

from Settings import fs



class Speaker:
    @dataclass
    class Description:
        N: int
        TypeId: int
        Type: str
        UseId: int
        Use: str
    
    def __init__(self, fax, H, coherence = None, description = None):
        self.fax = fax
        self.H = H
        self.coherence = coherence
        self.description = description
        self.processor = Processor(fs)

    def getRaw(self):
        return self.H

    def getProcessed(self):
        return self.processor.process(self.fax, self.H)
    
    def optimizeGain(self):
        self.processor.optimizeGain(self.fax, self.H)
        
    def optimizeDelay(self):
        self.processor.optimizeDelay(self.fax, self.H)
        
    def optimizeFIR(self, nTaps):
        self.processor.optimizeFIR(nTaps, self.fax, self.H)
