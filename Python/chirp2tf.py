#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov  9 04:37:21 2018

@author: juans
"""

import numpy as np
import scipy as sp
from MiscFunc import db, deg, tfPlot, nextPow2


fs, audio = wav.read('SpeakerNz_3.wav')
audio = audio / 2**16
audio = audio[:1000000,:]

res = audio[:, 0]
ref = audio[:, 1]

x0 = ref
x1 = res

N = len(x0)
Nlog = nextPow2(N)

xi = np.concatenate(( np.zeros(int((2**Nlog - N)/2)), np.flip(x0), np.zeros(int((2**Nlog - N)/2)) ))
x1 = np.concatenate(( np.zeros(int((2**Nlog - N)/2)), x1, np.zeros(int((2**Nlog - N)/2)) ))

N = len(x0)

Xi = sp.fft(xi)
X1 = sp.fft(x1)

wi = np.linspace(-1, 1, len(Xi), endpoint = False)
wi = sp.fftshift(wi)

H = Xi * X1 * np.abs(wi) / (16 * np.sqrt(N))

Hmag = db(H)
Hdeg = deg(H)
coh = np.ones_like(Hmag)
fax = np.linspace(0, fs/2, len(H), endpoint = False)

data = pd.DataFrame({ 'Frequency' : fax, 'MagdB' : Hmag, 'Phase': Hdeg, 'Coherence': coh})

data.to_csv( '1.csv', index=False)