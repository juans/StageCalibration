#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  8 09:59:29 2018

@author: juans
"""


import numpy as np
import control as ctl
import matplotlib.pyplot as plt


def db(H):
    return ctl.mag2db(np.abs(H))

def deg(H):
    return np.rad2deg(np.angle(H))

def zMean(H):
    Hmag = db(H)
    Hang = np.unwrap(np.angle(H))

    HmagMean = np.mean(Hmag)
    HangMean = np.mean(Hang)

    return ctl.db2mag(HmagMean) * np.exp(1j * HangMean)

def zwMean(fax, H):
    
    w = np.convolve(np.diff(fax), 0.5 * np.ones(2))
    Hmean = np.sum(H * w) / np.sum(w)
    return Hmean


def tfPlot(fax, H):
    plt.subplot(211)
    plt.semilogx(fax, db(H))
    plt.grid(True)
    plt.xlim(2e1, 2e4)
    plt.ylim(-20, 10)
    plt.subplot(212)
    plt.semilogx(fax, deg(H))
    plt.grid(True)
    plt.ylim(-180, 180)
    plt.xlim(2e1, 2e4)
