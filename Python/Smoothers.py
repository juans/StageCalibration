#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  8 09:59:29 2018

@author: juans
"""

import numpy as np
import control as ctl

from ComplexFunc import db, deg, zwMean

class CBWSmoother:
    def __init__(self, octFrac):
        self.octFrac = octFrac

    def smooth(self, fax, H):

        if self.octFrac == None:
            return H

        faxLog = np.log2(fax)

        lFaxLog = faxLog - self.octFrac / 2
        hFaxLog = faxLog + self.octFrac / 2
        lFaxLog[lFaxLog < faxLog[ 0]] = faxLog[ 0]
        hFaxLog[hFaxLog > faxLog[-1]] = faxLog[-1]

        Hmag = db(H)
        Hang = np.unwrap(np.angle(H))

        Hsmooth = np.zeros_like(H)

        for i in range(len(H)):
            lBin = np.argmax(lFaxLog[i] <= faxLog)
            hBin = np.argmax(hFaxLog[i] <= faxLog)

            HmagMean = zwMean(faxLog[lBin:hBin], Hmag[lBin:hBin] )
            HangMean = zwMean(faxLog[lBin:hBin], Hang[lBin:hBin] )

            Hsmooth[i] = ctl.db2mag(HmagMean) * np.exp(1j * HangMean)

        return Hsmooth


class ERBSmoother:
    def __init__(self, erbFrac):
        self.erbFrac = erbFrac

    def erb2freq(erb):
        freq = (10**(erb / 21.4) - 1.0) / 4.37 / 1000
        return freq

    def freq2erb(hz):
        erb = 21.4 * np.log10( 4.37 * hz / 1000 + 1.0)
        return erb


    def smooth(self, fax, H):

        if self.erbFrac == None:
            return H

        erb = ERBSmoother.freq2erb(fax)
        lErb = erb - self.erbFrac/2
        hErb = erb + self.erbFrac/2
        lErb[lErb < erb[ 0]] = erb[ 0]
        hErb[hErb > erb[-1]] = erb[-1]

        Hsmooth = np.zeros_like(H)

        Hmag = db(H)
        Hang = np.unwrap(np.angle(H))

        for i in range(len(H)):
            lBin = np.argmax(lErb[i] <= erb)
            hBin = np.argmax(hErb[i] <= erb)

            HmagMean = zwMean(erb[lBin:hBin], Hmag[lBin:hBin] )
            HangMean = zwMean(erb[lBin:hBin], Hang[lBin:hBin] )

            Hsmooth[i] = ctl.db2mag(HmagMean) * np.exp(1j * HangMean)

        return Hsmooth
