#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov  9 03:44:51 2018

@author: juans
"""

import numpy as np
import pandas as pd
import scipy as sp
import scipy.io.wavfile as wav

from ComplexFunc import db, deg

fs, audio = wav.read('SpeakerNz_3.wav')
audio = audio / 2**16
audio = audio[:1000000,:]

res = audio[:, 0]
ref = audio[:, 1]


N = len(res)
irLen = 2**10
hopSize = 2**6

SSpecX = np.zeros(irLen >> 1, dtype = complex)
SSpecY = np.zeros(irLen >> 1, dtype = complex)
CSpec  = np.zeros(irLen >> 1, dtype = complex)

win = sp.hamming(irLen)

pos = 0
numHops = 0

while( pos < N):
    if ( pos + irLen > N ):
        x = ref[pos:]
        y = res[pos:]
        x = np.concatenate((x, np.zeros(irLen - len(x))))
        y = np.concatenate((y, np.zeros(irLen - len(y))))
    else:
        x = ref[pos:pos + irLen]
        y = res[pos:pos + irLen]
        
    x = x * win
    y = y * win
    
    X = sp.fft(x)
    Y = sp.fft(y)
    
    XHalf = X[:int(len(X)/2)]
    YHalf = Y[:int(len(Y)/2)]
    
    SSpecX += np.conj(XHalf) * XHalf
    SSpecY += np.conj(YHalf) * YHalf
    CSpec  += np.conj(XHalf) * YHalf
    
    pos += hopSize
    numHops += 1

H = CSpec / SSpecX
Hmag = db(H)
Hdeg = deg(H)
coh = CSpec * np.conj(CSpec) / (SSpecX * SSpecY)
fax = np.linspace(0, fs/2, len(H), endpoint = False)

data = pd.DataFrame({ 'Frequency' : fax, 'MagdB' : Hmag, 'Phase': Hdeg, 'Coherence': coh})

data.to_csv( '1.csv', index=False)