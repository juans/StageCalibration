#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  8 09:59:29 2018

@author: juans
"""

import numpy as np
import scipy.signal as sg
import control as ctl
from Smoothers import CBWSmoother, ERBSmoother
from ComplexFunc import db, deg
from Settings import fs

from FIR import FIR

class Processor:
    def __init__(self, fs):
        self.fs = fs
        self.gain = 1
        self.polarity = 1
        self.delay = 0
        self.fir = FIR(np.ones(1))
        self.iir = []

    def getTF(self, fax):
        TF = np.ones_like(fax)
        TF = TF * self.gain * self.polarity * np.exp(-1j * 2 * np.pi * fax * self.delay) * self.fir.getTF(fax, self.fs)
        for filt in self.iir:
           TF *= filt.getTF(fax)
        return TF

    def process(self, fax, H):
        return H * self.getTF(fax)

    def optimizeGain(self, fax, H):
        w1 = np.convolve(np.diff(fax), 0.5 * np.ones(2))
        w2 = np.ones_like(fax)

        gainIndB = np.sum(db(H) * w1 * w2) / (np.sum(w1) * np.sum(w2))
        self.gain = ctl.db2mag(gainIndB)

    def optimizeDelay(self, fax, H):
        return

    def optimizeFIR(self, nTaps, fax, H):

        smoother = ERBSmoother(1)
        HSmooth = smoother.smooth(fax, H)

        wF = np.abs(self.iir[0].getTF(fax))
        wH = np.abs(self.iir[0].getTF(fax[::2]))

        HmagdB = db(HSmooth)

        HmagdB[HmagdB > 10] = 10
        HmagdB[HmagdB < -10] = -10

        Hmag = ctl.db2mag((-HmagdB))
        Hmag = Hmag * wF

        h = sg.firls(nTaps, fax, Hmag, wH, fs = fs)
        self.fir = FIR(h)
