clear all, close all, clc

% Some tests to understand group delay behaviour

% Set initial values
fs = 48000;             % SampleRate
fc = 1000;              % Frequency of signal
g = 20;                  % Gain of parametric section in dB
Q = 6;                 % Q of parametric section

wc = fc * 2 * pi;       
gMag = db2mag(g);

% Write typical transfer function for parametric section
B = [1 / (wc * wc), gMag/ (Q * wc), 1];
A = [1 / (wc * wc), 1/ (Q * wc), 1];

% Digitize
[Bd, Ad] = bilinear(B, A, fs);

% It looks like this :)
fax = linspace(0, fs/2, 2^16);

freqz(Bd, Ad, fax, fs)
subplot 211
set(gca, 'XScale', 'log')
xlim([2e1, 2e4])
ylim auto;

subplot 212
set(gca, 'XScale', 'log')
xlim([2e1, 2e4])
ylim([-180, 180]);

H = freqz(Bd, Ad, fax, fs);
Hmag = mag2db( abs ( H ) );
Hang = rad2deg( angle ( H ) );
%%

% For loop processing sinusoid starting at time 4800
% Visualize behavior while logarithmically changing frequency for 8 octaves

close all

for i = -48:0.1:48
    
    fx = fc * (2^(1/12.))^i;
    
    subplot 311
    plot(fax, Hmag);
    set(gca, 'XScale', 'log');
    xlim([2e1, 2e4]);
    ylim([-g, g])
    line([fx, fx],[-g, g]);
    grid on;
    
    subplot 312
    plot(fax, Hang);
    set(gca, 'XScale', 'log');
    xlim([2e1, 2e4]);
    ylim([-180, 180]);
    line([fx, fx],[-180, 180]);
    grid on;

    dur = 0.1;
    t = linspace(0, dur, dur * fs + 1);
    t = t(1:end-1);

    x = [zeros(size(t)), sin(2 * pi * fx * t), zeros(size(t))];

    y = filter(Bd, Ad,x);

    subplot 313
    hold off;
    plot(x);
    hold on
    plot(y);
    grid on
    xlim([fs/10 - 200,fs/10 + 200])
    ylim([-2, 2])
    
    pause(0.01)

end

