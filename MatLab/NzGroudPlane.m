clear all, close all, clc

addpath('../DSPFunc');
[~, fs] = audioread('AudioFiles/GroundPlane/32Bit/NzRef-SVS-12.wav');
subRef = audioread('AudioFiles/GroundPlane/32Bit/NzRef-SVS-12.wav');
subRes = audioread('AudioFiles/GroundPlane/32Bit/NzRes-SVS-12.wav');
mainRef = audioread('AudioFiles/GroundPlane/32Bit/NzRef-A77x-18.wav');
mainRes = audioread('AudioFiles/GroundPlane/32Bit/NzRes-A77x-18.wav');

irLen = 2^10;
hopSize = 2^8;

HMain = nzToTF(mainRef, mainRes, irLen, hopSize);
HSub  = nzToTF(subRef, subRes, irLen, hopSize);

%%

% plot(hMain)
% hold on
% plot(hSub)


%%

% HMain = fft(hMain);
% HSub = fft(hSub);
% 
% HMain = HMain(1:end/2);
% HSub = HSub(1:end/2);

N = length(HMain);

gMain = ones(size(fax)) \ (abs(HMain));
gAng = ones(size(fax)) \ (unwrap(angle(HMain)));

fax = freqAxis(fs, N);

tfPlot(fax, HMain);
tfPlot(fax, exp(-1j * gAng) * HMain / gMain);
% tfPlot(fax, HSub);
