clear all, close all, clc

fc = sqrt(2e1 * 2e4);
wc = fc * 2 * pi;
g = db2mag(6);
Q = 10;
fs = 44100;

B = [1 / (wc * wc), -1 / (Q * wc), 1];
A = [1 / (wc * wc), 1 / (Q * wc), 1];

fax = linspace(2e1, 2e4, 2^16);
wax = fax * 2 * pi;

freqs(B, A, wax)

[Bd, Ad] = bilinear(B, A, fs);

figure()
freqz(Bd, Ad, fax, fs)
subplot 211
set(gca, 'XScale', 'log')
subplot 212 
set(gca, 'XScale', 'log')

%%

close all

for i = -36:0.1:36
    
    fx = fc * (2^(1/12.))^i;
    
    dur = 0.1;
    t = linspace(0, dur, dur * fs + 1);
    t = t(1:end-1);

    x = [zeros(size(t)), cos(2 * pi * fx * t), zeros(size(t))];

    y = filter(Bd, Ad,x);

    hold off;
    plot(x);
    hold on
    plot(y);
    grid on
    xlim([4410 - 400,4410 + 400])
    ylim([-2, 2])
    
    pause(0.1)

end
