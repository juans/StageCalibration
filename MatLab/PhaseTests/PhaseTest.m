clear all, close all, clc

addpath('DSPFunc');

wc = sqrt(2e1 * 2e4) * 2 * pi;
Q = 1;
g = db2mag(2);
N = 2^16;
fs = 44100;

B = [1/(wc * wc), g / (Q * wc), 1];
A = [1/(wc * wc), 1 / (Q * wc), 1];

B2 = B .* [1, 0, 0];
B1 = B .* [0, 1, 0];
B0 = B .* [0, 0, 1];

fax = linspace(0, fs/2, N);
wax = linspace(0, fs/2, N) * 2 * pi;

H  = freqs(B, A, wax);
H2 = freqs(B2, A, wax);
H1 = freqs(B1, A, wax);
H0 = freqs(B0, A, wax);

[Bd, Ad] = bilinear(B, A, fs);
[Bd2, ~] = bilinear(B2, A, fs);
[Bd1, ~] = bilinear(B1, A, fs);
[Bd0, ~] = bilinear(B0, A, fs);

% Bd2 = Bd .* [1, 0, 0] / Bd(1);
% Bd1 = Bd .* [0, 1, 0] / Bd(2);
% Bd0 = Bd .* [0, 0, 1] / Bd(3);

Hd  = freqz(Bd,  Ad, fax, fs);
Hd2 = freqz(Bd2, Ad, fax, fs);
Hd1 = freqz(Bd1, Ad, fax, fs);
Hd0 = freqz(Bd0, Ad, fax, fs);
 
Gd  = grpdelay(Bd, Ad, fax, fs);
Gd2 = grpdelay(Bd2, Ad, fax, fs);
Gd1 = grpdelay(Bd1, Ad, fax, fs);
Gd0 = grpdelay(Bd0, Ad, fax, fs);

% ======================================================================= %

figure()
subplot 211
semilogx(fax, db(H));
hold on
semilogx(fax, db(H0));
semilogx(fax, db(H1));
semilogx(fax, db(H2));
grid on;
xlim([2e1, 2e4])
ylim([-20, 10]);

subplot 212
semilogx(fax, phase(H));
hold on
semilogx(fax, phase(H0));
semilogx(fax, phase(H1));
semilogx(fax, phase(H2));
grid on;
xlim([2e1, 2e4])
ylim([-180, 180]);

% ======================================================================= %

figure()
subplot 211
semilogx(fax, db(Hd));
hold on
semilogx(fax, db(Hd0));
semilogx(fax, db(Hd1));
semilogx(fax, db(Hd2));
grid on;
xlim([2e1, 2e4])
ylim([-20, 10]);

subplot 212
semilogx(fax, phase(Hd));
hold on
semilogx(fax, phase(Hd0));
semilogx(fax, phase(Hd1));
semilogx(fax, phase(Hd2));
grid on;
xlim([2e1, 2e4])
ylim([-180, 180]);

% ======================================================================= %

figure()
subplot 211
semilogx(wax, Gd);
hold on
semilogx(wax, Gd0);
semilogx(wax, Gd1);
semilogx(wax, Gd2);
grid on;
xlim([2e1, 2e4])

subplot 212
semilogx(wax, phase(Hd));
hold on
semilogx(wax, phase(Hd0));
semilogx(wax, phase(Hd1));
semilogx(wax, phase(Hd2));
grid on;
xlim([2e1, 2e4])
ylim([-180, 180]);



