clear all, close all, clc

[x0, fs] = audioread('AudioFiles/IR_REF.wav');
numSpkrs = 4;

x = zeros(length(x0), numSpkrs);

x(:, 1) = audioread('AudioFiles/IR_1.wav');
x(:, 2) = audioread('AudioFiles/IR_1s.wav');
x(:, 3) = audioread('AudioFiles/IR_2.wav');
x(:, 4) = audioread('AudioFiles/IR_2s.wav');

N = length(x0);
Nlog = nextpow2(length(x0));

x = [zeros((2^Nlog - N)/2, numSpkrs); x; zeros((2^Nlog - N)/2, numSpkrs); zeros(2^Nlog, numSpkrs)];
x0I = [zeros((2^Nlog - N)/2, 1); flip(x0); zeros((2^Nlog - N)/2, 1); zeros(2^Nlog, 1)];
x0  = [zeros((2^Nlog - N)/2, 1); x0; zeros((2^Nlog - N)/2, 1); zeros(2^Nlog, 1)];

N = length(x0);
Nlog = Nlog + 1;

%%

X = fft(x);
f = linspace(-fs/2, fs/2, 2^Nlog + 1)';
f = f(1:end-1);
f = fftshift(f);
H = zeros(size(X));
X0 = fft(x0);
X0I = fft(x0I);

for n = 1:numSpkrs 
    H(:,n) = X(:,n) .* X0I .* abs(f / fs) / (sqrt(32 * N)) * db2mag(-0.529);
end

h = ifft(H);

H0 = X0 .* X0I .* abs(f/fs) / (sqrt(32 * N)) * db2mag(-0.529);
h0 = ifft(H0);

plot(h0)



