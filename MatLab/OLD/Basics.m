clear all, close all, clc

[x0, fs] = audioread('AudioFiles/IR_REF.wav');
[xL, fs] = audioread('AudioFiles/KH_120_L.wav');
[xR, fs] = audioread('AudioFiles/KH_120_R.wav');

Nlog = nextpow2(length(x0));
N = length(x0);

xi = [zeros((2^Nlog - N)/2, 1); flip(x0); zeros((2^Nlog - N)/2, 1); zeros(2^Nlog, 1)];
x0 = [zeros((2^Nlog - N)/2, 1); x0; zeros((2^Nlog - N)/2, 1); zeros(2^Nlog, 1)];
xL = [zeros((2^Nlog - N)/2, 1); xL; zeros((2^Nlog - N)/2, 1); zeros(2^Nlog, 1)];
xR = [zeros((2^Nlog - N)/2, 1); xR; zeros((2^Nlog - N)/2, 1); zeros(2^Nlog, 1)];

X0 = fft(x0);
XI = fft(xi);
XL = fft(xL);
XR = fft(xR);

fi = linspace(-fs/2, fs/2, length(X0) + 1)';
fi = fi(1:end-1);
fi = fftshift(fi);

H0 = XI .* X0 .* abs(fi/fs);
HL = XI .* XL .* abs(fi/fs);
HR = XI .* XR .* abs(fi/fs);

h0 = ifft(H0);
hL = ifft(HL);
hR = ifft(HR);





