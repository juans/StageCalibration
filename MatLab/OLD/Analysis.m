clear all, close all, clc


[x0, fs] = audioread('AudioFiles/IR_REF.wav');
numSpkrs = 16;

x = zeros(length(x0), numSpkrs);

for n = 1:numSpkrs/2
    name = ['AudioFiles/IR_', num2str(n), '.wav'];
    x(:, n) = audioread(name);
    name = ['AudioFiles/IR_', num2str(n), 's.wav'];
    x(:, n+8) = audioread(name);
end

N = length(x0);
Nlog = nextpow2(length(x0));

x = [zeros(2^Nlog - N, numSpkrs); x; zeros(2^Nlog, numSpkrs)];
x0I = [zeros(2^Nlog - N, 1); flip(x0); zeros(2^Nlog, 1)];
x0 = [zeros(2^Nlog - N, 1); x0; zeros(2^Nlog, 1)];

clear N name;

%%

delay = zeros(numSpkrs, 1);

for n = 1:numSpkrs
    delay(n) = finddelay(x0, x(:,n));
%     x(:, n) = circshift(x(:, n), -delay(n));
end

%%

X = fft(x);
f = linspace(-fs/2, fs/2, 2^(Nlog + 1) + 1)';
f = f(1:end-1);
f = fftshift(f);
HPre = zeros(size(X));
X0I = fft(x0I);

for n = 1:numSpkrs
    HPre(:,n) = X(:, n) .* X0I .* abs(f/fs);
end

hPre = ifft(HPre);

%%

close all;

[val, pos] = max(abs(hPre));
lenIR = 2^10;
winLen = 2^10;
win = hann(winLen);
win = [win(1:winLen/2); ones(lenIR - winLen, 1); win(winLen/2+1:end)];
h = hPre(min(pos) - winLen/2:min(pos) + lenIR - winLen/2 - 1,:);% .* win;

del = pos - min(pos);
delM = [-00 * ones(8, 1); 900 * ones(8, 1)];
g = [1;1;1;1;-1;1;1;1;...
     -1;-1;-1;1;-1;-1;-1;-1];

figure(1);
for n = 1:numSpkrs
    h(:,n) = g(n) * h(:,n) / 550;
    h(:,n) = circshift(h(:,n), -del(n) - winLen/2 - delM(n));
%     plot(win + 2 * (n - 1), ':', 'color', 'k');
    hold on;
%     plot(h(:,n) + 2 * (n - 1));
    plot(mag2db(abs(h(:,n))) + 200 * (n - 1));
end

H = fft(h, lenIR * 8);

Hmag = mag2db(abs(H(1:end/2,:)));
Hang = rad2deg(unwrap(angle(H(1:end/2,:))));
fax = linspace(0, fs/2, length(Hmag) + 1);
fax = fax(1:end-1);

figure(2);
for n = 1:numSpkrs
    subplot 211
    semilogx(fax, Hmag(:,n));
    grid on;
    hold on;
    xlim([2e1, 2e3]);
    ylim([-50, 20]);
    
    subplot 212
    semilogx(fax(1:end-1), diff(Hang(:,n)));
    grid on;
    hold on;
    xlim([2e1, 2e3]);
end


