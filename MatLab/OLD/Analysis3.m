clear all, close all, clc

[x0, fs] = audioread('AudioFiles/IR_REF.wav');
[x1, fs] = audioread('AudioFiles/IR_1.wav');

Nlog = nextpow2(length(x0));

x0 = [x0; zeros(2^Nlog - length(x0), 1); zeros(2^Nlog, 1)];
x1 = [zeros(2^Nlog, 1); x1; zeros(2^Nlog - length(x1), 1)];

X0 = fft(x0);
X1 = fft(x1);

H0 = X1 ./ X0;

h0 = ifft(H0);

