clear all, close all, clc

[x0, fs] = audioread('AudioFiles/IR_REF.wav');
[x1, fs] = audioread('AudioFiles/IR_1.wav');

N = length(x0);
Nlog = nextpow2(N);

xi = [zeros((2^Nlog - N)/2, 1); flip(x0); zeros((2^Nlog - N)/2, 1); zeros(2^Nlog, 1)];
x0 = [zeros((2^Nlog - N)/2, 1); x0; zeros((2^Nlog - N)/2, 1); zeros(2^Nlog, 1)];
x1 = [zeros((2^Nlog - N)/2, 1); x1; zeros((2^Nlog - N)/2, 1); zeros(2^Nlog, 1)];

N = length(x0);
Nlog = log2(N);

Xi = fft(xi);
X0 = fft(x0);
X1 = fft(x1);
fi = linspace(-fs/2, fs/2, length(Xi) + 1)';
fi = fi(1:end-1);
fi = fftshift(fi);

H0 = Xi .* X0 .* abs(fi/fs) / (sqrt(32 * N)) * db2mag(-0.529);
H1 = Xi .* X1 .* abs(fi/fs) / (sqrt(32 * N)) * db2mag(-0.529);


h0 = ifft(H0);
h1 = ifft(H1);

winLen = 2^8;
irLen = 2^16;

winBase = hann(winLen);
win = [winBase(1:end/2); ones(irLen - winLen, 1); winBase(end/2 + 1: end)];
del = finddelay(h1, h0);
h1 = circshift(h1, del);
h = h1(end/2 - winLen/2:end/2 - winLen/2 + irLen - 1) .*win;


for d = 0:fs
    H = fft(h);
    Hmag = mag2db(abs(H(1:end/2)));
    Hang = rad2deg(angle(H(1:end/2)));
    fax  = linspace(0, fs/2, length(Hmag) + 1);
    fax  = fax(1:end-1);
    tax  = linspace(0,length(h)/fs,length(h) + 1);
    tax  = tax(1:end-1);

    subplot 311
    plot(tax, h);
    line([d/fs,d/fs],[min(h), max(h)], 'color', 'k', 'linestyle', ':');
    grid on;
    subplot 312
    semilogx(fax, Hmag);
    grid on;
    xlim([2e1, 2e4]);
    ylim([-60, 10]);
    subplot 313
    semilogx(fax, Hang);
    grid on;
    xlim([2e1, 2e4]);
    ylim([-180, 180]);
    pause(0.1);

end


% h = zeros(size(h1));
% h(end/2 - winLen/2:end/2 - winLen/2 + irLen - 1) = h1(end/2 - winLen/2:end/2 - winLen/2 + irLen - 1).*win;
% shInt = flip(cumsum(flip((h + eps))));
% plot(mag2db(abs(h)))









