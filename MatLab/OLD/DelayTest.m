clear all, close all, clc;

[x0, fs] = audioread('AudioFiles/IR_REF.wav');
[x, fs] = audioread('AudioFiles/IR_1.wav');
irLen = 2^20;
winLen = 2^8;
win = hann(winLen);

h = getIRFromChirp(x0, x, fs, win(1:end/2), win(end/2+1:end), irLen);

H = fft(h);
H = H(1:end/2);
N = length(H);

fax = linspace(0, fs/2, N + 1)';
fax = fax(1:end-1);


Hdel = exp(1j * 2 * pi * fax * 0.1/1000);
for n = 1:1000
    H = H .* Hdel;
    
    Hmag = mag2db(abs(H));
    Hang = rad2deg(angle(H));

    subplot 211
    semilogx(fax, Hmag);
    grid on;
    xlim([1e1, 2e3]);
    ylim([-60, 10]);

    subplot 212
    semilogx(fax, Hang);
    grid on;
    xlim([1e1, 2e3]);
    ylim([-180, 180]);
    pause(0.1);
end