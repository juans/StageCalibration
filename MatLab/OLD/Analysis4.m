clear all, close all, clc

[x0, fs] = audioread('AudioFiles/IR_REF.wav');
x1 = audioread('AudioFiles/IR_1.wav');

% C = xcorr(x0, 
del = finddelay(x0, x1);

Nlog = nextpow2(length(x0));

x0 = [zeros(2^Nlog + del, 1); x0; zeros(2^Nlog - length(x0) - del, 1)];
% x0 = [zeros(2^Nlog, 1); x0; zeros(2^Nlog - length(x0), 1)];
x1 = [zeros(2^Nlog, 1); x1; zeros(2^Nlog - length(x1), 1)];

X0 = fft(x0);
X1 = fft(x1);

HPre = X1 ./ X0;

hPre = real(ifft(HPre));

[maxVal, maxPos] = max(hPre);

h = hPre(maxPos:maxPos + 1023);
H = fft(h, 2^Nlog);


Hmag = mag2db(abs(H(1:end/2)));
Hang = rad2deg(angle(H(1:end/2)));
fax = linspace(0, fs/2, length(Hmag) + 1);
fax = fax(1:end-1);

subplot 311
plot(h);
subplot 312
semilogx(fax, Hmag);
grid on;
subplot 313
semilogx(fax, Hang);
grid on; 





% plot(h);


