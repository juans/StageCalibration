clear all, close all, clc


[x0, fs] = audioread('AudioFiles/IR_REF.wav');
numSpkrs = 16;

x = zeros(length(x0), numSpkrs);

for n = 1:numSpkrs/2
    name = ['AudioFiles/IR_', num2str(n), '.wav'];
    x(:, n) = audioread(name);
    name = ['AudioFiles/IR_', num2str(n), 's.wav'];
    x(:, n+8) = audioread(name);
end

N = length(x0);
Nlog = nextpow2(length(x0));

clear N name;

irLen = 2^18;
winLen = 2^8;
win = hann(winLen);

h = zeros(irLen, numSpkrs);

for n = 1:16
    h(:,n) = getIRFromChirp(x0, x(:,n), fs, win(1:end/2), win(end/2+1:end), irLen);    
end


H = fft(h);
Hmag = mag2db(abs(H(1:end/2,:)));
Hang = rad2deg(angle(H(1:end/2,:)));
fax = linspace(0, fs/2, irLen/2 + 1);
fax = fax(1:end - 1);

subplot 211
semilogx(fax, Hmag);
grid on;
xlim([1e1, 2e3]);
ylim([-60, 10]);
subplot 212
semilogx(fax, Hang);
grid on;
xlim([1e1, 2e3]);
ylim([-180, 180]);




