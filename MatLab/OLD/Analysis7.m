[x, fs] = audioread('AudioFiles/IR_REF.wav');

y = flip(x);

X = fft(x, length(x) * 2);
Y = fft(y, length(y) * 2);
fax = linspace(-fs/2, fs/2, length(X) + 1)';
fax = fax(1:end-1);
fax = fftshift(fax);

H = X .* Y .* abs(fax / fs);
h = real(ifft(H));