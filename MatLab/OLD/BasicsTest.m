clear all, 
% close all, 
clc

[x0, fs] = audioread('AudioFiles/IR_REF.wav');
[xL, fs] = audioread('AudioFiles/KH_120_L.wav');
[xR, fs] = audioread('AudioFiles/KH_120_R.wav');

winLen = 2^4;
irLen = 2^14;
zPFact = 8;

win = hamming(winLen);

h0 = getIRFromChirp(x0, x0, fs, win(1:end/2), win(end/2+1:end), irLen);
hL = getIRFromChirp(x0, xL, fs, win(1:end/2), win(end/2+1:end), irLen);
hR = getIRFromChirp(x0, xR, fs, win(1:end/2), win(end/2+1:end), irLen);

H0 = fft(h0, irLen * zPFact);
HL = fft(hL, irLen * zPFact);
HR = fft(hR, irLen * zPFact);

H0 = H0(1:end/2);
HL = HL(1:end/2);
HR = HR(1:end/2);

H0 = pureDelay(H0, 'samp',   8, fs);
HL = pureDelay(HL, 'samp',   8, fs);
HR = pureDelay(HR, 'samp', 7.3, fs);

fc = 100;
wc = 100 * 2 * pi;

[b, a] = butter(4, wc, 's');

H0 = applyAnalogFilter(b, a, H0, fs);
HL = applyAnalogFilter(b, a, HL, fs);
HR = applyAnalogFilter(b, a, HR, fs);

octFrac = 1/6;
H0 = cBwSmoother(H0, octFrac, 'comp');
HL = cBwSmoother(HL, octFrac, 'comp');
HR = cBwSmoother(HR, octFrac, 'comp');

H0Mag = mag2db(abs(H0));
HLMag = mag2db(abs(HL));
HRMag = mag2db(abs(HR));
H0Ang = rad2deg(angle(H0));
HLAng = rad2deg(angle(HL));
HRAng = rad2deg(angle(HR));

fax = linspace(0, fs/2, length(H0) + 1)';
fax = fax(1:end-1);

subplot 211
hold off
semilogx(fax, H0Mag);
hold on
semilogx(fax, HLMag);
semilogx(fax, HRMag);
grid on
ylim([-60, 6]);
xlim([1e1, 2e4]);

subplot 212
hold off
semilogx(fax, H0Ang);
hold on
semilogx(fax, HLAng);
semilogx(fax, HRAng);
grid on
xlim([1e1, 2e4]);

