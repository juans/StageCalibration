clear all, close all, clc
fs = 44100;
dur = 10;
t=linspace(0, 10, fs * dur + 1);
fo=10;
f1=20000/3;
x0=chirp(t,fo,10,f1,'logarithmic')';
x0=[x0; zeros(3 * fs, 1)];


% Apply Filtering
fc = 1000; 
wc = 2 * pi * fc;
g = db2mag(20);
Q = 1;

ba = [1/wc^2, g * Q/wc, 1];
aa = [1/wc^2, 1/wc, 1];

[bd, ad] = bilinear(ba, aa, fs);

x1 = filter(bd, ad, x0);

% Apply Distortion
x1 = x1.^3;

% Apply Gain
x1 = x1 * db2mag(-12);

% Apply Delay
x1 = circshift(x1, fs * 0.1);

% Apply Noise
x1 = x1 + db2mag(-40) * randn(size(x1));

chirpRef = x0;
chirpRsp = x1;


%%
x0 = chirpRef;
x1 = chirpRsp;


Nlog = nextpow2(length(x0));

x0 = [x0; zeros(2^Nlog - length(x0), 1); zeros(2^Nlog, 1)];
x1 = [zeros(2^Nlog, 1); x1; zeros(2^Nlog - length(x1), 1)];

X1 = fft(x1);
X0 = fft(x0);

H = X1 ./ X0;

h = real(ifft(H));

plot(h);



