clear all, close all, clc

[x0, fs] = audioread('AudioFiles/IR_REF.wav');

y0 = flip(x0);

h0 = conv(x0, y0);

%%

H0 = fft(h0);

H0 = H0(1:end/2);

fax = linspace(0, fs/2, length(H0) + 1);
fax = fax(1:end-1);
H0Mag = mag2db(abs(H0));
H0Ang = rad2deg(angle(H0));

subplot 211
semilogx(fax, H0Mag);

subplot 212
semilogx(fax, H0Ang);