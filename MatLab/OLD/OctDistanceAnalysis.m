clear all
close all
clc

[x0, fs] = audioread('AudioFiles/IR_REF.wav');

fmax = 20000;
fmin = 20;
ssLen = 10 * fs;
base = (fmax/fmin)^(1/(10 * fs));
octLen = floor(log(2)/log(base));

s1 = x0(5 * octLen + 1: 5 * octLen + 128);
s2 = x0(6 * octLen + 1: 6 * octLen + 128);

win = hann(128);

s1 = s1 .* win;
s2 = s2 .* win;

S1 = fft(s1, 2^16);
S2 = fft(s2, 2^16);
S1 = mag2db(abs(S1(1:end/2)));
S2 = mag2db(abs(S2(1:end/2)));

[~,f1Idx] = max(S1);
[~,f2Idx] = max(S2);

fax = linspace(0, fs/2, length(S1) + 1);
fax = fax(1:end-1);

f1 = fax(f1Idx);
f2 = fax(f2Idx);

subplot 211
semilogx(fax, S1);
grid on;

subplot 212
semilogx(fax, S2);
grid on;