clear all, close all, clc

[x, fs] = audioread('AudioFiles/IR_REF.wav');

X = fft(x) / sqrt(length(x));
fax = linspace(0, fs/2, length(X)/2 + 1);
fax = fax(1:end-1);
Y = X(1:end/2) .* fax'/fs/2;% .* (fax < 16000)' .* (fax > 30)';
Y = [Y; flip(Y)];

y = ifft(Y);

Y = fft(y, length(y) * 2);
X = fft(x, length(x) * 2);

H = X .* Y;

h = ifft(H);

plot(real(h))




