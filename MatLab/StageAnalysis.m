clear all, close all, clc

addpath('DSPFunc');


[x0, fs] = audioread('AudioFiles/IR_REF.wav');
numSpkrs = 16;

x = zeros(length(x0), numSpkrs);
ids = {};

for n = 1:numSpkrs/2
    name = ['AudioFiles/IR_', num2str(n), '.wav'];
    x(:, n) = audioread(name);
    ids(n) = cellstr(['Main', num2str(n)]);
    name = ['AudioFiles/IR_', num2str(n), 's.wav'];
    x(:, n+8) = audioread(name);
    ids(n+8) = cellstr(['Sub', num2str(n)]);
end

N = length(x0);
Nlog = nextpow2(length(x0));

x = [zeros(2^Nlog - N, numSpkrs); x; zeros(2^Nlog, numSpkrs)];
x0I = [zeros(2^Nlog - N, 1); flip(x0); zeros(2^Nlog, 1)];
x0 = [zeros(2^Nlog - N, 1); x0; zeros(2^Nlog, 1)];

clear N name;

%%

winLen = 2^4;
irLen = 2^10;
zPFact = 8;

win = hamming(winLen);

h0 = getIRFromChirp(x0, x0, fs, win, irLen);
h = zeros(irLen, numSpkrs);
for n = 1:numSpkrs
    h(:, n) = getIRFromChirp(x0, x(:,n), fs, win, irLen);
end

%%

hOpt = optimizeIrLengths(h, win);

%%
H0 = fft(h0, irLen * zPFact);
H  = fft(h , irLen * zPFact);

H0 = H0(1:end/2);
H  = H (1:end/2,:);

H0 = pureDelay(H0, 'samp', winLen/2, fs);
for n = 1:numSpkrs
    H(:,n) = pureDelay(H(:,n), 'samp', winLen/2, fs);
end

HPre = H;

%%

H = HPre;

%================== This Space is ready for processing ================== %


% ======= 1      2       3       4       5       6       7       8 =======%
% ======= 9     10      11      12      13      14      15      16 =======%
gains = [15,    15,     15,     15,     15,     15,     15,     15,...
         15,    15,     15,     15,     15,     15,     15,     15];
delays= [ 0,     0,      0,      0,      0,      0,      0,      0,...
          0,     0,      0,      0,      0,      0,      0,      0];
pol   = [ 1,     1,      1,      1,     -1,      1,      1,      1,...
         -1,    -1,     -1,      1,     -1,     -1,     -1,     -1];
doPlot= [ 1,     1,      1,      1,      1,      1,      1,      1,...
          1,     1,      1,      1,      1,      1,      1,      1];

H = pureDelay(H .* db2mag(gains) .* pol .* doPlot, 'samp', delays, fs);

[b, a] = butter(5, 180 * 2 * pi, 's');
H(:,9:16) = applyAnalogFilter(b, a, H(:,9:16), fs);


octFrac = 1/2;
H0Smooth = cBwSmoother(H0, octFrac, 'comp');
HSmooth = zeros(irLen * zPFact / 2, numSpkrs);
for n = 1:numSpkrs
    HSmooth(:,n) = cBwSmoother(H(:, n), octFrac, 'comp');
end


H0Mag = mag2db(abs(H0Smooth));
H0Ang = angle(H0Smooth);
HMag = mag2db(abs(HSmooth));
HAng = angle(HSmooth);

fax = linspace(0, fs/2, length(H0) + 1)';
fax = fax(1:end-1);

subplot 221
hold off
semilogx(fax, H0Mag, ':k');
hold on
semilogx(fax, HMag(:, 1:8));
grid on;
ylim([-40, 6]);
xlim([2e1, 2e4]);
legend(['ref', ids(1:8)]);

subplot 223
hold off
semilogx(fax, H0Ang, ':k');
hold on
semilogx(fax, HAng(:, 1:8));
grid on;
xlim([2e1, 2e4]);
legend(['ref', ids(1:8)]);
semilogx(fax, HAng(:, 9:16), ':');

subplot 222
hold off
semilogx(fax, H0Mag, ':k');
hold on
semilogx(fax, HMag(:, 9:16));
grid on;
ylim([-40, 6]);
xlim([2e1, 2e4]);
legend(['ref', ids(9:16)]);

subplot 224
hold off
semilogx(fax, H0Ang, ':k');
hold on
semilogx(fax, HAng(:, 9:16));
grid on;
xlim([2e1, 2e4]);
legend(['ref', ids(9:16)]);



