clear all, close all, clc

addpath('../DSPFunc');


[ref, fs] = audioread('../AudioFiles/GroundPlane/32Bit/NzRef-A77x-18.wav');
res = audioread('../AudioFiles/GroundPlane/32Bit/NzRes-A77x-18.wav');

[H, c] = nzToTF(ref, res, 2^16, 2^7);
hRaw = real(ifft(H));

H = H(1:end/2);
H = applyDelay(H, 'm', -1.06, fs);
fax = freqAxis(fs, length(H));
g = abs(H) \ (1 * ones(size(H)));
H = H * g;
HRaw = H;
hRaw = hRaw * g;
H = tfSmoother(fax, H, fs, 1, 'smoothing', 'erb');

HPre = H;

%%

H = HPre;

HLPF = applyButterworth(5, 45, ones(size(fax)), fs, 'high');
invH = 1./H;

firLen = 256;
b = firls(firLen, fax/fax(end), abs(1./H) .* abs(HLPF), abs(HLPF(1:2:end)).^2);
b = b(1:end-1)';
bMin = minPhaseFilt(b);
B = fft(bMin, length(H) * 2);
B = B(1:end/2);

% tfPlot(fax, H);
% tfPlot(fax, HLPF)
% tfPlot(fax, invH);
% tfPlot(fax, invH .* abs(HLPF))
figure(1)
tfPlot(fax, HRaw)
tfPlot(fax, B)
tfPlot(fax, HRaw .* B)
figure(2)
tax = timeAxis(fs, length(b));
plot(tax, mag2db(abs(b)))
hold on
plot(tax, mag2db(abs(bMin)))
% ylim([-1, 1]);
grid on;

%%

close all

subplot 321
plot(hRaw)
grid on

subplot 322

subplot 323
plot(b)
grid on

subplot 324
plot(conv(hRaw, b));
grid on

subplot 325
plot(bMin)
grid on

subplot 326
plot(conv(hRaw, bMin))
grid on
