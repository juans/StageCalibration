clear all, close all, clc

addpath('../DSPFunc');


[ref, fs] = audioread('../AudioFiles/GroundPlane/32Bit/NzRef-A77x-18.wav');
res = audioread('../AudioFiles/GroundPlane/32Bit/NzRes-A77x-18.wav');

[H, c] = nzToTF(ref, res, 2^16, 2^7);
hRaw = real(ifft(H));

H = H(1:end/2);
H = applyDelay(H, 'm', -1.06, fs);
fax = freqAxis(fs, length(H));
g = abs(H) \ (1 * ones(size(H)));
H = H * g;
HRaw = H;
hRaw = hRaw * g;
H = tfSmoother(fax, H, fs, 1, 'smoothing', 'erb');

HPre = H;
%%

H = HPre;

HLPF = applyButterworth(5, 45, ones(size(fax)), fs, 'high');
invH = 1./H .* abs(HLPF);


firLen = 128;
br     = firls(firLen, fax/fax(end), real(invH), abs(HLPF(1:2:end)).^2);
bi     = firls(firLen, fax/fax(end), imag(invH), abs(HLPF(1:2:end)).^2, 'hilbert');
bc     = br + bi;

brConj = firls(firLen, fax/fax(end),  real(invH), abs(HLPF(1:2:end)).^2);
biConj = firls(firLen, fax/fax(end), -imag(invH), abs(HLPF(1:2:end)).^2, 'hilbert');
bcConj = brConj + biConj;


%%

close all;

Bc = fft(bc, 2 * length(H))';
Bc = Bc(1:end/2);
BcConj = fft(bcConj, 2 * length(H))';
BcConj = BcConj(1:end/2);

[~, d] = max(bc);
Bc = applyDelay(Bc, 'samp', d, fs);
BcConj = applyDelay(BcConj, 'samp', d, fs);

figure()
tfPlot(fax, H);
tfPlot(fax, invH);
figure()
tfPlot(fax, Bc);
tfPlot(fax, Bc .* H)
figure()
tfPlot(fax, BcConj)
tfPlot(fax, BcConj .* H)
figure()
d = d/2;
subplot 311
plot(hRaw)
[~, idx] = max(hRaw);
xlim([idx - d, idx + d])
grid on
subplot 312
plot(bc)
[~, idx] = max(bc);
xlim([idx - d, idx + d])
grid on
subplot 313
hRawbc = conv(hRaw, flip(bc));
plot(hRawbc)
[~, idx] = max(hRawbc);
xlim([idx - d, idx + d])
grid on


%%

Hang = angle(H);
Hang = unwrap(Hang);

figure()
plot(Hang)

