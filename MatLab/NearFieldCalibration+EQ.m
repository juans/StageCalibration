clear all, close all, clc;


addpath('../DSPFunc');
[x, fs] = audioread('FullRange.aiff');
load('../GEq/GraphicEQ.mat');

HEq = HTs;
clear HTs;

x = x(:,1);

X = fft(x, 2^17);
X = X(1:end/2);

d = squeeze(argmax(x));
X = applyDelay(X, 'samp', -d, fs);
X = applyButterworth(4, 15, X, fs, 'high');

fax = freqAxis(fs, length(X));
XS = tfSmoother(fax, X, fs, 1/4, 'erb');






H1 = XS + eps;
g = abs(H1) \ ones(size(H1));
H1 = H1 * g;
H1mag = mag2db( abs(H1) );
HEqdB = mag2db( abs(HEq) );
[b, a] = butter(4, 100, 'high', 's');
w1 = abs(freqs(b, a, fax)); 
[b, a] = butter(4, 16000, 's');
w2 = abs(freqs(b, a, fax));

g = lsqlin(HEqdB, -(H1mag.*w1.*w2), eye(28), 24 * ones(28, 1));

g = round(g * 2) / 2;

HEqProc = 10.^(log10(HEq) * diag(g));

HT = prod([X, HEqProc], 2);

HS = tfSmoother(fax, HT, fs, 1/4, 'erb');

tfProcessingPlot(fax, [XS, HS], HEqProc);



