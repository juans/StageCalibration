clear all
close all
clc

[x0, fs] = audioread('AudioFiles/IR_REF.wav');
xL = audioread('AudioFiles/IR_1.wav');
xR = audioread('AudioFiles/IR_2.wav');
xS = audioread('AudioFiles/IR_1S.wav');
vD = volterraDistance(2, 20000, 20, fs, 10);


h0 = chirpToIR(x0, x0);
hL = chirpToIR(x0, xL);
hR = chirpToIR(x0, xR);
hS = chirpToIR(x0, xS);
hi = zeros(size(h0));
hi(end/2) = 1;

dL = finddelay(hL, h0);
dR = finddelay(hR, h0);
dS = finddelay(hS, h0);

hL = circshift(hL, dL);
hR = circshift(hR, dR);
hS = circshift(hS, dS);



hLOpt = optimizeIrLengths(hL, vD, -20);
hROpt = optimizeIrLengths(hR, vD, -20);
hSOpt = optimizeIrLengths(hS, vD, -20);





