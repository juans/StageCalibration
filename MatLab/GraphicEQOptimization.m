clear all, close all, clc

addpath('../DSPFunc');

load('../GEq/GraphicEQ.mat')
HGeq = HTs;

base = 'AudioFiles/RoomSineSweep/32Bit/IR_';

[x0, fs] = audioread([base, 'REF.wav']);
numSpkrs = 16;

x = zeros(length(x0), numSpkrs);
ids = {};

for n = 1:numSpkrs/2
    name = [base, num2str(n), '.wav'];
    x(:, n) = audioread(name);
    ids(n) = cellstr(['Main', num2str(n)]);
    name = [base, num2str(n), 's.wav'];
    x(:, n+8) = audioread(name);
    ids(n+8) = cellstr(['Sub', num2str(n)]);
end

vD = volterraDistance(2, 20000, 20, fs, 10);

N = length(x0);
Nlog = nextpow2(length(x0));

%%


h0 = chirpToIR(x0, x0);
h = zeros(length(h0), numSpkrs);
d = zeros(1, numSpkrs);
for n = 1:numSpkrs
    h(:,n) = chirpToIR(x0, x(:,n)); 
    d(n) = finddelay(h(:,n), h0);
    h(:,n) = circshift(h(:,n), d(n));
%     h(:,n) = circshift(h(:,n), -700);
end

clear N name;

%%


% hOpt = zeros(size(h));
% for n = 1:numSpkrs
%     hOpt(:,n) = optimizeIrLengths(h(:,n), vD, -20);
% end


%%

winLen = 2^4;
irLen = 2^7;
irPad = 2^12;
preRoll = 2^3;
postRoll = 2^5;

winStart = hann(preRoll * 2);
winStart = winStart(1:end/2);
winEnd = hann(postRoll * 2);
winEnd = winEnd(end/2+1:end);

win = [winStart; ones(irLen - preRoll - postRoll, 1); winEnd];

%%

h0Opt = zeros(irPad * 2, 1);
h0Opt(1:irLen) = h0(end/2 - preRoll + 1:end/2 - preRoll + irLen) .* win;
h0Opt = circshift(h0Opt, -preRoll + 1);
hOpt = zeros(irPad * 2, numSpkrs);
for n = 1:numSpkrs
    hOpt(1:irLen,n) = h(end/2 - preRoll + 1:end/2 - preRoll + irLen, n) .* win;
    hOpt(:,n) = circshift(hOpt(:,n), -preRoll + 1);
end

%%
H0 = fft(h0Opt);
H  = fft(hOpt);

H0 = H0(1:end/2);
H  = H (1:end/2,:);

HPre = H;
HEq = HTs(1:16:end, :);

fax = linspace(0, fs/2, length(H) + 1)';
fax = fax(1:end-1);

%%

close all;

H1 = H(:, 1);
x = abs(H1) \ ones(size(H1));
H1 = H1 * x;
H1mag = mag2db( abs(H1) );
HEqdB = mag2db( abs(HEq) );
[b, a] = butter(4, 100, 'high', 's');
w1 = abs(freqs(b, a, fax))'; 
[b, a] = butter(4, 16000, 's');
w2 = abs(freqs(b, a, fax))';

x = lsqlin(HEqdB, -(H1mag.*w1.*w2), eye(28), 24 * ones(28, 1));

x = round(x * 2) / 2;

HEqProc = 10.^(log10(HEq) * diag(x));

HT = prod([H1, HEqProc], 2);


% figure()
% tfPlot(fax, H1);
% % figure()
% tfPlot(fax, HEqProc);
% % figure()
% tfPlot(fax, HT);
figure()
tfProcessingPlot(fax, [H1, HT], HEqProc);







