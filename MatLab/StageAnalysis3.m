clear all, close all, clc

addpath('../DSPFunc');

base = 'AudioFiles/RoomIRs/Center/Windowed/';
[x, fs] = audioread([base, '1.wav']);
numSpkrs = 8;
N = length(x);

clear x;

h = zeros(N, numSpkrs);
hS= zeros(N, numSpkrs);

for i = 1:numSpkrs
    h(:,i)  = audioread([base, num2str(i), '.wav']);
    hS(:,i) = audioread([base, num2str(i), 's.wav']);
end

%%

winLen = 2^4;
irLen = 2^8;
preRoll = 2^3;
postRoll = 2^5;

winStart = hann(preRoll * 2);
winStart = winStart(1:end/2);
winEnd = hann(postRoll * 2);
winEnd = winEnd(end/2+1:end);

win = [winStart; ones(irLen - preRoll - postRoll, 1); winEnd];

%%

d = argmax(h);

hOpt = zeros(2 * N, numSpkrs);
hSOpt= zeros(2 * N, numSpkrs);
for i = 1:numSpkrs
    hOpt(1:irLen,i) = h(d(i) - preRoll + 1:d(i) - preRoll + irLen, i) .* win;
    hOpt(:,i) = circshift(hOpt(:,i), -preRoll + 1);
    hSOpt(1:irLen,i) = hS(d(i) - preRoll + 1:d(i) - preRoll + irLen, i) .* win;
    hSOpt(:,i) = circshift(hSOpt(:,i), -preRoll + 1);
end

%%

H = fft(hOpt);
HS = fft(hSOpt);

H = H(1:end/2,:);
HS = HS(1:end/2,:);



fax = freqAxis(fs, N);
%%
t = conv(hOpt(:, 1), ones(32, 1));

tx = xcorr(t, t);

[pks, idx] = findPeaks(tx, 2);

hK = zeros(diff(idx), 1);
hK(1) = sqrt(pks(1));
hK(end) = sqrt(pks(2));

HK = fft(hK, 2 * N);
HK = HK(1:end/2);
HKinv = 1./conj(HK);

% tfPlot(fax, HK);

firLen = 256;

hr = firls(firLen, fax / fax(end), real(HKinv));
hi = firls(firLen, fax / fax(end), -imag(HKinv), 'Hilbert');

hc = hr + hi;

HC = fft(hc, 2 * N);
HC = HC(1:end/2);


%%
tfPlot(fax, HK);
tfPlot(fax, HC);
tfPlot(fax, HK.*HC');

% plot(tx);
% Hs = tfSmoother(fax, H, fs, 1/2, 'erb');
% tfPlot(fax, H);





