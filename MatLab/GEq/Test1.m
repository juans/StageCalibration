clear all, close all, clc

addpath('../DSPFunc');

numFreqs = 28;
freqs = [31.5, 40, 50, 63, 80, 100, 125, 160, 200, 250, 315, 400, 500, 630, 800, 1000, 1250, 1600, 2000, 2500, 3150, 4000, 5000, 6300, 8000, 10000, 12500, 16000];
baseFolder = '../AudioFiles/dbx/GraphicEQ/';
[x0, fs] = audioread([baseFolder, '../Ref-Flat.wav']);
y0 = audioread([baseFolder, '../Res-Flat.wav']);

x = zeros(length(x0), numFreqs);
y = zeros(length(x0), numFreqs);


for i = 1:numFreqs
    x(:, i) = audioread([baseFolder, 'Ref-', num2str(freqs(i)), '+1.wav']);
    y(:, i) = audioread([baseFolder, 'Res-', num2str(freqs(i)), '+1.wav']);
end

%%

close all
clc

x1 = x;
y1 = y;

% h0 = nzToIr(x0, y0, 2^16, 2^8);
% h = nzToIr(x1, y1, 2^16, 2^8);

[H0, c0] = nzToTF(x0, y0, 2^16, 2^8);
[H, c] = nzToTF(x1, y1, 2^16, 2^8);

%%

[h, fs] = audioread('/Users/juans/Downloads/StudioEFilters/1.wav');

h0 = ifft(H0);
h = ifft(H);

[~, delay] = max(h0);

h = circshift(h, -delay + 1);
h0 = circshift(h0, -delay + 1);

H0 = fft(h0);
H0 = H0(1:end/2);

H = fft(h);
H = H(1:end/2, :);

%%

close all

HT = (H ./ H0);

fax = linspace(0, fs/2, length(H) + 1)';
fax = fax(1:end-1);

HTs = tfSmoother(fax, HT, fs, 1/12);

tfPlot(fax, HTs, 'ylim', [-1, 1]);




