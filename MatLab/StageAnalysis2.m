clear all, close all, clc

addpath('DSPFunc');

base = 'AudioFiles/RoomSineSweep/32Bit/IR_';

[x0, fs] = audioread([base, 'REF.wav']);
numSpkrs = 16;

x = zeros(length(x0), numSpkrs);
ids = {};

for n = 1:numSpkrs/2
    name = [base, num2str(n), '.wav'];
    x(:, n) = audioread(name);
    ids(n) = cellstr(['Main', num2str(n)]);
    name = [base, num2str(n), 's.wav'];
    x(:, n+8) = audioread(name);
    ids(n+8) = cellstr(['Sub', num2str(n)]);
end

vD = volterraDistance(2, 20000, 20, fs, 10);

N = length(x0);
Nlog = nextpow2(length(x0));

%%


h0 = chirpToIR(x0, x0);
h = zeros(length(h0), numSpkrs);
d = zeros(1, numSpkrs);
for n = 1:numSpkrs
    h(:,n) = chirpToIR(x0, x(:,n)); 
    d(n) = finddelay(h(:,n), h0);
    h(:,n) = circshift(h(:,n), d(n));
%     h(:,n) = circshift(h(:,n), -700);
end

clear N name;

%%


% hOpt = zeros(size(h));
% for n = 1:numSpkrs
%     hOpt(:,n) = optimizeIrLengths(h(:,n), vD, -20);
% end


%%

winLen = 2^4;
irLen = 2^7;
irPad = 2^12;
preRoll = 2^3;
postRoll = 2^5;

winStart = hann(preRoll * 2);
winStart = winStart(1:end/2);
winEnd = hann(postRoll * 2);
winEnd = winEnd(end/2+1:end);

win = [winStart; ones(irLen - preRoll - postRoll, 1); winEnd];

%%

h0Opt = zeros(irPad * 2, 1);
h0Opt(1:irLen) = h0(end/2 - preRoll + 1:end/2 - preRoll + irLen) .* win;
h0Opt = circshift(h0Opt, -preRoll + 1);
hOpt = zeros(irPad * 2, numSpkrs);
for n = 1:numSpkrs
    hOpt(1:irLen,n) = h(end/2 - preRoll + 1:end/2 - preRoll + irLen, n) .* win;
    hOpt(:,n) = circshift(hOpt(:,n), -preRoll + 1);
end

%%
H0 = fft(h0Opt);
H  = fft(hOpt);

H0 = H0(1:end/2);
H  = H (1:end/2,:);

HPre = H;

%%

H = HPre;

%================== This Space is ready for processing ================== %


%======= 1      2       3       4       5       6       7       8 =======%
%======= 9     10      11      12      13      14      15      16 =======%
gains = [ 0,     0,   -1.5,      0,      0,      0,      0,      0,...
         -1,    -2,   +1.5,     +2,      0,   -1.5,      0,   -0.5];
     
gGains= [ 0 * ones(1, 8), 2 * ones(1, 8)];

gGain = 15;

delays= [ 0,     0,      0,      0,      0,      0,      0,      0,...
          0,     0,      0,      0,      0,      0,      0,      0];
      
gDelays= [ 16 * ones(1, 8), 0 * ones(1, 8)];

pol   = [ 1,     1,      1,      1,     -1,      1,      1,      1,...
         -1,    -1,     -1,      1,     -1,     -1,     -1,     -1];
     
gPol  = [ 1 * ones(1, 8), -1 * ones(1, 8)];

doPlot= [ 1,     1,      1,      1,      1,      1,      1,      1,...
          1,     1,      1,      1,      1,      1,      1,      1];
% doPlot= zeros(1, 16);
% doPlot(1) = 1;
% doPlot(9) = 1;

      
H = H .* db2mag(gains) .* pol .* gPol .* doPlot .* db2mag(gGains) .* db2mag(gGain);
H = pureDelay(H, 'samp', delays, fs);
H = pureDelay(H, 'samp', gDelays, fs);


H(:, 1:8) = applyButterworth(2, 90, H(:, 1:8), fs, 'high'); 
H(:, 1:8) = applyParametric(120, 3, 1, H(:,1:8), fs); 
H(:, 1:8) = applyParametric(1000, -2, 1, H(:,1:8), fs);
H(:, 1:8) = applyParametric(350, -1, 1, H(:,1:8), fs);
H(:, 9:16) = applyLinkWitz(4, 90, H(:, 9:16), fs, 'low');



octFrac = 1/24;
H0Smooth = cBwSmoother(H0, octFrac, 'comp');
HSmooth = zeros(size(H));
for n = 1:numSpkrs
    HSmooth(:,n) = cBwSmoother(H(:, n), octFrac, 'comp');
end

fax = linspace(0, fs/2, length(H0) + 1)';
fax = fax(1:end-1);

close all
ylim = [-20, 6];
tfPlot(fax, H0Smooth, 'ylim', ylim);
tfPlot(fax, HSmooth, 'ylim', ylim);
tfPlot(fax, sum(HSmooth, 2) / sqrt(sum(doPlot)), 'ylim', ylim);
legend(['ref', ids, 'sum'])


