clear all, close all, clc

addpath('../DSPFunc');


[ref, fs] = audioread('../AudioFiles/GroundPlane/32Bit/NzRef-A77x-18.wav');
res = audioread('../AudioFiles/GroundPlane/32Bit/NzRes-A77x-18.wav');

[H, c] = nzToTF(ref, res, 2^16, 2^7);

H = H(1:end/2);
H = applyDelay(H, 'm', -1.06, fs);
fax = freqAxis(fs, length(H));
g = abs(H) \ (1 * ones(size(H)));
H = H * g;
H = tfSmoother(fax, H, fs, 1, 'smoothing', 'erb');

HPre = H;

%%
H = HPre;

% H = tfSmoother(fax, H, fs, 3, 'smoothing', 'erb');

f = [72, 780, 1131, 5450, 2754, 3600, 5430, 10000];
g = [-1,   4, -2.5,    5,    1,  1.5,   -1,    -1];
q = [ 1,   1,    5,    5,  0.8,    1,    5,   0.5];

Hp= applyParametric(f, g, q, ones(size(fax)), fs);
H = applyParametric(f, g, q, H, fs);


% tfPlot(fax, H, 'ylim', [-6, 3])
tfPlot(fax, H)%, 'ylim', [-6, 3])
tfPlot(fax, Hp)
tfPlot(fax, HPre)

%%

H = HPre;

tfPlot(fax, H)