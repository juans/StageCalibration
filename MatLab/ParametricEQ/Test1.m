clear all, close all, clc


addpath('../DSPFunc');

[x0, fs] = audioread('../AudioFiles/RoomSineSweep/32Bit/IR_REF.wav');
x = audioread('../AudioFiles/RoomSineSweep/32Bit/IR_1.wav');

h0 = chirpToIR(x0, x0);
h  = chirpToIR(x0, x);
d = finddelay(x, x0);
h = circshift(h, d);

winLen = 2^4;
irLen = 2^8;
irPad = 2^12;
preRoll = 2^3;
postRoll = 2^5;

winStart = hann(preRoll * 2);
winStart = winStart(1:end/2);
winEnd = hann(postRoll * 2);
winEnd = winEnd(end/2+1:end);

win = [winStart; ones(irLen - preRoll - postRoll, 1); winEnd];

h0Opt = zeros(irPad * 2, 1);
h0Opt(1:irLen) = h0(end/2 - preRoll + 1:end/2 - preRoll + irLen) .* win;
hOpt = zeros(irPad * 2, 1);
hOpt(1:irLen, 1) = h(end/2 - preRoll + 1:end/2 - preRoll + irLen, 1) .* win;
hOpt = circshift(hOpt, -preRoll + 1);

H0 = fft(h0Opt);
H  = fft(hOpt);

H0 = H0(1:end/2);
H  = H (1:end/2);

HPre = H;

plot(h(end/2:end))

%%

H = HPre;
fax = freqAxis(fs, length(H));
H = pureDelay(H, 'samp', 16, fs);

tfPlot(fax, H);





