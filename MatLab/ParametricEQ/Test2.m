clear all, close all, clc

addpath('../DSPFunc');


[ref, fs] = audioread('../AudioFiles/GroundPlane/32Bit/NzRef-A77x-18.wav');
res = audioread('../AudioFiles/GroundPlane/32Bit/NzRes-A77x-18.wav');

[H, c] = nzToTF(ref, res, 2^16, 2^7);


HPre = H;

%%

H = HPre;
H = H(1:end/2);
H = applyDelay(H, 'm', -1.06, fs);
g = abs(H) \ (sqrt(2) * ones(size(H)));
H = H * g;
fax = freqAxis(fs, length(H));
H = tfSmoother(fax, H, fs, 1, 'smoothing', 'erb');

%%

close all;

figure(1)
tfPlot(fax, H);
pause(1);

figure(2)

Ht = applyParametric(1000, 6, 1, ones(size(H)), fs);


gKernel = 6;
qKernel = 10;

% Hmag = mag2db(abs(Ht));

fcMin = 50;
fcMax = 15000;

f = [];
g = [];
q = [];

count = 0;
numFilters = 10;

while (true)
    
    Hmag = mag2db(abs(H));
    
    xCCum = [];
    
    fc = fcMin;
    
    while fc < fcMax

        Hp = applyParametric(fc, gKernel, qKernel, ones(size(H)), fs);
        Hpmag= mag2db(abs(Hp));

        xC = Hpmag' * (Hmag ./ (fax + eps));
        
        xCCum = [xCCum, xC];

        subplot 211
        plot(fax, Hmag);
        hold on
        plot(fax, Hpmag);
        hold off
        set(gca, 'Xscale', 'log');
        grid on;
        xlim([2e1, 2e4]);
        ylim([-10, 10]);


        subplot 212
        semilogx(fc, xC, '.k');
        grid on;
        hold on;
        xlim([2e1, 2e4]);

        pause(0.01)

        fc = fc * 2^(1/12);
    end
    
    [~, idx] = max(abs(xCCum));
    fi = fcMin * 2^(idx / 12);
    fIdx = find(fax > fi, 1);
    gi = Hmag(fIdx);
    if (abs(gi) < 0.1)
        break
    end
    qi = optimalQForTF(fi, gi, fax, H);
    H = applyParametric(fi, -gi, qi, H, fs);
    f = [f, fi];
    g = [g, gi];
    q = [q, qi];
    
    display(['Freq: ', num2str(fi), ' Gain: ', num2str(gi), ' Q: ', num2str(qi)]);
    
    
    count = count + 1;
    
    if (count == numFilters)
        break;
    end
    
end
display('done');

figure(1);
tfPlot(fax, H);

for i = 1:length(f);
    tfPlot(fax, applyParametric(f(i), g(i), q(i), ones(size(fax)), fs));
end



