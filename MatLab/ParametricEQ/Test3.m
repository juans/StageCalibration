clear all, close all, clc

addpath('../DSPfunc');

N = 2^12;
fc = 10000;
fs = 44100;
fax = freqAxis(fs, N);
k = 31;

qError = zeros(k, k);
gLims = [-20, 20];
qLims = [0.01, 100];
gAx = linspace(gLims(1), gLims(2), k)';
qAx = 10.^linspace(log10(qLims(1)), log10(qLims(2)), k);
for j = 1:k
    g = gAx(j);
    if (g == 0)
        continue
    end
    for i = 1:k    
        q = qAx(i);
        H = applyParametric(fc, g, q, ones(size(fax)), fs);
        QT = optimalQForTF(fc, g, fax, H);
        Ha = applyParametric(fc, g, QT, ones(size(fax)), fs);
        qError(j, i) = (QT - q) / q;
%             subplot 211
%             hold off
%             subplot 212
%             hold off
%             tfPlot(fax, H)
%             tfPlot(fax, Ha, 'ylim', [-10, 10])
%             pause(0.1)
    end
end
%     imagesc(qAx, gAx, qError)
imagesc(qError)
set(gca, 'XScale', 'log')
colorbar
pause(0.001)
meanError = mean(qError(:).^2);



% figure()
% imagesc(qAx, gAx, log(tfError.^2))
% colorbar


