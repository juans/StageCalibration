clear all, close all, clc

addpath('../DSPFunc');


[ref, fs] = audioread('../AudioFiles/GroundPlane/32Bit/NzRef-A77x-18.wav');
res = audioread('../AudioFiles/GroundPlane/32Bit/NzRes-A77x-18.wav');

[H, c] = nzToTF(ref, res, 2^16, 2^7);

H = H(1:end/2);
H = applyDelay(H, 'm', -1.06, fs);
fax = freqAxis(fs, length(H));
g = abs(H) \ (1 * ones(size(H)));
H = H * g;
H = tfSmoother(fax, H, fs, 1, 'smoothing', 'erb');

HPre = H;

%%

H = HPre;



logfax = 2.^linspace(log2(fax(2)), log2(fax(end)), 2^8);
Hlog = zeros(size(logfax));

for i = 1:length(logfax)
    idx = find(fax > logfax(i), 1);
    Hlog(i) = H(idx);
end

% tfPlot(logfax, Hlog)
% tfPlot(fax, H)

stem(logfax, mag2db(abs(Hlog)))
hold on
plot(fax, mag2db(abs(H)))
set(gca, 'XScale', 'log')
xlim([2e1, 2e4])
grid on
