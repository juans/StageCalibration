
clear all, close all, clc
addpath('DSPFunc');

files = dir('AudioFiles/*.wav');

x0 = [];
xM = [];
xS = [];

nFiles = numel(files);

[~, fs] = audioread(['AudioFiles/', files(1).name]);


for n = 1:nFiles
    
    if regexp(files(n).name, 'IR_REF.wav')
        x0 = audioread(['AudioFiles/', files(n).name]);
    end
    
    if regexp(files(n).name, '**S.wav')
        xS = [xS, audioread(['AudioFiles/', files(n).name])];
    end
    
    if regexp(files(n).name, '**[^S][0-9].wav')
        xM = [xM, audioread(['AudioFiles/', files(n).name])];
    end
end

[~, nMains] = size(xM);
[~, nSubs]  = size(xS);

h0 = chirpToIR(x0, x0);
hM = chirpToIR(x0, xM);
hS = chirpToIR(x0, xS);

[hM, pM] = rectifyPolarities(hM);
[hS, pS] = rectifyPolarities(hS);

if any([pM, pS] == -1)
    disp("Caution at least one Sine Sweep has an inverted polarity");
    disp("The corresponding Sine Sweep has been corrected for the analysis");
end

tM = finddelay(h0, hM);
tS = finddelay(h0, hS);

hM = circMatrixShift(hM, -tM);
hS = circMatrixShift(hS, -tS);

%%

winLen = 2^4;
irLen = 2^7;
irPad = 2^13;
preRoll = 2^4;
postRoll = 2^5;

winStart = hann(preRoll * 2);
winStart = winStart(1:end/2);
winEnd = hann(postRoll * 2);
winEnd = winEnd(end/2+1:end);

win = [winStart; ones(irLen - preRoll - postRoll, 1); winEnd];

%%

h0Opt = zeros(irPad, 1);
h0Opt(1:irLen) = h0(end/2 - preRoll + 1:end/2 - preRoll + irLen) .* win;

hMOpt = zeros(irPad, nMains);
for n = 1:nMains
    hMOpt(1:irLen, n) = hM(end/2 - preRoll + 1:end/2 - preRoll + irLen, n) .* win;
end

hSOpt = zeros(irPad, nSubs);
for n = 1:nSubs
    hSOpt(1:irLen, n) = hS(end/2 - preRoll + 1:end/2 - preRoll + irLen, n) .* win;
end

h0Opt = circMatrixShift(h0Opt, -preRoll + 1);
hMOpt = circMatrixShift(hMOpt, -preRoll + 1);
hSOpt = circMatrixShift(hSOpt, -preRoll + 1);

H0 = fft(h0Opt);
HM = fft(hMOpt);
HS = fft(hSOpt);

H0 = H0(1:end/2,:);
HM = HM(1:end/2,:);
HS = HS(1:end/2,:);

fax = linspace(0, fs/2, length(H0) + 1)';
fax = fax(1:end-1);

HPre0 = H0;
HPreM = HM;
HPreS = HS;

%%

H0 = HPre0;
HM = HPreM;
HS = HPreS;

xOverFreq = 90;


HM  = applyButterworth(2, xOverFreq, HM, fs, 'high');
HPF = applyButterworth(2, xOverFreq, H0, fs, 'high');

HS  = applyLinkWitz(4, xOverFreq, HS, fs, 'low');
LPF = applyLinkWitz(4, xOverFreq, H0, fs, 'low');

[dM, dS] = optimizePhase(HM, HS, fax, 2, xOverFreq, fs, 'single');

HM = applyDelay(HM, 'ms', dM, fs);
HS = applyDelay(HS, 'ms', dS, fs);

HM = applyParametric(120, 3, 1, HM, fs); 
HM = applyParametric(1000, -2, 1, HM, fs);
HM = applyParametric(350, -1, 1, HM, fs);
HM = applyParametric(3800, 1.8, 0.7, HM, fs);

weights = abs(HPF) / sum(abs(HPF));
gM = optimizeGains(HM, H0, weights);
weights = abs(LPF) / sum(abs(LPF));
gS = optimizeGains(HS, H0, weights);

HM = applyGain(gM, HM);
HS = applyGain(gS, HS);

HS = applyGain(-0.5, HS);



% =================== Plotting ================= %

octFrac = 1;
smoothingType = 'erb';

HT = sqrt(2) * sum(HM + HS, 2) / nMains; 

H0Smooth = tfSmoother(fax, H0, fs, octFrac, 'smoothing', smoothingType);
HMSmooth = tfSmoother(fax, HM, fs, octFrac, 'smoothing', smoothingType);
HSSmooth = tfSmoother(fax, HS, fs, octFrac, 'smoothing', smoothingType);
HTSmooth = tfSmoother(fax, HT, fs, octFrac, 'smoothing', smoothingType);


close all;
ylim = [-20, 6];
tfPlot(fax, H0Smooth, 'ylim', ylim);
tfPlot(fax, HMSmooth, 'ylim', ylim);
tfPlot(fax, HSSmooth, 'ylim', ylim);
tfPlot(fax, HTSmooth, 'ylim', ylim);