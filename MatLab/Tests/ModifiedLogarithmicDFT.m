load handel

N = 1024;
n = (0:N-1);
ko = (N/2)^(1/N);
k = ko.^(1 + n');
win = blackman(N);
% k = n';
A = exp(-1j * 2 * pi * k * n / (N));
%%
i = 1;
% T = cputime;
% while i < length(y) - N;
    Y = A * y(i:i+N-1);
%     i = i + 1;
% end
% cputime - T;

%%
% Y = A * (win .* y(1:N));
% Y = conv(mag2db(abs(Y)), ones(1, 1), 'same');
% Y = conv(mag2db(abs(Y)), ones(1, 1), 'same');

% i = 1;
% T = cputime;
% while i < length(y) - N
    X = fft(y(i:i+N-1), 2 * N);
    X = X(1:end/2);
%     i = i + 1;
% end
% cputime - T;




%%

fax1 = k * Fs/N;
fax2 = linspace(0, Fs/2, N);

subplot 211
plot(fax1, conv(mag2db(abs(Y)), ones(10, 1), 'same'))
set(gca, 'XScale', 'log')
xlim([2e1, Fs/2])
subplot 212
plot(fax2, mag2db(abs(X)))
set(gca, 'XScale', 'log')
xlim([2e1, Fs/2])

%%

% for i = 1:N
%     plot(real(A(i,:)));
%     pause(0.1)
% end