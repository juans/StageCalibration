clear all, close all, clc

addpath('DSPFunc');

[x0, fs] = audioread('AudioFiles/IR_REF.wav');
[x1, fs] = audioread('AudioFiles/IR_1.wav');

h = chirpToIR(x0, x1);

irLen = 2^12;
preRoll = 2^4;
postRoll = 2^6;

winStart = hann(preRoll * 2);
winStart = winStart(1:end/2);
winEnd = hann(postRoll * 2);
winEnd = winEnd(end/2+1:end);

win =  AsymWin(winStart, winEnd, irLen);
hSec = h(end/2 + 1 - preRoll:end/2 - preRoll + irLen);
hSec = hSec .* win;

[~, idx] = max(hSec);

hSec = circshift(hSec, -idx + 1);

figure()
plot(win);
hold on
plot(hSec / max(abs(hSec)));

figure()
freqz(hSec)

figure()
freqz(win);