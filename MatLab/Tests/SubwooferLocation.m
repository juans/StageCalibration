close all, clc

c = 343;
f = 300;
l = 5;
w = 4;
Td = 0.01;

xAx = 0:Td:l;
yAx = 0:Td:w;

% src = [4, 1; 1, 3];
src = [1, 1; 1, 3];

[X, Y] = meshgrid(xAx, yAx);
Nk = 5;
alpha = 0.9;

S = zeros(60, 401, 501);
for f = 20:1:80
    sT = zeros(size(X));
    for i = 1:length(src)
        for kx = -Nk:Nk
            for ky = -Nk:Nk
                xSrc = src(i, 1);
                ySrc = src(i, 2);
                x = (-1)^kx * xSrc + (kx + (1 - (-1)^kx)/2) * l;
                y = (-1)^ky * ySrc + (ky + (1 - (-1)^ky)/2) * w;
%                 display(['X: ', num2str(kx), ' Y: ', num2str(ky)])
                d = sqrt((X - x).^2 + (Y - y).^2);
                si= alpha.^(abs(kx) + abs(ky)) * exp(-1j * 2 * pi * f * d / c)./(d / 0.1);
                sT = sT + si;
            end
        end
    end
    sT(sT > 1) = 0;
    S(f-19,:,:) = sT;
end

%%

meanP = zeros(60, 1);
varP = zeros(60, 1);
for f = 20:1:80
    sT = squeeze(S(f-19,:,:));
    dC = sqrt((X - l/2).^2 + (Y - w/2).^2);
    dF = dC < 0.5;
    
    sTmag = abs(sT);% ./ dF .* dF;
    
    meanP(f - 19) = mean(mean(sTmag, 'omitnan'), 'omitnan');
    varP(f - 19) = var(var(sTmag, 'omitnan'), 'omitnan');
    
    hold off
    imagesc(xAx, yAx, mag2db(sTmag),[-60, 0]);
    daspect([1, 1, 1])
    colorbar
    colormap jet
    hold on
    
    ref = 0.5 * exp(-1j * 2 * pi * linspace(0, 1));
    plot(real(ref) + l/2, imag(ref) + w/2, 'k:');
    titleString = ['Frequency: ', num2str(f), ' Hz'];
    title(titleString)
    pause(0.1);
end



