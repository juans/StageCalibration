clear all
close all
clc

addpath('DSPFunc');

[~, fs] = audioread('AudioFiles/IR_REF.wav');
x0 = audioread('AudioFiles/IR_REF.wav');
xL = audioread('AudioFiles/KH_120_L.wav');
xR = audioread('AudioFiles/KH_120_R.wav');

h0 = chirpToIR(x0, x0);
hL = chirpToIR(x0, xL);
hR = chirpToIR(x0, xR);

dL = finddelay(h0, hL);
dR = finddelay(h0, hR);

hL = circshift(hL, -dL);
hR = circshift(hR, -dR);


winLen = 2^4;
irLen = 2^8;
irPad = 2^13;
preRoll = 2^3;
postRoll = 2^5;

winStart = hann(preRoll * 2);
winStart = winStart(1:end/2);
winEnd = hann(postRoll * 2);
winEnd = winEnd(end/2+1:end);

win = [winStart; ones(irLen - preRoll - postRoll, 1); winEnd];

h0Opt = zeros(irPad * 2, 1);

h0Opt(1:irLen) = h0(end/2 - preRoll + 1:end/2 - preRoll + irLen) .* win;
h0Opt = circshift(h0Opt, -preRoll + 1);

hOptL = zeros(irPad * 2, 1);
hOptR = zeros(irPad * 2, 1);

hOptL(1:irLen) = hL(end/2 - preRoll + 1:end/2 - preRoll + irLen) .* win;
hOptR(1:irLen) = hR(end/2 - preRoll + 1:end/2 - preRoll + irLen) .* win;

hOptL = circshift(hOptL, -preRoll + 1);
hOptR = circshift(hOptR, -preRoll + 1);

HL = fft(hOptL);
HR = fft(hOptR);

HL = HL(1:end/2);
HR = HR(1:end/2);

HR = pureDelay(HR, 'samp', 0.7, fs);




fax = linspace(0, fs/2, length(HL) + 1);
fax = fax(1:end-1);

ylim = [-60, 10];

tfPlot(fax, HL, 'ylim', ylim);
tfPlot(fax, HR, 'ylim', ylim);
