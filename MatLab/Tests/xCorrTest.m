clear all, close all, clc

[~, fs] = audioread('AudioFiles/IR_REF.wav');
x0 = audioread('AudioFiles/IR_REF.wav');
x1 = audioread('AudioFiles/IR_2.wav');
x2 = audioread('AudioFiles/IR_2S.wav');

h0 = chirpToIR(x0, x0);
h1 = chirpToIR(x0, x1);
h2 = chirpToIR(x0, x2);

d1 = finddelay(h0, h1);
d2 = finddelay(h0, h2);

h1 = circshift(h1, -d1);
h2 = circshift(h2, -d2);

dMax = volterraDistance(2, 20000, 20, fs, 10);

h1 = h1(end/2:end);
h2 = h2(end/2:end);

xCorr1 = xcorr(h1, h1);
xCorr2 = xcorr(h2, h2);
