function d = distanceBetweenMaxPeaks(x)
    
    [pks, idx] = findPeaks(x);
    
    [~, maxIdx] = max(pks);
    pks(maxIdx) = 0;
    maxIdx = idx(maxIdx);
    
    [~, maxIdx2] = max(pks);
    maxIdx2 = idx(maxIdx2);
    
    d = abs(maxIdx - maxIdx2);
end