function gains = optimizeGains(H, HRef, varargin)

N = length(HRef);

base = (N-1)^(1/N);

wex = 1:N;
wex = (base).^(-wex');
wex = wex / sum(wex);

if length(varargin) == 1
    w = varargin{1};
else 
    w = ones(size(H0));
end

levels = mag2db(sum(abs(H) .* w .* wex));
refLev = mag2db(sum(abs(HRef) .* w .* wex));


gains = refLev - levels;

end



