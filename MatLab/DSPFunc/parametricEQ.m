function [b, a] = parametricEQ(fc, gainIndB, Q);
% Written by: JuanS

% Design a parametric analog eq.
% Take into account that the transfer function for a boost
% is slightly different than the one for a cut; this is to
% preserve the same Q for both transfer functions.

g = db2mag(gainIndB);
wo = fc;

if g > 1
	b2 = 1/(wo * wo);
	b1 = g/(Q * wo);
	b0 = 1;
	a2 = 1/(wo * wo);
	a1 = 1/(wo * Q);
	a0 = 1;
elseif g < 1
	b2 = 1/(wo * wo);
	b1 = 1/(Q * wo);
	b0 = 1;
	a2 = 1/(wo * wo);
	a1 = 1/(g * wo * Q);
	a0 = 1;
else
	b2 = 0;
	b1 = 0;
	b0 = 1;
	a2 = 0;
	a1 = 0;
	a0 = 1;
end



b = [b2, b1, b0];
a = [a2, a1, a0];

end