function [pks, idx] = findPeaks(x, numPeaks)

N = length(x);
data = zeros(0, 2);
for i = 2:N-1
    if x(i) > x(i-1) && x(i) > x(i+1)
        pk = x(i);
        idx = i;
        data = [data; [idx, pk]];
    end
end

data = sortrows(data, 2, 'descend');

if nargin < 2
    numPeaks = length(data);
end

idx = data(1:numPeaks,1);
pks = data(1:numPeaks,2);


end