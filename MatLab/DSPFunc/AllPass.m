function [b, a] = AllPass(fc, Q)

% Written by: JuanS

% All Pass section from a biquad

w=2*pi*fc;						% converts centerfrequency from Hz to Radians
b=[1 -w./Q w^2];				% numerator of allpass transfer funct.
a=[1  w./Q w^2];	


end