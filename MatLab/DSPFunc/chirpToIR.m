function h = chirpToIR(chirpRef, chirpRes)
% CHIRPTOIR exponential sine sweep to impulse response with volterra harmonic
% distortion byproducts.
%     h = chirpToIR(chirpRef, chirpRes) returns a 2 * nextPow2(length(chirpRes)
%     vector containing the response from end/2+1:end.
%     

    [~, nRes] = size(chirpRes); 
    x0 = chirpRef;
    x1 = chirpRes;

    N = length(x0);
    Nlog = nextpow2(N);

    xi = [zeros((2^Nlog - N)/2, 1); flip(x0); zeros((2^Nlog - N)/2 + 2^Nlog, 1)];
    x1 = [zeros((2^Nlog - N)/2, nRes); x1; zeros((2^Nlog - N)/2 + 2^Nlog, nRes)];

    N = length(x0);

    Xi = fft(xi);
    X1 = fft(x1);
    
    wi = linspace(-1, 1, length(Xi) + 1)';
    wi = wi(1:end-1);
    wi = fftshift(wi);
    
    
    % TODO: check the scaling factor which shouldnt be this
    H = Xi .* X1 .* abs(wi) / (16 * sqrt(N));

    h = real(ifft(H));
end