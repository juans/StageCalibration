function win = AsymWin(preRollWin, postRollWin, irLen)

    preRollWin = preRollWin(:);
    postRollWin = postRollWin(:);
    preRoll = length(preRollWin);
    postRoll = length(postRollWin);

    win = ones(irLen, 1);
    win(1:preRoll) = preRollWin;
    win(end - postRoll+1:end) = postRollWin;

end