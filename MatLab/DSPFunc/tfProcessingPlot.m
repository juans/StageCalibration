function tfProcessingPlot(fax, H, HFilt, varargin)


% Written by: JuanS

% ===================================================== %
% Transfer Function Plot
% ===================================================== %
%
% TODO
% 
% Include title in optional parameter list with valparam


% List of Default Params available :)

% this particular implementation allows to also plot the processing
% applied to the data given that you provide the transfer functions
% of those filters

unwrapPhaseFlag = 0;
yMagLim = 'auto';



for n = 1:2:length(varargin)
	if strcmp(varargin{n}, 'unwrap')
		unwrapPhaseFlag = varargin{n+1} == 1;
	elseif strcmp(varargin{n}, 'ylim')
		yMagLim = varargin{n+1};
	elseif varargin{n} == 0
	end
end


if [0, 0] == size(H)
	fprintf('Transfer Function is Empty\n');
	return
end

colordef white

scrsz = get(0, 'ScreenSize');

xLim = [2e1, 2e4];
yAngLim = [-180, 180];


subplot(3, 1, 1);
semilogx(fax, mag2db(abs(H)));
set(gcf,'OuterPosition', [8 scrsz(4)-(scrsz(4)-35), (scrsz(3)-16), (scrsz(4)-105)]);
grid on;
xlim(xLim);
ylim(yMagLim);
hold on;
xlabel('Frequency (Hz)');
ylabel('dB');

subplot(3, 1, 2);
if unwrapPhaseFlag == 1
	semilogx(fax, rad2deg(unwrap(angle(H),[], 2)));
	axis([xLim, yAngLim]);
	ylim('auto');
else
	semilogx(fax, rad2deg(angle(H)));
	axis([xLim, yAngLim]);
	set(gca, 'ytick', -180:30:180);
end
hold on;
grid on;
xlabel('Frequency (Hz)')
ylabel('Phase (degrees)')

subplot(3, 1, 3);
semilogx(fax, mag2db(abs(HFilt)), 'color', 'k');
set(gcf,'OuterPosition', [8 scrsz(4)-(scrsz(4)-35), (scrsz(3)-16), (scrsz(4)-105)]);
grid on;
xlim(xLim);
ylim([-6 6]);
hold on;
xlabel('Frequency (Hz)');
ylabel('dB');

yyaxis right;
if unwrapPhaseFlag == 1
	semilogx(fax, rad2deg(unwrap(angle(HFilt),[], 2)), ':', 'color', 'r');
	axis([xLim, yAngLim]);
	ylim('auto');
else
	semilogx(fax, rad2deg(angle(HFilt)), ':', 'color', 'r');
	axis([xLim, yAngLim]);
	set(gca, 'ytick', -180:30:180);
end
hold on;
grid on;
xlabel('Frequency (Hz)')
ylabel('Phase (degrees)')
yyaxis left;



subplot(3, 1, 1);

end