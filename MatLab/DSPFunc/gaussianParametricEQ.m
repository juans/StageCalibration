function Hmag = gaussianParametricEQ(fax, f, gainIndB, Q) 

    if (strcmp(Q, 'base'))
        Q = 0.25;
        gainIndB = 1;
    end

    mu = 1 / ( 2 * Q );

    Hmag = gainIndB .* 10.^(-(2 .* Q .* log10(fax / f)).^2);
    Hmag(1) = Hmag(2);
    
end