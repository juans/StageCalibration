function Hmag = db( H )
    Hmag = mag2db(abs(H));
end