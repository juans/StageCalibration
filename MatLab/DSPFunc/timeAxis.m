function tax = timeAxis(fs, N)

    tax = linspace(0, (N + 1) / fs, N + 1)';
    tax = tax(1:end-1);
   
end