function Hfilt = applyAnalogFilter(b, a, H, fs)

fax = linspace(0, fs/2, length(H) + 1)';
fax = fax(1:end-1);

B = freqs(b, a, fax);

Hfilt = H .* B;

end

