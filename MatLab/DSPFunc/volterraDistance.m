function d = volterraDistance(harmonic, fmax, fmin, fs, length)

    ssLen = length * fs;
    base = (fmax/fmin)^(1/(ssLen));
    d = floor(log(harmonic)/log(base));

end