function H = applyLinkWitz(order, fc, H, fs, type)

[b, a] = butter(order/2, fc, type, 's');
b = conv(b, b);
a = conv(a, a);
H = applyAnalogFilter(b, a, H, fs);

end