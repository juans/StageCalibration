function H = applyButterworth(order, fc, H, fs, type)

    [b, a] = butter(order, fc, type, 's');
    H = applyAnalogFilter(b, a, H, fs);

end