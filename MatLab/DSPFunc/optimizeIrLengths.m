function hOpt = optimizeIrLengths(h, volterraDistance, th)

    N = length(h);

    vD = volterraDistance;

    hSub = h(end/2:end);
    xC = xcorr(hSub, hSub);
    irLen = distanceBetweenMaxPeaks(xC);
    
    
    hDiffdB = mag2db(abs(diff(h)));
    hDiffdB = hDiffdB - max(hDiffdB);
    
    idx = N/2 - vD/2;
    
    while hDiffdB(idx) < th
        idx = idx + 1;
    end
    
    preRoll = N/2 - idx;
    
    hOpt = h(N/2 - preRoll:N/2 - preRoll + irLen);
    
end