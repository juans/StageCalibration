function H = applyShelf(fc, Q, H, fs, type)

[b, a] = AllPass(fc, Q);

shelvingEQ(fc, gainIndB, Q, type);

H = applyAnalogFilter(b, a, H, fs);

end