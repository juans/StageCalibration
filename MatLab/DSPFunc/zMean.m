function Hmean = zMean(H)

Hmag = mag2db(abs(H));
Hang = angle(H);
Hang = unwrap(Hang);

HmagMean = mean(Hmag, 2, 'omitnan');
HangMean = mean(Hang, 2, 'omitnan');

Hmean = db2mag(HmagMean).*exp(1j * HangMean);

end