function [dM, dS] = optimizePhase(HM, HS, fax, order, xOverFreq, fs, type)
  
    [~, nMains] = size(HM);
    [~, nSubs] = size(HS);

    decayPerOctave = order * 6;
    octFrac = 24 / decayPerOctave;
    
    minFreq = xOverFreq * 2^(-octFrac/2);
    maxFreq = xOverFreq * 2^( octFrac/2);
    
    minIdx = find(minFreq < fax, 1);
    maxIdx = find(maxFreq < fax, 1);
    
    maxPow2Idx = floor(log2(maxIdx - minIdx));
    
    idxs = minIdx + 2.^(0:maxPow2Idx)';
    
    HMMeanAng = unwrap(angle(zMean(HM)));
    HSMeanAng = unwrap(angle(zMean(HS)));
    
    
    pMMean = polyfit(idxs, HMMeanAng(idxs), 1);
    pSMean = polyfit(idxs, HSMeanAng(idxs), 1);
    
    dM = zeros(1, nMains);
    dS = zeros(1, nSubs);
    
    if pMMean(1) > pSMean(1)
        HMAng = unwrap(angle(HM));
        for n = 1:nMains
            pM = polyfit(idxs, HMAng(idxs,n), 1);
            dM(n) = (pM(1) - pSMean(1)) * fax(2);
        end
    elseif pmMean(1) < pSMean(1)
        HSAng = unwrap(angle(HS));
        for n = 1:nSubs
            pM = polyfit(idxs, HSAng(idxs,n), 1);
            dS(n) = (pM(1) - pSMean(1)) * fax(2);
        end
    else
        disp("Optimization was not possible, data too sparse");
        return
    end
    
    if strcmp(type, 'single')
        dM = mean(dM);
        dS = mean(dS);
    end
end

