function fax = freqAxis(fs, N)

    fax = linspace(0, fs/2, N + 1)';
    fax = fax(1:end-1);
   
end