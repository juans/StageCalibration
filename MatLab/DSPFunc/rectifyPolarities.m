function [h, p] = rectifyPolarities(h)

p = max(h) == max(abs(h));

p = p * 2 - 1;

h = h .* p;

end