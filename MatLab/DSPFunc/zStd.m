function Hstd = zStd(H)
Hmag = dB(H);
Hang = angle(H);
Hang = unwrap(Hang);

HmagStd = std(Hmag, 1, 'omitnan');
HangStd = std(Hang, 1, 'omitnan');

Hstd = db2mag(HmagStd).*exp(1j * HangStd);
end