function [b, a] = shelvingEQ(fc, gainIndB, Q, type)

% Written by: JuanS

% Design a second order shelving EQ.
% take into account that the Q factor can create big resonances on the
% boost side of the EQ


g = db2mag(gainIndB);
wo = 2 * pi * fc;

if g == 1
    b2 = 0;
	b1 = 0;
	b0 = 1;
	a2 = 0;
	a1 = 0;
	a0 = 1;
else
    b2 = 1/(wo * wo);
	b1 = 1/(Q * sqrt(wo));
	b0 = 1;
	a2 = 1/(wo * wo);
	a1 = 1/(Q * sqrt(wo));
	a0 = 1;
    
    if type == 'low'
        if g > 1
            b2 = b2 * g;
        else
            a2 = a2 / g;
        end
    elseif type == 'hi'
        if g > 1
            b0 = b0 * g;
        else
            a0 = a0 / g;
        end
    else
        error('Type not recognized');
    end
end
	
b = [b2, b1, b0];
a = [a2, a1, a0];

end