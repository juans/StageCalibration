function H = circMatrixShift(H, k)
% CIRCMATRIXSHIFT circular shift on a matrix with a different shift value for
% each column
%   H = circMatrixShift(H, k) returns a circularly shifted version of H
%   such that each column of H is shifted by the corresponding value in the
%   array k. If k is not an array but a single value, all the columns are
%   shifted by the same value k.
    
    if length(k) == 1
        H = circshift(H, k);
        return 
    end
    
    [~, numCols] = size(H);
    
    if numCols ~= length(k)
        error('Number of Elements in vector doesnt match the number of columns of H');
    end
    
    for i = 1:length(k)
        H(:,i) = circshift(H(:,i), k(i));
    end
end