function HDel = pureDelay(H, type, del, fs)
    if strcmp(type, 'samp')
        d = del / fs;
    elseif strcmp(type, 'ms')
        d = del / 1000;
    elseif strcmp(type, 'm')
        d = del / 340;
    else
        error("The Type flag is neither of the options available");
    end
    
    fax = linspace(0, fs/2, length(H) + 1)';
    fax = fax(1:end - 1);
        
    HDel = H .* exp(-1j * 2 * pi * fax * d);
end