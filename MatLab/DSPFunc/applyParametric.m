function H = applyParametric(fc, gainIndB, Q, H, fs);

    for i = 1:length(fc)
        [b, a] = parametricEQ(fc(i), gainIndB(i), Q(i));
        H = applyAnalogFilter(b, a, H, fs);
    end
end