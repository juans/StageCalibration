function hmin = minPhaseFilt(h)
    H = fft(h);
    Hmag = abs(H);
    
    HmagLog = log10(Hmag);
    HangLog = imag(hilbert(HmagLog));

    Hmin = 10.^(HmagLog - 1j * HangLog);
    hmin = real(ifft(Hmin));

    flag = isminphase( hmin );
    if flag == true
        disp('Your IR is minPhase');
    end
end