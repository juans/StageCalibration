function H = applyGain(Gain, H)

H = H .* db2mag(Gain);

end