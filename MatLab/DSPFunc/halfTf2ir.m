function h = halfTf2ir(H)
    H = H(:);
    h = real(ifft( [H; flip(H)] ) );
end