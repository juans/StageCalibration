function H = applyAllPass(fc, Q, H, fs)

[b, a] = AllPass(fc, Q);

H = applyAnalogFilter(b, a, H, fs);

end