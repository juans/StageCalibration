function tfPlot(fax, H, varargin)

% Written by: JuanS
% ===================================================== %
% Transfer Function Plot
% ===================================================== %
%
% TODO
% 
% Include title in optional parameter list with valparam


% List of Default Params available :)

unwrapPhaseFlag = 0;
yMagLim = 'auto';

for n = 1:2:length(varargin)
	if strcmp(varargin{n}, 'unwrap')
		unwrapPhaseFlag = varargin{n+1} == 1;
		fprintf('Unwraping is Enabled\n');
	elseif strcmp(varargin{n}, 'ylim')
		yMagLim = varargin{n+1};
	elseif strcmp(varargin{n}, 'fax')
		fax = varargin{n+1};
	end
end

% 
% if fax(1) == 0
%     fax(1) = 1;
% end


if [0, 0] == size(H)
	fprintf('Transfer Function is Empty\n');
	return
end

colordef white

scrsz = get(0, 'ScreenSize');


xLim = [1e1, 2e4];
yAngLim = [-180, 180];

% [n, m] = size(H);
% if n > 5
% 	lineStyle = ':';
% else
% 	lineStyle = '-';
% end

lineStyle = '-';


subplot(2, 1, 1);
semilogx(fax, mag2db(abs(H)), lineStyle);
set(gcf,'OuterPosition', [8 scrsz(4)-(scrsz(4)-35), (scrsz(3)-16), (scrsz(4)-105)]);
grid on;
xlim(xLim);
ylim(yMagLim);
hold on;
xlabel('Frequency (Hz)');
ylabel('dB');

subplot(2, 1, 2);
if unwrapPhaseFlag == 1
	semilogx(fax, rad2deg(unwrap(angle(H),[], 2)), lineStyle);
	axis([xLim, yAngLim]);
	ylim('auto');
else
	semilogx(fax, rad2deg(angle(H)), lineStyle);
	axis([xLim, yAngLim]);
	set(gca, 'ytick', -180:30:180);
end
hold on;
grid on;
xlabel('Frequency (Hz)')
ylabel('Phase (degrees)')

subplot(2,1,1);

end