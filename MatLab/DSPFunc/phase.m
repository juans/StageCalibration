function Hang = phase( H )

    Hang = rad2deg(angle(H));

end