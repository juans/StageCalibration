function H = applyPolarities(H, p)

    H = H .* p;

end