function Hsmooth = tfSmoother(freq, H, fs, frac, varargin)

    if frac == 0
        Hsmooth = H;
        return;
    end

    tfType = 'comp';
    smoothingType = 'constantQ';
    if nargin == 2
        frac = 1/6;
    end
    
    for n = 1:2:length(varargin)
        if strcmp(varargin{n}, 'tfType')
            tfType = varargin{n+1};
        elseif strcmp(varargin{n}, 'smoothing')
            smoothingType = varargin{n+1};
        end
    end

    if strcmp(tfType, 'mag')
        if strcmp(smoothingType, 'constantQ')
            Hsmooth = zeros(size(H));
            N = length(H);
            for i = 1:N
                minLim = floor(max(1, i * 2^(-frac / 2)));
                maxLim = floor(min(N, i * 2^( frac / 2)));
                Hsmooth(i,:) = mean(H(minLim:maxLim,:));
            end
        elseif strcmp(smoothingType, 'erb')
            
            N = length(freq);
            erb = hz2erb(freq);
            erbLow = erb - frac/2;
            erbHi = erb + frac/2;

            erbLow(erbLow < erb(1)) = erb(1);
            erbHi(erbHi > erb(end)) = erb(end);

            lowBin = round( N * erb2hz(erbLow) * 2 / fs ) + 1;
            hiBin  = round( N * erb2hz(erbHi ) * 2 / fs );
            Hsmooth = zeros(size(H));
            for i = 1:N
                minLim = max(lowBin(i), 1);
                maxLim = min(hiBin(i), N);
                Hsmooth(i,:) = mean(H(minLim:maxLim,:));
            end
        else
            error('Smoothing typer not recognized');
        end
    elseif strcmp(tfType, 'ang')
        if strcmp(smoothingType, 'constantQ')
            H = unwrap(H);

            Hsmooth = zeros(size(H));
            N = length(H);
            for i = 1:N
                minLim = floor(max(1, i * 2^(-frac / 2)));
                maxLim = floor(min(N, i * 2^( frac / 2)));
                Hsmooth(i,:) = mean(H(minLim:maxLim,:));
            end

            Hsmooth = mod(Hsmooth + pi, 2 * pi) - pi;
        elseif strcmp(smoothingType, 'erb')
            H = unwrap(H);
                        
            N = length(freq);
            erb = hz2erb(freq);
            erbLow = erb - frac/2;
            erbHi = erb + frac/2;

            erbLow(erbLow < erb(1)) = erb(1);
            erbHi(erbHi > erb(end)) = erb(end);

            lowBin = round( N * erb2hz(erbLow) * 2 / fs ) + 1;
            hiBin  = round( N * erb2hz(erbHi ) * 2 / fs );
            Hsmooth = zeros(size(H));
            for i = 1:N
                minLim = lowBin(i);
                maxLim = hiBin(i);
                Hsmooth(i,:) = mean(H(minLim:maxLim,:));
            end
            
            Hsmooth = mod(Hsmooth + pi, 2 * pi) - pi;
        else
            error('Smoothing typer not recognized');
        end
    elseif strcmp(tfType, 'comp')
        
        Hmag = mag2db(abs(H));
        Hang = angle(H);
        HmagSmooth = tfSmoother(freq, Hmag, fs, frac, 'tfType', 'mag', 'smoothing', smoothingType);
        HangSmooth = tfSmoother(freq, Hang, fs, frac, 'tfType', 'ang', 'smoothing', smoothingType);

        Hsmooth = db2mag(HmagSmooth) .* exp(1j * HangSmooth);
    else
        error('TF Type didnt match any available types');
    end  
end