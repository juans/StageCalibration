function h = nzToIr(ref, res, irLen, hopSize)

    [N, numIrs] = size(ref);
    XcumMag = zeros(2 * irLen, numIrs);
    XcumAng = zeros(2 * irLen, numIrs);
    YcumMag = zeros(2 * irLen, numIrs);
    YcumAng = zeros(2 * irLen, numIrs);
    pos = 1;
    numHops = 0;
    while(pos < N)
        if (pos + irLen-1 > N)
            x = ref(pos:end,:);
            y = res(pos:end,:);
            x = [x; zeros(irLen - length(x), numIrs)];
            y = [y; zeros(irLen - length(y), numIrs)];
        else
            x = ref(pos:pos+irLen-1,:);
            y = res(pos:pos+irLen-1,:);
        end
        
        X = fft([x; zeros(length(x), numIrs)]);
        Y = fft([y; zeros(length(y), numIrs)]);
        
        XcumMag = XcumMag + mag2db(abs( X ));
        XcumAng = XcumAng + (unwrap(angle( X )) + pi);
        YcumMag = YcumMag + mag2db(abs( Y ));
        YcumAng = YcumAng + (unwrap(angle( Y )) + pi);
        
        pos = pos + hopSize;
        numHops = numHops + 1;
    end
    
    XcumMag = (XcumMag/numHops);
    XcumAng = mod((XcumAng/numHops), 2 * pi) - pi;
    
    YcumMag = (YcumMag/numHops);
    YcumAng = mod((YcumAng/numHops), 2 * pi) - pi;
    
    Xcum = db2mag(XcumMag) .* exp(1j * XcumAng);
    Ycum = db2mag(YcumMag) .* exp(1j * YcumAng);

    h = real(ifft( Ycum ./ Xcum ));
end