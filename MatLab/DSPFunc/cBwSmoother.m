function Hsmooth = cBwSmoother(H, octFrac, type)

    if octFrac == 0
        Hsmooth = H;
        return;
    end

    if strcmp(type, 'mag')
        Hsmooth = zeros(size(H));
        N = length(H);
        for i = 1:N
            minLim = floor(max(1, i * 2^(-octFrac / 2)));
            maxLim = floor(min(N, i * 2^( octFrac / 2)));
            Hsmooth(i,:) = mean(H(minLim:maxLim,:));
        end
    elseif strcmp(type, 'ang')
        
        H = unwrap(H);
        
        Hsmooth = zeros(size(H));
        N = length(H);
        for i = 1:N
            minLim = floor(max(1, i * 2^(-octFrac / 2)));
            maxLim = floor(min(N, i * 2^( octFrac / 2)));
            Hsmooth(i,:) = mean(H(minLim:maxLim,:));
        end
        
        Hsmooth = mod(Hsmooth + pi, 2 * pi) - pi;
    elseif strcmp(type, 'comp')
        Hmag = mag2db(abs(H));
        Hang = angle(H);
        HmagSmooth = cBwSmoother(Hmag, octFrac, 'mag');
        HangSmooth = cBwSmoother(Hang, octFrac, 'ang');
        
        Hsmooth = db2mag(HmagSmooth) .* exp(1j * HangSmooth);
    else
        error('The type flag didnt match any of the available options');
    end
end