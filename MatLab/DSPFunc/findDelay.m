function idx = findDelay(a, b)

    A = fft(a);
    B = fft(b);
    
    xCorr = ifft( A .* conj( B ) );
    
    [~, idx] = max(xCorr);
    
end