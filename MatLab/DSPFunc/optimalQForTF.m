function Q = optimalQForTF(fc, gain, fax, H)

    Hg = gaussianParametricEQ(fax, fc, gain, 'base');
    
    HTemp = log10(Hg);
    mu = HTemp \ (mag2db(abs(H)) / gain);
    
    Q = 1 / (4 * sqrt(abs(mu)));   
end