function h = getIRFromChirp(chirpRef, chirpResp, window, irLen)

x0 = chirpRef(:);
x1 = chirpResp(:);
window = window(:);

N = length(x0);
Nlog = nextpow2(N);

xi = [zeros((2^Nlog - N)/2, 1); flip(x0); zeros((2^Nlog - N)/2, 1); zeros(2^Nlog, 1)];
x0 = [zeros((2^Nlog - N)/2, 1); x0; zeros((2^Nlog - N)/2, 1); zeros(2^Nlog, 1)];
x1 = [zeros((2^Nlog - N)/2, 1); x1; zeros((2^Nlog - N)/2, 1); zeros(2^Nlog, 1)];

N = length(x0);

Xi = fft(xi);
X0 = fft(x0);
X1 = fft(x1);
fi = linspace(-0.5, 0.5, length(Xi) + 1)';
fi = fi(1:end-1);
fi = fftshift(fi);

H0 = Xi .* X0 .* abs(fi) / (sqrt(32 * N)) * db2mag(-0.529);
H1 = Xi .* X1 .* abs(fi) / (sqrt(32 * N)) * db2mag(-0.529);

h0 = ifft(H0);
h1 = ifft(H1);

winLen = length(window);

win = [window(1:end/2); ones(irLen - winLen, 1); window(end/2+1:end)];

del = finddelay(h1, h0)
h1 = circshift(h1, del);

h = h1((end-winLen)/2:(end-winLen)/2 + irLen - 1) .* win;

end